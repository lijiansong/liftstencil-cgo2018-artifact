
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: b3f4969b5eac5ef76b04f1fa21cf514a33c5b1988304ecf6a1be8f4ed1101894

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_578" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_577" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__597, global float* v__601){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__598[(4+(2*v_TP_577)+(2*v_TP_578)+(v_TP_577*v_TP_578))];
  /* Typed Value memory */
  /* Private Memory */
  float v__600_0;
  
  for (int v_wg_id_591 = get_group_id(1); v_wg_id_591 < (4094 / v_TP_577); v_wg_id_591 = (v_wg_id_591 + NUM_GROUPS_1)) {
    for (int v_wg_id_592 = get_group_id(0); v_wg_id_592 < (4094 / v_TP_578); v_wg_id_592 = (v_wg_id_592 + NUM_GROUPS_0)) {
      for (int v_l_id_593 = get_local_id(1); v_l_id_593 < (2 + v_TP_577); v_l_id_593 = (v_l_id_593 + LOCAL_SIZE_1)) {
        for (int v_l_id_594 = get_local_id(0); v_l_id_594 < (2 + v_TP_578); v_l_id_594 = (v_l_id_594 + LOCAL_SIZE_0)) {
          v__598[(v_l_id_594 + (2 * v_l_id_593) + (v_TP_578 * v_l_id_593))] = idfloat(v__597[(v_l_id_594 + (4096 * v_TP_577 * v_wg_id_591) + (4096 * (v_wg_id_592 / (4094 / v_TP_578))) + (4096 * (((4094 / v_TP_578) * (v_l_id_593 % (2 + v_TP_577))) / (4094 / v_TP_578))) + (v_TP_578 * ((v_wg_id_592 + ((4094 / v_TP_578) * (v_l_id_593 % (2 + v_TP_577)))) % (4094 / v_TP_578))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_595 = get_local_id(1); v_l_id_595 < v_TP_577; v_l_id_595 = (v_l_id_595 + LOCAL_SIZE_1)) {
        for (int v_l_id_596 = get_local_id(0); v_l_id_596 < v_TP_578; v_l_id_596 = (v_l_id_596 + LOCAL_SIZE_0)) {
          v__600_0 = grad(v__598[(1 + (v_l_id_596 % v_TP_578) + (2 * v_l_id_595) + (v_TP_578 * v_l_id_595))], v__598[(5 + v_l_id_596 + (2 * v_TP_578) + (2 * v_l_id_595) + (v_TP_578 * v_l_id_595))], v__598[(2 + v_TP_578 + ((v_TP_578 + v_l_id_596) % v_TP_578) + (2 * v_l_id_595) + (v_TP_578 * v_l_id_595))], v__598[(4 + v_TP_578 + ((v_TP_578 + v_l_id_596) % v_TP_578) + (2 * v_l_id_595) + (v_TP_578 * v_l_id_595))], v__598[(3 + v_TP_578 + ((v_TP_578 + v_l_id_596) % v_TP_578) + (2 * v_l_id_595) + (v_TP_578 * v_l_id_595))]); 
          v__601[(4097 + v_l_id_596 + (-4096 * ((v_l_id_596 + (v_TP_578 * ((v_wg_id_592 + ((4094 / v_TP_578) * ((v_l_id_595 + (v_TP_577 * v_wg_id_592)) % v_TP_577))) % (4094 / v_TP_578)))) / 4095)) + (4096 * v_TP_577 * v_wg_id_591) + (-16777216 * (((((4094 / v_TP_578) * ((v_l_id_595 + (v_TP_577 * v_wg_id_592)) % v_TP_577)) / (4094 / v_TP_578)) + (v_wg_id_592 / (4094 / v_TP_578)) + (v_TP_577 * v_wg_id_591)) / 4095)) + (4096 * (((4094 / v_TP_578) * ((v_l_id_595 + (v_TP_577 * v_wg_id_592)) % v_TP_577)) / (4094 / v_TP_578))) + (4096 * (v_wg_id_592 / (4094 / v_TP_578))) + (v_TP_578 * ((v_wg_id_592 + ((4094 / v_TP_578) * ((v_l_id_595 + (v_TP_577 * v_wg_id_592)) % v_TP_577))) % (4094 / v_TP_578))))] = id(v__600_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


