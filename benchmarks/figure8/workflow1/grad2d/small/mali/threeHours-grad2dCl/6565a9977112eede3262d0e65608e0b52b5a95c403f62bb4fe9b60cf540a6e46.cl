
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: 6565a9977112eede3262d0e65608e0b52b5a95c403f62bb4fe9b60cf540a6e46

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_238" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_237" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__253, global float* v__256){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__255_0;
  
  for (int v_gl_id_249 = get_global_id(0); v_gl_id_249 < (4094 / v_TP_237); v_gl_id_249 = (v_gl_id_249 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_250 = get_global_id(1); v_gl_id_250 < (4094 / v_TP_238); v_gl_id_250 = (v_gl_id_250 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_251 = 0; v_i_251 < v_TP_237; v_i_251 = (1 + v_i_251)) {
        /* map_seq */
        for (int v_i_252 = 0; v_i_252 < v_TP_238; v_i_252 = (1 + v_i_252)) {
          v__255_0 = grad(v__253[(1 + (v_i_252 % v_TP_238) + (4096 * v_TP_237 * v_gl_id_249) + (4096 * (((4094 / v_TP_238) * (v_i_251 % (2 + v_TP_237))) / (4094 / v_TP_238))) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * (v_i_251 % (2 + v_TP_237)))) % (4094 / v_TP_238))))], v__253[(1 + ((v_i_252 + (2 * v_TP_238)) % v_TP_238) + (4096 * v_TP_237 * v_gl_id_249) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (4096 * (((4094 / v_TP_238) * ((2 + v_i_251) % (2 + v_TP_237))) / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((2 + v_i_251) % (2 + v_TP_237)))) % (4094 / v_TP_238))))], v__253[(((v_TP_238 + v_i_252) % v_TP_238) + (4096 * v_TP_237 * v_gl_id_249) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (4096 * (((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237))) / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237)))) % (4094 / v_TP_238))))], v__253[(2 + ((v_TP_238 + v_i_252) % v_TP_238) + (4096 * v_TP_237 * v_gl_id_249) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (4096 * (((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237))) / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237)))) % (4094 / v_TP_238))))], v__253[(1 + ((v_TP_238 + v_i_252) % v_TP_238) + (4096 * v_TP_237 * v_gl_id_249) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (4096 * (((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237))) / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((1 + v_i_251) % (2 + v_TP_237)))) % (4094 / v_TP_238))))]); 
          v__256[(4097 + v_i_252 + (4096 * v_TP_237 * v_gl_id_249) + (-16777216 * (((v_gl_id_250 / (4094 / v_TP_238)) + (((4094 / v_TP_238) * ((v_i_251 + (v_TP_237 * v_gl_id_250)) % v_TP_237)) / (4094 / v_TP_238)) + (v_TP_237 * v_gl_id_249)) / 4095)) + (-4096 * ((v_i_252 + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((v_i_251 + (v_TP_237 * v_gl_id_250)) % v_TP_237))) % (4094 / v_TP_238)))) / 4095)) + (4096 * (v_gl_id_250 / (4094 / v_TP_238))) + (4096 * (((4094 / v_TP_238) * ((v_i_251 + (v_TP_237 * v_gl_id_250)) % v_TP_237)) / (4094 / v_TP_238))) + (v_TP_238 * ((v_gl_id_250 + ((4094 / v_TP_238) * ((v_i_251 + (v_TP_237 * v_gl_id_250)) % v_TP_237))) % (4094 / v_TP_238))))] = id(v__255_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


