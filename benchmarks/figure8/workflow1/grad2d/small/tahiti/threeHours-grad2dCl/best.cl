// High-level hash: cf9fea2a31fa72c45f3d3100da7343e7a4f3b05761ff36852c5bc3fb51a49c8f
// Low-level hash: 1b1065b5a02c7918d9b5ef3d07d3e5913dc3339f05449991c8ca0c9de13c3c2f
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 4094; v_gl_id_12 = (v_gl_id_12 + 4096)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 4094; v_gl_id_13 = (v_gl_id_13 + 2048)) {
      v__16 = grad(v__14[(1 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4096 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4098 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4097 + v_gl_id_13 + (4096 * v_gl_id_12))]); 
      v__17[(4097 + v_gl_id_13 + (4096 * v_gl_id_12))] = id(v__16); 
    }
  }
}}
