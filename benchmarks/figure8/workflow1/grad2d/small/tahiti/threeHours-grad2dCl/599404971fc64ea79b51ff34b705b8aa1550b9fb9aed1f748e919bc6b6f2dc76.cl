
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: 599404971fc64ea79b51ff34b705b8aa1550b9fb9aed1f748e919bc6b6f2dc76

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__57, global float* v__60){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__59_0;
  
  for (int v_gl_id_53 = get_global_id(1); v_gl_id_53 < (4094 / v_TP_41); v_gl_id_53 = (v_gl_id_53 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_54 = get_global_id(0); v_gl_id_54 < (4094 / v_TP_42); v_gl_id_54 = (v_gl_id_54 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_55 = 0; v_i_55 < v_TP_41; v_i_55 = (1 + v_i_55)) {
        /* map_seq */
        for (int v_i_56 = 0; v_i_56 < v_TP_42; v_i_56 = (1 + v_i_56)) {
          v__59_0 = grad(v__57[(1 + (v_i_56 % v_TP_42) + (4096 * v_TP_41 * v_gl_id_53) + (4096 * (((4094 / v_TP_42) * (v_i_55 % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * (v_i_55 % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(1 + ((v_i_56 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_gl_id_53) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(((v_TP_42 + v_i_56) % v_TP_42) + (4096 * v_TP_41 * v_gl_id_53) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(2 + ((v_TP_42 + v_i_56) % v_TP_42) + (4096 * v_TP_41 * v_gl_id_53) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(1 + ((v_TP_42 + v_i_56) % v_TP_42) + (4096 * v_TP_41 * v_gl_id_53) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))]); 
          v__60[(4097 + v_i_56 + (4096 * v_TP_41 * v_gl_id_53) + (-16777216 * (((v_gl_id_54 / (4094 / v_TP_42)) + (((4094 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41)) / (4094 / v_TP_42)) + (v_TP_41 * v_gl_id_53)) / 4095)) + (-4096 * ((v_i_56 + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41))) % (4094 / v_TP_42)))) / 4095)) + (4096 * (v_gl_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41)) / (4094 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((4094 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41))) % (4094 / v_TP_42))))] = id(v__59_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


