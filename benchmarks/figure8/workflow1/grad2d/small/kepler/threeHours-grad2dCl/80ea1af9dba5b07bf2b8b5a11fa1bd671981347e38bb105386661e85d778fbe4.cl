
// High-level hash: 3e86bf4262017665dc53d7c0145e6fa20a7301215072af4776a8b0c858fb2a5d
// Low-level hash: 80ea1af9dba5b07bf2b8b5a11fa1bd671981347e38bb105386661e85d778fbe4

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__57, global float* v__60){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__59_0;
  
  for (int v_wg_id_53 = get_group_id(1); v_wg_id_53 < (4094 / v_TP_41); v_wg_id_53 = (v_wg_id_53 + NUM_GROUPS_1)) {
    for (int v_wg_id_54 = get_group_id(0); v_wg_id_54 < (4094 / v_TP_42); v_wg_id_54 = (v_wg_id_54 + NUM_GROUPS_0)) {
      for (int v_l_id_55 = get_local_id(1); v_l_id_55 < v_TP_41; v_l_id_55 = (v_l_id_55 + LOCAL_SIZE_1)) {
        for (int v_l_id_56 = get_local_id(0); v_l_id_56 < v_TP_42; v_l_id_56 = (v_l_id_56 + LOCAL_SIZE_0)) {
          v__59_0 = grad(v__57[(1 + (v_l_id_56 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_53) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * (v_l_id_55 % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * (v_l_id_55 % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(1 + ((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_53) + (4096 * (((4094 / v_TP_42) * ((2 + v_l_id_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((2 + v_l_id_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(((v_TP_42 + v_l_id_56) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_53) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(2 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_53) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__57[(1 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_53) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((1 + v_l_id_55) % (2 + v_TP_41)))) % (4094 / v_TP_42))))]); 
          v__60[(4097 + v_l_id_56 + (-4096 * ((v_l_id_56 + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41))) % (4094 / v_TP_42)))) / 4095)) + (4096 * v_TP_41 * v_wg_id_53) + (-16777216 * (((((4094 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41)) / (4094 / v_TP_42)) + (v_wg_id_54 / (4094 / v_TP_42)) + (v_TP_41 * v_wg_id_53)) / 4095)) + (4096 * (((4094 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41)) / (4094 / v_TP_42))) + (4096 * (v_wg_id_54 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((4094 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41))) % (4094 / v_TP_42))))] = id(v__59_0); 
        }
      }
    }
  }
}}


