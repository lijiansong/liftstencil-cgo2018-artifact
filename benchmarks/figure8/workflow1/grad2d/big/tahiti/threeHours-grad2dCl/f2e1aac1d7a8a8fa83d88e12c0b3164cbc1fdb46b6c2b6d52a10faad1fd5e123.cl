
// High-level hash: 90c18c8a78a82c9e9f620124d26c4f8d01f5527610d104730b0f5de20d24c309
// Low-level hash: f2e1aac1d7a8a8fa83d88e12c0b3164cbc1fdb46b6c2b6d52a10faad1fd5e123

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_359" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_358" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__401, global float* v__405){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__402[(4+(2*v_TP_358)+(2*v_TP_359)+(v_TP_358*v_TP_359))];
  /* Typed Value memory */
  /* Private Memory */
  float v__404_0;
  
  for (int v_wg_id_395 = get_group_id(1); v_wg_id_395 < (8190 / v_TP_358); v_wg_id_395 = (v_wg_id_395 + NUM_GROUPS_1)) {
    for (int v_wg_id_396 = get_group_id(0); v_wg_id_396 < (8190 / v_TP_359); v_wg_id_396 = (v_wg_id_396 + NUM_GROUPS_0)) {
      for (int v_l_id_397 = get_local_id(1); v_l_id_397 < (2 + v_TP_358); v_l_id_397 = (v_l_id_397 + LOCAL_SIZE_1)) {
        for (int v_l_id_398 = get_local_id(0); v_l_id_398 < (2 + v_TP_359); v_l_id_398 = (v_l_id_398 + LOCAL_SIZE_0)) {
          v__402[(v_l_id_398 + (2 * v_l_id_397) + (v_TP_359 * v_l_id_397))] = idfloat(v__401[(v_l_id_398 + (8192 * v_TP_358 * v_wg_id_395) + (8192 * (v_wg_id_396 / (8190 / v_TP_359))) + (8192 * (((8190 / v_TP_359) * (v_l_id_397 % (2 + v_TP_358))) / (8190 / v_TP_359))) + (v_TP_359 * ((v_wg_id_396 + ((8190 / v_TP_359) * (v_l_id_397 % (2 + v_TP_358)))) % (8190 / v_TP_359))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_399 = get_local_id(1); v_l_id_399 < v_TP_358; v_l_id_399 = (v_l_id_399 + LOCAL_SIZE_1)) {
        for (int v_l_id_400 = get_local_id(0); v_l_id_400 < v_TP_359; v_l_id_400 = (v_l_id_400 + LOCAL_SIZE_0)) {
          v__404_0 = grad(v__402[(1 + (v_l_id_400 % v_TP_359) + (2 * v_l_id_399) + (v_TP_359 * v_l_id_399))], v__402[(5 + v_l_id_400 + (2 * v_TP_359) + (2 * v_l_id_399) + (v_TP_359 * v_l_id_399))], v__402[(2 + v_TP_359 + ((v_TP_359 + v_l_id_400) % v_TP_359) + (2 * v_l_id_399) + (v_TP_359 * v_l_id_399))], v__402[(4 + v_TP_359 + ((v_TP_359 + v_l_id_400) % v_TP_359) + (2 * v_l_id_399) + (v_TP_359 * v_l_id_399))], v__402[(3 + v_TP_359 + ((v_TP_359 + v_l_id_400) % v_TP_359) + (2 * v_l_id_399) + (v_TP_359 * v_l_id_399))]); 
          v__405[(8193 + v_l_id_400 + (-8192 * ((v_l_id_400 + (v_TP_359 * ((v_wg_id_396 + ((8190 / v_TP_359) * ((v_l_id_399 + (v_TP_358 * v_wg_id_396)) % v_TP_358))) % (8190 / v_TP_359)))) / 8191)) + (8192 * v_TP_358 * v_wg_id_395) + (-67108864 * (((((8190 / v_TP_359) * ((v_l_id_399 + (v_TP_358 * v_wg_id_396)) % v_TP_358)) / (8190 / v_TP_359)) + (v_wg_id_396 / (8190 / v_TP_359)) + (v_TP_358 * v_wg_id_395)) / 8191)) + (8192 * (((8190 / v_TP_359) * ((v_l_id_399 + (v_TP_358 * v_wg_id_396)) % v_TP_358)) / (8190 / v_TP_359))) + (8192 * (v_wg_id_396 / (8190 / v_TP_359))) + (v_TP_359 * ((v_wg_id_396 + ((8190 / v_TP_359) * ((v_l_id_399 + (v_TP_358 * v_wg_id_396)) % v_TP_358))) % (8190 / v_TP_359))))] = id(v__404_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


