
// High-level hash: cf319ab820ad4208bfb376fe2b327fcf68f6289ab1beadeb50bdd5956dc279ac
// Low-level hash: f2e1aac1d7a8a8fa83d88e12c0b3164cbc1fdb46b6c2b6d52a10faad1fd5e123

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_75" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_74" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__119, global float* v__153){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__141[(4+(2*v_TP_74)+(2*v_TP_75)+(v_TP_74*v_TP_75))];
  /* Typed Value memory */
  /* Private Memory */
  float v__151_0;
  
  for (int v_wg_id_113 = get_group_id(1); v_wg_id_113 < (8190 / v_TP_74); v_wg_id_113 = (v_wg_id_113 + NUM_GROUPS_1)) {
    for (int v_wg_id_114 = get_group_id(0); v_wg_id_114 < (8190 / v_TP_75); v_wg_id_114 = (v_wg_id_114 + NUM_GROUPS_0)) {
      for (int v_l_id_115 = get_local_id(1); v_l_id_115 < (2 + v_TP_74); v_l_id_115 = (v_l_id_115 + LOCAL_SIZE_1)) {
        for (int v_l_id_116 = get_local_id(0); v_l_id_116 < (2 + v_TP_75); v_l_id_116 = (v_l_id_116 + LOCAL_SIZE_0)) {
          v__141[(v_l_id_116 + (2 * v_l_id_115) + (v_TP_75 * v_l_id_115))] = idfloat(v__119[(v_l_id_116 + (8192 * v_TP_74 * v_wg_id_113) + (8192 * (v_wg_id_114 / (8190 / v_TP_75))) + (8192 * (((8190 / v_TP_75) * (v_l_id_115 % (2 + v_TP_74))) / (8190 / v_TP_75))) + (v_TP_75 * ((v_wg_id_114 + ((8190 / v_TP_75) * (v_l_id_115 % (2 + v_TP_74)))) % (8190 / v_TP_75))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_117 = get_local_id(1); v_l_id_117 < v_TP_74; v_l_id_117 = (v_l_id_117 + LOCAL_SIZE_1)) {
        for (int v_l_id_118 = get_local_id(0); v_l_id_118 < v_TP_75; v_l_id_118 = (v_l_id_118 + LOCAL_SIZE_0)) {
          v__151_0 = grad(v__141[(1 + (v_l_id_118 % v_TP_75) + (2 * v_l_id_117) + (v_TP_75 * v_l_id_117))], v__141[(5 + v_l_id_118 + (2 * v_TP_75) + (2 * v_l_id_117) + (v_TP_75 * v_l_id_117))], v__141[(2 + v_TP_75 + ((v_TP_75 + v_l_id_118) % v_TP_75) + (2 * v_l_id_117) + (v_TP_75 * v_l_id_117))], v__141[(4 + v_TP_75 + ((v_TP_75 + v_l_id_118) % v_TP_75) + (2 * v_l_id_117) + (v_TP_75 * v_l_id_117))], v__141[(3 + v_TP_75 + ((v_TP_75 + v_l_id_118) % v_TP_75) + (2 * v_l_id_117) + (v_TP_75 * v_l_id_117))]); 
          v__153[(8193 + v_l_id_118 + (-8192 * ((v_l_id_118 + (v_TP_75 * ((v_wg_id_114 + ((8190 / v_TP_75) * ((v_l_id_117 + (v_TP_74 * v_wg_id_114)) % v_TP_74))) % (8190 / v_TP_75)))) / 8191)) + (8192 * v_TP_74 * v_wg_id_113) + (-67108864 * (((((8190 / v_TP_75) * ((v_l_id_117 + (v_TP_74 * v_wg_id_114)) % v_TP_74)) / (8190 / v_TP_75)) + (v_wg_id_114 / (8190 / v_TP_75)) + (v_TP_74 * v_wg_id_113)) / 8191)) + (8192 * (((8190 / v_TP_75) * ((v_l_id_117 + (v_TP_74 * v_wg_id_114)) % v_TP_74)) / (8190 / v_TP_75))) + (8192 * (v_wg_id_114 / (8190 / v_TP_75))) + (v_TP_75 * ((v_wg_id_114 + ((8190 / v_TP_75) * ((v_l_id_117 + (v_TP_74 * v_wg_id_114)) % v_TP_74))) % (8190 / v_TP_75))))] = id(v__151_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


