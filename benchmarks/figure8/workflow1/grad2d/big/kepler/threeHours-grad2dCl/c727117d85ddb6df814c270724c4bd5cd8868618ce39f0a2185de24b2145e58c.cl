
// High-level hash: cf319ab820ad4208bfb376fe2b327fcf68f6289ab1beadeb50bdd5956dc279ac
// Low-level hash: c727117d85ddb6df814c270724c4bd5cd8868618ce39f0a2185de24b2145e58c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_64" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_63" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__125, global float* v__130){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__128_0;
  
  for (int v_gl_id_121 = get_global_id(1); v_gl_id_121 < (8190 / v_TP_63); v_gl_id_121 = (v_gl_id_121 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_122 = get_global_id(0); v_gl_id_122 < (8190 / v_TP_64); v_gl_id_122 = (v_gl_id_122 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_123 = 0; v_i_123 < v_TP_63; v_i_123 = (1 + v_i_123)) {
        /* map_seq */
        for (int v_i_124 = 0; v_i_124 < v_TP_64; v_i_124 = (1 + v_i_124)) {
          v__128_0 = grad(v__125[(1 + (v_i_124 % v_TP_64) + (8192 * v_TP_63 * v_gl_id_121) + (8192 * (((8190 / v_TP_64) * (v_i_123 % (2 + v_TP_63))) / (8190 / v_TP_64))) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * (v_i_123 % (2 + v_TP_63)))) % (8190 / v_TP_64))))], v__125[(1 + ((v_i_124 + (2 * v_TP_64)) % v_TP_64) + (8192 * v_TP_63 * v_gl_id_121) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (8192 * (((8190 / v_TP_64) * ((2 + v_i_123) % (2 + v_TP_63))) / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((2 + v_i_123) % (2 + v_TP_63)))) % (8190 / v_TP_64))))], v__125[(((v_TP_64 + v_i_124) % v_TP_64) + (8192 * v_TP_63 * v_gl_id_121) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (8192 * (((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63))) / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63)))) % (8190 / v_TP_64))))], v__125[(2 + ((v_TP_64 + v_i_124) % v_TP_64) + (8192 * v_TP_63 * v_gl_id_121) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (8192 * (((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63))) / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63)))) % (8190 / v_TP_64))))], v__125[(1 + ((v_TP_64 + v_i_124) % v_TP_64) + (8192 * v_TP_63 * v_gl_id_121) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (8192 * (((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63))) / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((1 + v_i_123) % (2 + v_TP_63)))) % (8190 / v_TP_64))))]); 
          v__130[(8193 + v_i_124 + (8192 * v_TP_63 * v_gl_id_121) + (-67108864 * (((v_gl_id_122 / (8190 / v_TP_64)) + (((8190 / v_TP_64) * ((v_i_123 + (v_TP_63 * v_gl_id_122)) % v_TP_63)) / (8190 / v_TP_64)) + (v_TP_63 * v_gl_id_121)) / 8191)) + (-8192 * ((v_i_124 + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((v_i_123 + (v_TP_63 * v_gl_id_122)) % v_TP_63))) % (8190 / v_TP_64)))) / 8191)) + (8192 * (v_gl_id_122 / (8190 / v_TP_64))) + (8192 * (((8190 / v_TP_64) * ((v_i_123 + (v_TP_63 * v_gl_id_122)) % v_TP_63)) / (8190 / v_TP_64))) + (v_TP_64 * ((v_gl_id_122 + ((8190 / v_TP_64) * ((v_i_123 + (v_TP_63 * v_gl_id_122)) % v_TP_63))) % (8190 / v_TP_64))))] = id(v__128_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


