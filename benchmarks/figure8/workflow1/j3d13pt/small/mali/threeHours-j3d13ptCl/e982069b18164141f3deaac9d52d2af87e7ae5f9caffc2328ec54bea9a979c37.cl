
// High-level hash: 952ed6e585e8c66c2ee49bae315e8147db8650df17b5cb3b39f687a1192470de
// Low-level hash: e982069b18164141f3deaac9d52d2af87e7ae5f9caffc2328ec54bea9a979c37

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
  return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__50, global float* v__58){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__57_0;
  
  for (int v_gl_id_47 = get_global_id(0); v_gl_id_47 < 252; v_gl_id_47 = (v_gl_id_47 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_48 = get_global_id(1); v_gl_id_48 < 252; v_gl_id_48 = (v_gl_id_48 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_49 = 0; v_i_49 < 252; v_i_49 = (1 + v_i_49)) {
        v__57_0 = jacobi(v__50[(131588 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131587 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131585 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131584 + v_i_49 + (256 * v_gl_id_48) + (65536 * v_gl_id_47))], v__50[(132098 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131842 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131330 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(131074 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(262658 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(197122 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(66050 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(514 + v_i_49 + (256 * v_gl_id_48) + (65536 * v_gl_id_47))], v__50[(131586 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))]); 
        v__58[(131586 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))] = id(v__57_0); 
      }
      /* end map_seq */
    }
  }
}}


