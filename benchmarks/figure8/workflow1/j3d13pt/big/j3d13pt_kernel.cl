__kernel void kernel0(__global float *inref, __global float *outref)
{
    int b0 = get_group_id(0), b1 = get_group_id(1);
    int t0 = get_local_id(0), t1 = get_local_id(1), t2 = get_local_id(2);

    #define ppcg_min(x,y)    min((__typeof__(x + y)) x, (__typeof__(x + y)) y)
    #define ppcg_max(x,y)    max((__typeof__(x + y)) x, (__typeof__(x + y)) y)
    if (32 * b0 + t0 >= 2 && 32 * b0 + t0 <= 509)
      for (int c2 = 0; c2 <= 509; c2 += 32)
        for (int c4 = ppcg_max(t1, ((t1 + 2) % 4) - 32 * b1 + 2); c4 <= ppcg_min(31, -32 * b1 + 509); c4 += 4)
          for (int c5 = ppcg_max(t2, ((t2 + c2 + 2) % 4) - c2 + 2); c5 <= ppcg_min(31, -c2 + 509); c5 += 4)
            outref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)] = (((((((((((((0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5 + 2)]) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5 + 1)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5 - 1)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5 - 2)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4 + 2)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4 - 1)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4 - 2)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0 + 2) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0 - 1) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)])) + (0.083f * inref[((32 * b0 + t0 - 2) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)])) - (0.996f * inref[((32 * b0 + t0) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5)]));
}
