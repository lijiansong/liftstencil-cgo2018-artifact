
// High-level hash: ad6f020667d982b1a7373026abbcb0fefe63ee24c3bbe7ed3324fcbef02cd46f
// Low-level hash: 56606dbe9003e1c33dc9235f5c029f72a167c147abae14580c8b8bf36b60f0ce

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__68, global float* v__71){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__70_0;
  
  for (int v_gl_id_64 = get_global_id(1); v_gl_id_64 < (8190 / v_TP_41); v_gl_id_64 = (v_gl_id_64 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_65 = get_global_id(0); v_gl_id_65 < (8190 / v_TP_42); v_gl_id_65 = (v_gl_id_65 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_66 = 0; v_i_66 < v_TP_41; v_i_66 = (1 + v_i_66)) {
        /* map_seq */
        for (int v_i_67 = 0; v_i_67 < v_TP_42; v_i_67 = (1 + v_i_67)) {
          v__70_0 = jacobi(v__68[(1 + (v_i_67 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_64) + (8192 * (((8190 / v_TP_42) * (v_i_66 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * (v_i_66 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__68[(1 + ((v_i_67 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_66) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((2 + v_i_66) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__68[(((v_TP_42 + v_i_67) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__68[(2 + ((v_TP_42 + v_i_67) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__68[(1 + ((v_TP_42 + v_i_67) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((1 + v_i_66) % (2 + v_TP_41)))) % (8190 / v_TP_42))))]); 
          v__71[(8193 + v_i_67 + (8192 * v_TP_41 * v_gl_id_64) + (-67108864 * (((v_gl_id_65 / (8190 / v_TP_42)) + (((8190 / v_TP_42) * ((v_i_66 + (v_TP_41 * v_gl_id_65)) % v_TP_41)) / (8190 / v_TP_42)) + (v_TP_41 * v_gl_id_64)) / 8191)) + (-8192 * ((v_i_67 + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((v_i_66 + (v_TP_41 * v_gl_id_65)) % v_TP_41))) % (8190 / v_TP_42)))) / 8191)) + (8192 * (v_gl_id_65 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((v_i_66 + (v_TP_41 * v_gl_id_65)) % v_TP_41)) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_65 + ((8190 / v_TP_42) * ((v_i_66 + (v_TP_41 * v_gl_id_65)) % v_TP_41))) % (8190 / v_TP_42))))] = id(v__70_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


