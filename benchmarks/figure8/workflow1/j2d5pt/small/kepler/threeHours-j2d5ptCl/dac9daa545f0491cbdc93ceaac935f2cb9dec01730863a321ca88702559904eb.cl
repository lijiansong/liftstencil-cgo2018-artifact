
// High-level hash: f62cb1a6a4509dd2009c2f06b29816ce49d9f7184b15079f6abe9de74b4855b8
// Low-level hash: dac9daa545f0491cbdc93ceaac935f2cb9dec01730863a321ca88702559904eb

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__62, global float* v__65){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__64_0;
  
  for (int v_wg_id_58 = get_group_id(1); v_wg_id_58 < (4094 / v_TP_41); v_wg_id_58 = (v_wg_id_58 + NUM_GROUPS_1)) {
    for (int v_wg_id_59 = get_group_id(0); v_wg_id_59 < (4094 / v_TP_42); v_wg_id_59 = (v_wg_id_59 + NUM_GROUPS_0)) {
      for (int v_l_id_60 = get_local_id(1); v_l_id_60 < v_TP_41; v_l_id_60 = (v_l_id_60 + LOCAL_SIZE_1)) {
        for (int v_l_id_61 = get_local_id(0); v_l_id_61 < v_TP_42; v_l_id_61 = (v_l_id_61 + LOCAL_SIZE_0)) {
          v__64_0 = jacobi(v__62[(1 + (v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_58) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (4096 * (((4094 / v_TP_42) * (v_l_id_60 % (2 + v_TP_41))) / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * (v_l_id_60 % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__62[(1 + ((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_58) + (4096 * (((4094 / v_TP_42) * ((2 + v_l_id_60) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((2 + v_l_id_60) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__62[(((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_58) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__62[(2 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_58) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41)))) % (4094 / v_TP_42))))], v__62[(1 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_58) + (4096 * (((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41))) / (4094 / v_TP_42))) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((1 + v_l_id_60) % (2 + v_TP_41)))) % (4094 / v_TP_42))))]); 
          v__65[(4097 + v_l_id_61 + (-4096 * ((v_l_id_61 + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41))) % (4094 / v_TP_42)))) / 4095)) + (4096 * v_TP_41 * v_wg_id_58) + (-16777216 * (((((4094 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41)) / (4094 / v_TP_42)) + (v_wg_id_59 / (4094 / v_TP_42)) + (v_TP_41 * v_wg_id_58)) / 4095)) + (4096 * (((4094 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41)) / (4094 / v_TP_42))) + (4096 * (v_wg_id_59 / (4094 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4094 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41))) % (4094 / v_TP_42))))] = id(v__64_0); 
        }
      }
    }
  }
}}


