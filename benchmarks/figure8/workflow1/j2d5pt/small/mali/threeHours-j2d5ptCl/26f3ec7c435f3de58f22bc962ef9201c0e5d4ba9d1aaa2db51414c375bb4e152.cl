
// High-level hash: d9610667c34db21b3b85dc5bdf67a09b199cf48e2fe915ba50f720758fe1c5d7
// Low-level hash: 26f3ec7c435f3de58f22bc962ef9201c0e5d4ba9d1aaa2db51414c375bb4e152

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_65" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_64" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__80, global float* v__83){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__82_0;
  
  for (int v_wg_id_76 = get_group_id(0); v_wg_id_76 < (4094 / v_TP_64); v_wg_id_76 = (v_wg_id_76 + NUM_GROUPS_0)) {
    for (int v_wg_id_77 = get_group_id(1); v_wg_id_77 < (4094 / v_TP_65); v_wg_id_77 = (v_wg_id_77 + NUM_GROUPS_1)) {
      for (int v_l_id_78 = get_local_id(0); v_l_id_78 < v_TP_64; v_l_id_78 = (v_l_id_78 + LOCAL_SIZE_0)) {
        for (int v_l_id_79 = get_local_id(1); v_l_id_79 < v_TP_65; v_l_id_79 = (v_l_id_79 + LOCAL_SIZE_1)) {
          v__82_0 = jacobi(v__80[(1 + (v_l_id_79 % v_TP_65) + (4096 * v_TP_64 * v_wg_id_76) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (4096 * (((4094 / v_TP_65) * (v_l_id_78 % (2 + v_TP_64))) / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * (v_l_id_78 % (2 + v_TP_64)))) % (4094 / v_TP_65))))], v__80[(1 + ((v_l_id_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_wg_id_76) + (4096 * (((4094 / v_TP_65) * ((2 + v_l_id_78) % (2 + v_TP_64))) / (4094 / v_TP_65))) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((2 + v_l_id_78) % (2 + v_TP_64)))) % (4094 / v_TP_65))))], v__80[(((v_TP_65 + v_l_id_79) % v_TP_65) + (4096 * v_TP_64 * v_wg_id_76) + (4096 * (((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64))) / (4094 / v_TP_65))) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64)))) % (4094 / v_TP_65))))], v__80[(2 + ((v_TP_65 + v_l_id_79) % v_TP_65) + (4096 * v_TP_64 * v_wg_id_76) + (4096 * (((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64))) / (4094 / v_TP_65))) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64)))) % (4094 / v_TP_65))))], v__80[(1 + ((v_TP_65 + v_l_id_79) % v_TP_65) + (4096 * v_TP_64 * v_wg_id_76) + (4096 * (((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64))) / (4094 / v_TP_65))) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((1 + v_l_id_78) % (2 + v_TP_64)))) % (4094 / v_TP_65))))]); 
          v__83[(4097 + v_l_id_79 + (-4096 * ((v_l_id_79 + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((v_l_id_78 + (v_TP_64 * v_wg_id_77)) % v_TP_64))) % (4094 / v_TP_65)))) / 4095)) + (4096 * v_TP_64 * v_wg_id_76) + (-16777216 * (((((4094 / v_TP_65) * ((v_l_id_78 + (v_TP_64 * v_wg_id_77)) % v_TP_64)) / (4094 / v_TP_65)) + (v_wg_id_77 / (4094 / v_TP_65)) + (v_TP_64 * v_wg_id_76)) / 4095)) + (4096 * (((4094 / v_TP_65) * ((v_l_id_78 + (v_TP_64 * v_wg_id_77)) % v_TP_64)) / (4094 / v_TP_65))) + (4096 * (v_wg_id_77 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_77 + ((4094 / v_TP_65) * ((v_l_id_78 + (v_TP_64 * v_wg_id_77)) % v_TP_64))) % (4094 / v_TP_65))))] = id(v__82_0); 
        }
      }
    }
  }
}}


