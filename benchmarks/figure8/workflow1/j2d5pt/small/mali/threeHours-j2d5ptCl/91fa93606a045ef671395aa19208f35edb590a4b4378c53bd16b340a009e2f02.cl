
// High-level hash: d9610667c34db21b3b85dc5bdf67a09b199cf48e2fe915ba50f720758fe1c5d7
// Low-level hash: 91fa93606a045ef671395aa19208f35edb590a4b4378c53bd16b340a009e2f02

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_411" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_410" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__447, global float* v__450){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__449_0;
  
  for (int v_gl_id_443 = get_global_id(0); v_gl_id_443 < (4094 / v_TP_410); v_gl_id_443 = (v_gl_id_443 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_444 = get_global_id(1); v_gl_id_444 < (4094 / v_TP_411); v_gl_id_444 = (v_gl_id_444 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_445 = 0; v_i_445 < v_TP_410; v_i_445 = (1 + v_i_445)) {
        /* map_seq */
        for (int v_i_446 = 0; v_i_446 < v_TP_411; v_i_446 = (1 + v_i_446)) {
          v__449_0 = jacobi(v__447[(1 + (v_i_446 % v_TP_411) + (4096 * v_TP_410 * v_gl_id_443) + (4096 * (((4094 / v_TP_411) * (v_i_445 % (2 + v_TP_410))) / (4094 / v_TP_411))) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * (v_i_445 % (2 + v_TP_410)))) % (4094 / v_TP_411))))], v__447[(1 + ((v_i_446 + (2 * v_TP_411)) % v_TP_411) + (4096 * v_TP_410 * v_gl_id_443) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (4096 * (((4094 / v_TP_411) * ((2 + v_i_445) % (2 + v_TP_410))) / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((2 + v_i_445) % (2 + v_TP_410)))) % (4094 / v_TP_411))))], v__447[(((v_TP_411 + v_i_446) % v_TP_411) + (4096 * v_TP_410 * v_gl_id_443) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (4096 * (((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410))) / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410)))) % (4094 / v_TP_411))))], v__447[(2 + ((v_TP_411 + v_i_446) % v_TP_411) + (4096 * v_TP_410 * v_gl_id_443) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (4096 * (((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410))) / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410)))) % (4094 / v_TP_411))))], v__447[(1 + ((v_TP_411 + v_i_446) % v_TP_411) + (4096 * v_TP_410 * v_gl_id_443) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (4096 * (((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410))) / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((1 + v_i_445) % (2 + v_TP_410)))) % (4094 / v_TP_411))))]); 
          v__450[(4097 + v_i_446 + (4096 * v_TP_410 * v_gl_id_443) + (-16777216 * (((v_gl_id_444 / (4094 / v_TP_411)) + (((4094 / v_TP_411) * ((v_i_445 + (v_TP_410 * v_gl_id_444)) % v_TP_410)) / (4094 / v_TP_411)) + (v_TP_410 * v_gl_id_443)) / 4095)) + (-4096 * ((v_i_446 + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((v_i_445 + (v_TP_410 * v_gl_id_444)) % v_TP_410))) % (4094 / v_TP_411)))) / 4095)) + (4096 * (v_gl_id_444 / (4094 / v_TP_411))) + (4096 * (((4094 / v_TP_411) * ((v_i_445 + (v_TP_410 * v_gl_id_444)) % v_TP_410)) / (4094 / v_TP_411))) + (v_TP_411 * ((v_gl_id_444 + ((4094 / v_TP_411) * ((v_i_445 + (v_TP_410 * v_gl_id_444)) % v_TP_410))) % (4094 / v_TP_411))))] = id(v__449_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


