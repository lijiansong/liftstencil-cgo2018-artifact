
// High-level hash: 134ba95f430b3b2f339c99f0eaf746a910c5005430cef6c4f89ba212bbfcc835
// Low-level hash: ed278aca3ad77af1a95b82983204d9f44c6aae7b28fa84743e2a08abcb4f0fd9

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__50, global float* v__68){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__67_0;
  
  for (int v_gl_id_47 = get_global_id(0); v_gl_id_47 < 254; v_gl_id_47 = (v_gl_id_47 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_48 = get_global_id(1); v_gl_id_48 < 254; v_gl_id_48 = (v_gl_id_48 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_49 = 0; v_i_49 < 254; v_i_49 = (1 + v_i_49)) {
        v__67_0 = jacobi(v__50[(65793 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(65537 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(66049 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(65794 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))], v__50[(65792 + v_i_49 + (256 * v_gl_id_48) + (65536 * v_gl_id_47))], v__50[(257 + v_i_49 + (256 * v_gl_id_48) + (65536 * v_gl_id_47))], v__50[(131329 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))]); 
        v__68[(65793 + v_i_49 + (65536 * v_gl_id_47) + (256 * v_gl_id_48))] = id(v__67_0); 
      }
      /* end map_seq */
    }
  }
}}


