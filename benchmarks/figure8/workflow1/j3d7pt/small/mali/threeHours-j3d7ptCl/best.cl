// High-level hash: 134ba95f430b3b2f339c99f0eaf746a910c5005430cef6c4f89ba212bbfcc835
// Low-level hash: d0961838d9105ccc00b02d417d5bf86355c6dcb9253c8df983acc7e42206f966
float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__125, global float* v__128){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__127; 
  for (int v_gl_id_122 = get_global_id(2); v_gl_id_122 < 254; v_gl_id_122 = (v_gl_id_122 + 256)) {
    for (int v_gl_id_123 = get_global_id(1); v_gl_id_123 < 254; v_gl_id_123 = (v_gl_id_123 + 256)) {
      for (int v_gl_id_124 = get_global_id(0); v_gl_id_124 < 254; v_gl_id_124 = (v_gl_id_124 + 8)) {
        v__127 = jacobi(v__125[(65793 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))], v__125[(65537 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))], v__125[(66049 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))], v__125[(65794 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))], v__125[(65792 + v_gl_id_124 + (256 * v_gl_id_123) + (65536 * v_gl_id_122))], v__125[(257 + v_gl_id_124 + (256 * v_gl_id_123) + (65536 * v_gl_id_122))], v__125[(131329 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))]); 
        v__128[(65793 + v_gl_id_124 + (65536 * v_gl_id_122) + (256 * v_gl_id_123))] = id(v__127); 
      }
    }
  }
}}
