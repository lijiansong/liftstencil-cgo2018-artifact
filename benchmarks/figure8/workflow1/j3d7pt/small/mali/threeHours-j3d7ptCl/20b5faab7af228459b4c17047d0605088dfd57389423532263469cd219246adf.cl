
// High-level hash: 134ba95f430b3b2f339c99f0eaf746a910c5005430cef6c4f89ba212bbfcc835
// Low-level hash: 20b5faab7af228459b4c17047d0605088dfd57389423532263469cd219246adf

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__56, global float* v__66){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__65_0;
  
  for (int v_gl_id_53 = get_global_id(1); v_gl_id_53 < 254; v_gl_id_53 = (v_gl_id_53 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_54 = get_global_id(0); v_gl_id_54 < 254; v_gl_id_54 = (v_gl_id_54 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_55 = 0; v_i_55 < 254; v_i_55 = (1 + v_i_55)) {
        v__65_0 = jacobi(v__56[(65793 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))], v__56[(65537 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))], v__56[(66049 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))], v__56[(65794 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))], v__56[(65792 + v_i_55 + (256 * v_gl_id_54) + (65536 * v_gl_id_53))], v__56[(257 + v_i_55 + (256 * v_gl_id_54) + (65536 * v_gl_id_53))], v__56[(131329 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))]); 
        v__66[(65793 + v_i_55 + (65536 * v_gl_id_53) + (256 * v_gl_id_54))] = id(v__65_0); 
      }
      /* end map_seq */
    }
  }
}}


