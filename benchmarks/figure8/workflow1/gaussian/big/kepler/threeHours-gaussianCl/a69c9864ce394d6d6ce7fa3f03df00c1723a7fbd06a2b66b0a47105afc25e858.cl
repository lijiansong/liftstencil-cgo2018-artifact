
// High-level hash: bfab1cbf6d9d15128fbfb958c120c81491403df4ac45ed65e33661f888707a43
// Low-level hash: a69c9864ce394d6d6ce7fa3f03df00c1723a7fbd06a2b66b0a47105afc25e858

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_164" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"
#atf::tp name "v_TP_163" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__242, global float* v__252){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__250_0;
  
  for (int v_gl_id_237 = get_global_id(1); v_gl_id_237 < (8188 / v_TP_163); v_gl_id_237 = (v_gl_id_237 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_238 = get_global_id(0); v_gl_id_238 < (8188 / v_TP_164); v_gl_id_238 = (v_gl_id_238 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_240 = 0; v_i_240 < v_TP_163; v_i_240 = (1 + v_i_240)) {
        /* map_seq */
        for (int v_i_241 = 0; v_i_241 < v_TP_164; v_i_241 = (1 + v_i_241)) {
          v__250_0 = jacobi(v__242[((v_i_241 % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163))) / (8188 / v_TP_164))) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(1 + (v_i_241 % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163))) / (8188 / v_TP_164))) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(2 + (v_i_241 % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163))) / (8188 / v_TP_164))) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(3 + (v_i_241 % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163))) / (8188 / v_TP_164))) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(4 + (v_i_241 % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163))) / (8188 / v_TP_164))) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * (v_i_240 % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(((v_TP_164 + v_i_241) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(1 + ((v_TP_164 + v_i_241) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(2 + ((v_TP_164 + v_i_241) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(3 + ((v_TP_164 + v_i_241) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(4 + ((v_TP_164 + v_i_241) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((1 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(((v_i_241 + (2 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(1 + ((v_i_241 + (2 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(2 + ((v_i_241 + (2 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(3 + ((v_i_241 + (2 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(4 + ((v_i_241 + (2 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((2 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(((v_i_241 + (3 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(1 + ((v_i_241 + (3 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(2 + ((v_i_241 + (3 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(3 + ((v_i_241 + (3 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(4 + ((v_i_241 + (3 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((3 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(((v_i_241 + (4 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(1 + ((v_i_241 + (4 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(2 + ((v_i_241 + (4 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(3 + ((v_i_241 + (4 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))], v__242[(4 + ((v_i_241 + (4 * v_TP_164)) % v_TP_164) + (8192 * v_TP_163 * v_gl_id_237) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163))) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((4 + v_i_240) % (4 + v_TP_163)))) % (8188 / v_TP_164))))]); 
          v__252[(16386 + v_i_241 + (8192 * v_TP_163 * v_gl_id_237) + (-67108864 * (((v_gl_id_238 / (8188 / v_TP_164)) + (((8188 / v_TP_164) * ((v_i_240 + (v_TP_163 * v_gl_id_238)) % v_TP_163)) / (8188 / v_TP_164)) + (v_TP_163 * v_gl_id_237)) / 8191)) + (-8192 * ((v_i_241 + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((v_i_240 + (v_TP_163 * v_gl_id_238)) % v_TP_163))) % (8188 / v_TP_164)))) / 8191)) + (8192 * (v_gl_id_238 / (8188 / v_TP_164))) + (8192 * (((8188 / v_TP_164) * ((v_i_240 + (v_TP_163 * v_gl_id_238)) % v_TP_163)) / (8188 / v_TP_164))) + (v_TP_164 * ((v_gl_id_238 + ((8188 / v_TP_164) * ((v_i_240 + (v_TP_163 * v_gl_id_238)) % v_TP_163))) % (8188 / v_TP_164))))] = id(v__250_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


