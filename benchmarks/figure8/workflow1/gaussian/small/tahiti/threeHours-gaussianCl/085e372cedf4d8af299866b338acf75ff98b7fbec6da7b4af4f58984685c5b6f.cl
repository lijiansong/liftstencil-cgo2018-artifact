
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: 085e372cedf4d8af299866b338acf75ff98b7fbec6da7b4af4f58984685c5b6f

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_89" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_88" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__135, global float* v__141){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__138[(16+(4*v_TP_88)+(4*v_TP_89)+(v_TP_88*v_TP_89))];
  /* Typed Value memory */
  /* Private Memory */
  float v__140_0;
  
  for (int v_wg_id_127 = get_group_id(1); v_wg_id_127 < (4092 / v_TP_88); v_wg_id_127 = (v_wg_id_127 + NUM_GROUPS_1)) {
    for (int v_wg_id_128 = get_group_id(0); v_wg_id_128 < (4092 / v_TP_89); v_wg_id_128 = (v_wg_id_128 + NUM_GROUPS_0)) {
      for (int v_l_id_130 = get_local_id(1); v_l_id_130 < (4 + v_TP_88); v_l_id_130 = (v_l_id_130 + LOCAL_SIZE_1)) {
        for (int v_l_id_132 = get_local_id(0); v_l_id_132 < (4 + v_TP_89); v_l_id_132 = (v_l_id_132 + LOCAL_SIZE_0)) {
          v__138[(v_l_id_132 + (4 * v_l_id_130) + (v_TP_89 * v_l_id_130))] = idfloat(v__135[(v_l_id_132 + (4096 * v_TP_88 * v_wg_id_127) + (4096 * (v_wg_id_128 / (4092 / v_TP_89))) + (4096 * (((4092 / v_TP_89) * (v_l_id_130 % (4 + v_TP_88))) / (4092 / v_TP_89))) + (v_TP_89 * ((v_wg_id_128 + ((4092 / v_TP_89) * (v_l_id_130 % (4 + v_TP_88)))) % (4092 / v_TP_89))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_133 = get_local_id(1); v_l_id_133 < v_TP_88; v_l_id_133 = (v_l_id_133 + LOCAL_SIZE_1)) {
        for (int v_l_id_134 = get_local_id(0); v_l_id_134 < v_TP_89; v_l_id_134 = (v_l_id_134 + LOCAL_SIZE_0)) {
          v__140_0 = jacobi(v__138[((v_l_id_134 % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(1 + (v_l_id_134 % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(2 + (v_l_id_134 % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(3 + (v_l_id_134 % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(4 + (v_l_id_134 % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(4 + v_TP_89 + ((v_TP_89 + v_l_id_134) % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(5 + v_TP_89 + ((v_TP_89 + v_l_id_134) % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(6 + v_TP_89 + ((v_TP_89 + v_l_id_134) % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(7 + v_TP_89 + ((v_TP_89 + v_l_id_134) % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(8 + v_TP_89 + ((v_TP_89 + v_l_id_134) % v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(8 + v_l_id_134 + (2 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(9 + v_l_id_134 + (2 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(10 + v_l_id_134 + (2 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(11 + v_l_id_134 + (2 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(12 + v_l_id_134 + (2 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(12 + v_l_id_134 + (3 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(13 + v_l_id_134 + (3 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(14 + v_l_id_134 + (3 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(15 + v_l_id_134 + (3 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(16 + v_l_id_134 + (3 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(16 + v_l_id_134 + (4 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(17 + v_l_id_134 + (4 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(18 + v_l_id_134 + (4 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(19 + v_l_id_134 + (4 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))], v__138[(20 + v_l_id_134 + (4 * v_TP_89) + (4 * v_l_id_133) + (v_TP_89 * v_l_id_133))]); 
          v__141[(8194 + v_l_id_134 + (-4096 * ((v_l_id_134 + (v_TP_89 * ((v_wg_id_128 + ((4092 / v_TP_89) * ((v_l_id_133 + (v_TP_88 * v_wg_id_128)) % v_TP_88))) % (4092 / v_TP_89)))) / 4095)) + (4096 * v_TP_88 * v_wg_id_127) + (-16777216 * (((((4092 / v_TP_89) * ((v_l_id_133 + (v_TP_88 * v_wg_id_128)) % v_TP_88)) / (4092 / v_TP_89)) + (v_wg_id_128 / (4092 / v_TP_89)) + (v_TP_88 * v_wg_id_127)) / 4095)) + (4096 * (((4092 / v_TP_89) * ((v_l_id_133 + (v_TP_88 * v_wg_id_128)) % v_TP_88)) / (4092 / v_TP_89))) + (4096 * (v_wg_id_128 / (4092 / v_TP_89))) + (v_TP_89 * ((v_wg_id_128 + ((4092 / v_TP_89) * ((v_l_id_133 + (v_TP_88 * v_wg_id_128)) % v_TP_88))) % (4092 / v_TP_89))))] = id(v__140_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


