
// High-level hash: b25aafd5344e2613607aeacf0ac8b39af4010fc888fe7aaa7d2630b91f7a6de9
// Low-level hash: 085e372cedf4d8af299866b338acf75ff98b7fbec6da7b4af4f58984685c5b6f

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_253" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_252" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__355, global float* v__426){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__423[(16+(4*v_TP_252)+(4*v_TP_253)+(v_TP_252*v_TP_253))];
  /* Typed Value memory */
  /* Private Memory */
  float v__425_0;
  
  for (int v_wg_id_347 = get_group_id(1); v_wg_id_347 < (4092 / v_TP_252); v_wg_id_347 = (v_wg_id_347 + NUM_GROUPS_1)) {
    for (int v_wg_id_350 = get_group_id(0); v_wg_id_350 < (4092 / v_TP_253); v_wg_id_350 = (v_wg_id_350 + NUM_GROUPS_0)) {
      for (int v_l_id_351 = get_local_id(1); v_l_id_351 < (4 + v_TP_252); v_l_id_351 = (v_l_id_351 + LOCAL_SIZE_1)) {
        for (int v_l_id_352 = get_local_id(0); v_l_id_352 < (4 + v_TP_253); v_l_id_352 = (v_l_id_352 + LOCAL_SIZE_0)) {
          v__423[(v_l_id_352 + (4 * v_l_id_351) + (v_TP_253 * v_l_id_351))] = idfloat(v__355[(v_l_id_352 + (4096 * v_TP_252 * v_wg_id_347) + (4096 * (v_wg_id_350 / (4092 / v_TP_253))) + (4096 * (((4092 / v_TP_253) * (v_l_id_351 % (4 + v_TP_252))) / (4092 / v_TP_253))) + (v_TP_253 * ((v_wg_id_350 + ((4092 / v_TP_253) * (v_l_id_351 % (4 + v_TP_252)))) % (4092 / v_TP_253))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_353 = get_local_id(1); v_l_id_353 < v_TP_252; v_l_id_353 = (v_l_id_353 + LOCAL_SIZE_1)) {
        for (int v_l_id_354 = get_local_id(0); v_l_id_354 < v_TP_253; v_l_id_354 = (v_l_id_354 + LOCAL_SIZE_0)) {
          v__425_0 = jacobi(v__423[((v_l_id_354 % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(1 + (v_l_id_354 % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(2 + (v_l_id_354 % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(3 + (v_l_id_354 % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(4 + (v_l_id_354 % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(4 + v_TP_253 + ((v_TP_253 + v_l_id_354) % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(5 + v_TP_253 + ((v_TP_253 + v_l_id_354) % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(6 + v_TP_253 + ((v_TP_253 + v_l_id_354) % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(7 + v_TP_253 + ((v_TP_253 + v_l_id_354) % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(8 + v_TP_253 + ((v_TP_253 + v_l_id_354) % v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(8 + v_l_id_354 + (2 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(9 + v_l_id_354 + (2 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(10 + v_l_id_354 + (2 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(11 + v_l_id_354 + (2 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(12 + v_l_id_354 + (2 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(12 + v_l_id_354 + (3 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(13 + v_l_id_354 + (3 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(14 + v_l_id_354 + (3 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(15 + v_l_id_354 + (3 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(16 + v_l_id_354 + (3 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(16 + v_l_id_354 + (4 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(17 + v_l_id_354 + (4 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(18 + v_l_id_354 + (4 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(19 + v_l_id_354 + (4 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))], v__423[(20 + v_l_id_354 + (4 * v_TP_253) + (4 * v_l_id_353) + (v_TP_253 * v_l_id_353))]); 
          v__426[(8194 + v_l_id_354 + (-4096 * ((v_l_id_354 + (v_TP_253 * ((v_wg_id_350 + ((4092 / v_TP_253) * ((v_l_id_353 + (v_TP_252 * v_wg_id_350)) % v_TP_252))) % (4092 / v_TP_253)))) / 4095)) + (4096 * v_TP_252 * v_wg_id_347) + (-16777216 * (((((4092 / v_TP_253) * ((v_l_id_353 + (v_TP_252 * v_wg_id_350)) % v_TP_252)) / (4092 / v_TP_253)) + (v_wg_id_350 / (4092 / v_TP_253)) + (v_TP_252 * v_wg_id_347)) / 4095)) + (4096 * (((4092 / v_TP_253) * ((v_l_id_353 + (v_TP_252 * v_wg_id_350)) % v_TP_252)) / (4092 / v_TP_253))) + (4096 * (v_wg_id_350 / (4092 / v_TP_253))) + (v_TP_253 * ((v_wg_id_350 + ((4092 / v_TP_253) * ((v_l_id_353 + (v_TP_252 * v_wg_id_350)) % v_TP_252))) % (4092 / v_TP_253))))] = id(v__425_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


