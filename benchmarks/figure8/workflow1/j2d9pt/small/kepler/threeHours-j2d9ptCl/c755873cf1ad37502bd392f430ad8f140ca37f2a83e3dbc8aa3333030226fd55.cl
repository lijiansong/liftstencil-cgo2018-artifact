
// High-level hash: c125ef912061758c481ab314a9fcb215e5eb2a2d71057421e8b4079bb95f0097
// Low-level hash: c755873cf1ad37502bd392f430ad8f140ca37f2a83e3dbc8aa3333030226fd55

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_289" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_288" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__313, global float* v__322){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__314[(4+(2*v_TP_288)+(2*v_TP_289)+(v_TP_288*v_TP_289))];
  /* Typed Value memory */
  /* Private Memory */
  float v__321_0;
  
  for (int v_wg_id_307 = get_group_id(1); v_wg_id_307 < (4094 / v_TP_288); v_wg_id_307 = (v_wg_id_307 + NUM_GROUPS_1)) {
    for (int v_wg_id_308 = get_group_id(0); v_wg_id_308 < (4094 / v_TP_289); v_wg_id_308 = (v_wg_id_308 + NUM_GROUPS_0)) {
      for (int v_l_id_309 = get_local_id(1); v_l_id_309 < (2 + v_TP_288); v_l_id_309 = (v_l_id_309 + LOCAL_SIZE_1)) {
        for (int v_l_id_310 = get_local_id(0); v_l_id_310 < (2 + v_TP_289); v_l_id_310 = (v_l_id_310 + LOCAL_SIZE_0)) {
          v__314[(v_l_id_310 + (2 * v_l_id_309) + (v_TP_289 * v_l_id_309))] = idfloat(v__313[(v_l_id_310 + (4096 * v_TP_288 * v_wg_id_307) + (4096 * (v_wg_id_308 / (4094 / v_TP_289))) + (4096 * (((4094 / v_TP_289) * (v_l_id_309 % (2 + v_TP_288))) / (4094 / v_TP_289))) + (v_TP_289 * ((v_wg_id_308 + ((4094 / v_TP_289) * (v_l_id_309 % (2 + v_TP_288)))) % (4094 / v_TP_289))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_311 = get_local_id(1); v_l_id_311 < v_TP_288; v_l_id_311 = (v_l_id_311 + LOCAL_SIZE_1)) {
        for (int v_l_id_312 = get_local_id(0); v_l_id_312 < v_TP_289; v_l_id_312 = (v_l_id_312 + LOCAL_SIZE_0)) {
          v__321_0 = jacobi(v__314[((v_l_id_312 % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(1 + (v_l_id_312 % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(2 + (v_l_id_312 % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(2 + v_TP_289 + ((v_TP_289 + v_l_id_312) % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(3 + v_TP_289 + ((v_TP_289 + v_l_id_312) % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(4 + v_TP_289 + ((v_TP_289 + v_l_id_312) % v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(4 + v_l_id_312 + (2 * v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(5 + v_l_id_312 + (2 * v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))], v__314[(6 + v_l_id_312 + (2 * v_TP_289) + (2 * v_l_id_311) + (v_TP_289 * v_l_id_311))]); 
          v__322[(4097 + v_l_id_312 + (-4096 * ((v_l_id_312 + (v_TP_289 * ((v_wg_id_308 + ((4094 / v_TP_289) * ((v_l_id_311 + (v_TP_288 * v_wg_id_308)) % v_TP_288))) % (4094 / v_TP_289)))) / 4095)) + (4096 * v_TP_288 * v_wg_id_307) + (-16777216 * (((((4094 / v_TP_289) * ((v_l_id_311 + (v_TP_288 * v_wg_id_308)) % v_TP_288)) / (4094 / v_TP_289)) + (v_wg_id_308 / (4094 / v_TP_289)) + (v_TP_288 * v_wg_id_307)) / 4095)) + (4096 * (((4094 / v_TP_289) * ((v_l_id_311 + (v_TP_288 * v_wg_id_308)) % v_TP_288)) / (4094 / v_TP_289))) + (4096 * (v_wg_id_308 / (4094 / v_TP_289))) + (v_TP_289 * ((v_wg_id_308 + ((4094 / v_TP_289) * ((v_l_id_311 + (v_TP_288 * v_wg_id_308)) % v_TP_288))) % (4094 / v_TP_289))))] = id(v__321_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


