
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: ca900c4dcbbc1810a084f86b127798f7de6c3f116373b58f917de85ed0e4a3d9

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_187" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_186" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__333, global float* v__340){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__339_0;
  
  for (int v_wg_id_327 = get_group_id(0); v_wg_id_327 < (4094 / v_TP_186); v_wg_id_327 = (v_wg_id_327 + NUM_GROUPS_0)) {
    for (int v_wg_id_329 = get_group_id(1); v_wg_id_329 < (4094 / v_TP_187); v_wg_id_329 = (v_wg_id_329 + NUM_GROUPS_1)) {
      for (int v_l_id_330 = get_local_id(0); v_l_id_330 < v_TP_186; v_l_id_330 = (v_l_id_330 + LOCAL_SIZE_0)) {
        for (int v_l_id_332 = get_local_id(1); v_l_id_332 < v_TP_187; v_l_id_332 = (v_l_id_332 + LOCAL_SIZE_1)) {
          v__339_0 = jacobi(v__333[((v_l_id_332 % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (4096 * (((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186))) / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(1 + (v_l_id_332 % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (4096 * (((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186))) / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(2 + (v_l_id_332 % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (4096 * (((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186))) / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * (v_l_id_330 % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(((v_TP_187 + v_l_id_332) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(1 + ((v_TP_187 + v_l_id_332) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(2 + ((v_TP_187 + v_l_id_332) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((1 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(((v_l_id_332 + (2 * v_TP_187)) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(1 + ((v_l_id_332 + (2 * v_TP_187)) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))], v__333[(2 + ((v_l_id_332 + (2 * v_TP_187)) % v_TP_187) + (4096 * v_TP_186 * v_wg_id_327) + (4096 * (((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186))) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((2 + v_l_id_330) % (2 + v_TP_186)))) % (4094 / v_TP_187))))]); 
          v__340[(4097 + v_l_id_332 + (-4096 * ((v_l_id_332 + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((v_l_id_330 + (v_TP_186 * v_wg_id_329)) % v_TP_186))) % (4094 / v_TP_187)))) / 4095)) + (4096 * v_TP_186 * v_wg_id_327) + (-16777216 * (((((4094 / v_TP_187) * ((v_l_id_330 + (v_TP_186 * v_wg_id_329)) % v_TP_186)) / (4094 / v_TP_187)) + (v_wg_id_329 / (4094 / v_TP_187)) + (v_TP_186 * v_wg_id_327)) / 4095)) + (4096 * (((4094 / v_TP_187) * ((v_l_id_330 + (v_TP_186 * v_wg_id_329)) % v_TP_186)) / (4094 / v_TP_187))) + (4096 * (v_wg_id_329 / (4094 / v_TP_187))) + (v_TP_187 * ((v_wg_id_329 + ((4094 / v_TP_187) * ((v_l_id_330 + (v_TP_186 * v_wg_id_329)) % v_TP_186))) % (4094 / v_TP_187))))] = id(v__339_0); 
        }
      }
    }
  }
}}


