// High-level hash: 7b1586687b50546015b3162e1e3abfb4f73f61833fc59ffcd7b8b33cfda3da2d
// Low-level hash: 1b78677663bd2dedfca1d511f2bb6b08eb5a0c873e51762fac94c9f7387371c7
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__20, global float* v__27){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__26; 
  for (int v_gl_id_18 = get_global_id(1); v_gl_id_18 < 4094; v_gl_id_18 = (v_gl_id_18 + 4096)) {
    for (int v_gl_id_19 = get_global_id(0); v_gl_id_19 < 4094; v_gl_id_19 = (v_gl_id_19 + 1024)) {
      v__26 = jacobi(v__20[(v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(1 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(2 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(4096 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(4097 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(4098 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(8192 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(8193 + v_gl_id_19 + (4096 * v_gl_id_18))], v__20[(8194 + v_gl_id_19 + (4096 * v_gl_id_18))]); 
      v__27[(4097 + v_gl_id_19 + (4096 * v_gl_id_18))] = id(v__26); 
    }
  }
}}
