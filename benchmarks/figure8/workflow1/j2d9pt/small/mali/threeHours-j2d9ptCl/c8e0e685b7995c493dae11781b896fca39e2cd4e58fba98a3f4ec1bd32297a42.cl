
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: c8e0e685b7995c493dae11781b896fca39e2cd4e58fba98a3f4ec1bd32297a42

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_73" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_72" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__94, global float* v__97){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__96_0;
  
  for (int v_wg_id_90 = get_group_id(1); v_wg_id_90 < (4094 / v_TP_72); v_wg_id_90 = (v_wg_id_90 + NUM_GROUPS_1)) {
    for (int v_wg_id_91 = get_group_id(0); v_wg_id_91 < (4094 / v_TP_73); v_wg_id_91 = (v_wg_id_91 + NUM_GROUPS_0)) {
      for (int v_l_id_92 = get_local_id(1); v_l_id_92 < v_TP_72; v_l_id_92 = (v_l_id_92 + LOCAL_SIZE_1)) {
        for (int v_l_id_93 = get_local_id(0); v_l_id_93 < v_TP_73; v_l_id_93 = (v_l_id_93 + LOCAL_SIZE_0)) {
          v__96_0 = jacobi(v__94[((v_l_id_93 % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (4096 * (((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72))) / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(1 + (v_l_id_93 % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (4096 * (((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72))) / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(2 + (v_l_id_93 % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (4096 * (((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72))) / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * (v_l_id_92 % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(((v_TP_73 + v_l_id_93) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(1 + ((v_TP_73 + v_l_id_93) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(2 + ((v_TP_73 + v_l_id_93) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((1 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(((v_l_id_93 + (2 * v_TP_73)) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(1 + ((v_l_id_93 + (2 * v_TP_73)) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))], v__94[(2 + ((v_l_id_93 + (2 * v_TP_73)) % v_TP_73) + (4096 * v_TP_72 * v_wg_id_90) + (4096 * (((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72))) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((2 + v_l_id_92) % (2 + v_TP_72)))) % (4094 / v_TP_73))))]); 
          v__97[(4097 + v_l_id_93 + (-4096 * ((v_l_id_93 + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((v_l_id_92 + (v_TP_72 * v_wg_id_91)) % v_TP_72))) % (4094 / v_TP_73)))) / 4095)) + (4096 * v_TP_72 * v_wg_id_90) + (-16777216 * (((((4094 / v_TP_73) * ((v_l_id_92 + (v_TP_72 * v_wg_id_91)) % v_TP_72)) / (4094 / v_TP_73)) + (v_wg_id_91 / (4094 / v_TP_73)) + (v_TP_72 * v_wg_id_90)) / 4095)) + (4096 * (((4094 / v_TP_73) * ((v_l_id_92 + (v_TP_72 * v_wg_id_91)) % v_TP_72)) / (4094 / v_TP_73))) + (4096 * (v_wg_id_91 / (4094 / v_TP_73))) + (v_TP_73 * ((v_wg_id_91 + ((4094 / v_TP_73) * ((v_l_id_92 + (v_TP_72 * v_wg_id_91)) % v_TP_72))) % (4094 / v_TP_73))))] = id(v__96_0); 
        }
      }
    }
  }
}}


