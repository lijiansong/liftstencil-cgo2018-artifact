
// High-level hash: c8811d8271478b4ed36deb17467fc194c184f2f00cdfce1fcf12efcacb29b052
// Low-level hash: fc7159b0c6fc7d1c250faddc848796bc1d87e5b588305ce9010f05ffaad239f1

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__64, global float* v__67){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__66_0;
  
  for (int v_gl_id_60 = get_global_id(1); v_gl_id_60 < (8190 / v_TP_41); v_gl_id_60 = (v_gl_id_60 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_61 = get_global_id(0); v_gl_id_61 < (8190 / v_TP_42); v_gl_id_61 = (v_gl_id_61 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_62 = 0; v_i_62 < v_TP_41; v_i_62 = (1 + v_i_62)) {
        /* map_seq */
        for (int v_i_63 = 0; v_i_63 < v_TP_42; v_i_63 = (1 + v_i_63)) {
          v__66_0 = jacobi(v__64[((v_i_63 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(1 + (v_i_63 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(2 + (v_i_63 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * (v_i_62 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(((v_TP_42 + v_i_63) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(1 + ((v_TP_42 + v_i_63) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(2 + ((v_TP_42 + v_i_63) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((1 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(((v_i_63 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(1 + ((v_i_63 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__64[(2 + ((v_i_63 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_60) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((2 + v_i_62) % (2 + v_TP_41)))) % (8190 / v_TP_42))))]); 
          v__67[(8193 + v_i_63 + (8192 * v_TP_41 * v_gl_id_60) + (-67108864 * (((v_gl_id_61 / (8190 / v_TP_42)) + (((8190 / v_TP_42) * ((v_i_62 + (v_TP_41 * v_gl_id_61)) % v_TP_41)) / (8190 / v_TP_42)) + (v_TP_41 * v_gl_id_60)) / 8191)) + (-8192 * ((v_i_63 + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((v_i_62 + (v_TP_41 * v_gl_id_61)) % v_TP_41))) % (8190 / v_TP_42)))) / 8191)) + (8192 * (v_gl_id_61 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((v_i_62 + (v_TP_41 * v_gl_id_61)) % v_TP_41)) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_61 + ((8190 / v_TP_42) * ((v_i_62 + (v_TP_41 * v_gl_id_61)) % v_TP_41))) % (8190 / v_TP_42))))] = id(v__66_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


