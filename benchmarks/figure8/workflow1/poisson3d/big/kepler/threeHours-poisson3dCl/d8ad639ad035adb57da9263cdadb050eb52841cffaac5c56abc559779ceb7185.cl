
// High-level hash: a833cd12dcbe405c10d9598d11208f2ef2d845a4b19e7aadf3c47a394b97fc2d
// Low-level hash: d8ad639ad035adb57da9263cdadb050eb52841cffaac5c56abc559779ceb7185

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"


#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(134217728)"
#atf::ocl::input "atf::buffer<float>(134217728)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
  return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 510; v_gl_id_24 = (v_gl_id_24 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 510; v_gl_id_25 = (v_gl_id_25 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 510; v_gl_id_26 = (v_gl_id_26 + GLOBAL_SIZE_0)) {
        v__29 = jacobi(v__27[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262145 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(263169 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262658 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262656 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(513 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(524801 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(1 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(524289 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(1025 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(525313 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(512 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(524800 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(262144 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(263168 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(514 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(524802 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262146 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(263170 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))]); 
        v__30[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}


