
// High-level hash: fa2766cf081e60976c85fc2be011368f23dc2d03ee4a10ee662dfe4702eb3524
// Low-level hash: 0b28c76861eca1cf3d09d37286e06393b7d39519822f413644d04fbeaca3b5da

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
  return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__44, global float* v__66){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__63_0;
  
  for (int v_gl_id_41 = get_global_id(0); v_gl_id_41 < 254; v_gl_id_41 = (v_gl_id_41 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_42 = get_global_id(1); v_gl_id_42 < 254; v_gl_id_42 = (v_gl_id_42 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_43 = 0; v_i_43 < 254; v_i_43 = (1 + v_i_43)) {
        v__63_0 = jacobi(v__44[(65793 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(65537 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(66049 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(65794 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(65792 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(257 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(131329 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(1 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(131073 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(513 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(131585 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(256 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(131328 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(65536 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(66048 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(258 + v_i_43 + (256 * v_gl_id_42) + (65536 * v_gl_id_41))], v__44[(131330 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(65538 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))], v__44[(66050 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))]); 
        v__66[(65793 + v_i_43 + (65536 * v_gl_id_41) + (256 * v_gl_id_42))] = id(v__63_0); 
      }
      /* end map_seq */
    }
  }
}}


