
// High-level hash: 660cae58392acd1b84ff4618386905f6b02940bfec7a333667bc0e3cffc65801
// Low-level hash: a79f2f235e48f8172bbae1dce2b974c78ed0f95160e16895b29251c19f683984

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__72, global float* v__87){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__86; 
  for (int v_gl_id_60 = get_global_id(0); v_gl_id_60 < 254; v_gl_id_60 = (v_gl_id_60 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_61 = get_global_id(1); v_gl_id_61 < 254; v_gl_id_61 = (v_gl_id_61 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_62 = get_global_id(2); v_gl_id_62 < 254; v_gl_id_62 = (v_gl_id_62 + GLOBAL_SIZE_2)) {
        v__86 = heat(v__72[(65793 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))], v__72[(65537 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))], v__72[(66049 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))], v__72[(65794 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))], v__72[(65792 + v_gl_id_62 + (256 * v_gl_id_61) + (65536 * v_gl_id_60))], v__72[(257 + v_gl_id_62 + (256 * v_gl_id_61) + (65536 * v_gl_id_60))], v__72[(131329 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))]); 
        v__87[(65793 + v_gl_id_62 + (65536 * v_gl_id_60) + (256 * v_gl_id_61))] = id(v__86); 
      }
    }
  }
}}


