#define area (Nx*Ny)
typedef float value;
__kernel void inout(__global value *u,
                    __global value *out,
                    const value ins,
                    const int n)
{	
	// sum in source
	u[(Sz*area)+(Sy*Nx+Sx)] += ins;

	// non-interp read out
	out[n]  = u[(Rz*area)+(Ry*Nx+Rx)];
	
}
