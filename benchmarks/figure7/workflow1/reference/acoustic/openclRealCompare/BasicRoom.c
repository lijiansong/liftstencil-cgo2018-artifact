// Set precision

#include<CL/cl.h>
#include "Audio.h"
#include "timing_macros.h"
#include "timing.h"
#include "grid_structs.h"
#include <sys/time.h>
#ifndef cl_utils_h
#include "cl_utils.h"
#endif

#include "blocks.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "printOut.h"

//#define STENCIL_PROGRAM_NAME "localKernel.cl"
#define STENCIL_PROGRAM_NAME "stencil_kernel.cl"
#define STENCIL_KERNEL_FUNC "UpdateScheme"

#define INOUT_PROGRAM_NAME "inout_kernel.cl"
#define INOUT_KERNEL_FUNC "inout"

// ----------------------------------------------

int main(int argc, char** argv){
    
  if (argc != 5)
  {
    fprintf(stderr, "Requires 4 arguments { Platform, Device, Iterations, UseFiles } \n");
    exit(1);
  }

  int platform_idx = atoi(argv[1]);
  int device_idx = atoi(argv[2]);
  int iterations = atoi(argv[3]);
  int useFileFlag = atoi(argv[4]);


    // ------------------------------------------
    // Simulation parameters					      
    value SR         = (value) numberSamples;             // Sample Rate
    value alpha      = 0.005;               // Boundary loss
    value c          = 344.0;               // Speed of sound in air (m/s)
    value k          = 1/SR;                                    
    value h          = sqrt(3.0)*c*k;
    value lambda     = c*k/h;                        
   
    // Set constant memory coeffs
    coeffs_type cf_h[1];
    cf_h[0].l2      = (c*c*k*k)/(h*h);
    cf_h[0].loss1   = 1.0/(1.0+lambda*alpha);
    cf_h[0].loss2   = 1.0-lambda*alpha;
   
    //-------------------------------------------
    // Initialise input
    int n;
    int duration = 20;
    int alength = duration;
    value *si_h = (value *)calloc(numberSamples,sizeof(value));
    for(n=0;n<duration;n++)
    {
      si_h[n] = 0.5*(1.0-cos(2.0*pi*n/(value)duration));
    }
    
    //timing values:
    
    startTime = getTime(); 
	// ------------------------------------------
	// Set up grid and blocks

	int Gx          = Nx/Bx;
	int Gy          = Ny/By;
	int Gz          = Nz/Bz;
	
	size_t pr_size  = sizeof(value);
	size_t mem_size = AREA*Nz*pr_size;
        
        cl_mem out_d, u_d, u1_d;    
        cl_mem cf_d, dummy_ptr;
        value ins;             
	
        // ---
        // Setup OpenCL stuff

        cl_platform_id platform;        // OpenCL platform
        cl_device_id device_id;           // device ID
        cl_context context;               // context
        cl_command_queue queue;           // command queue
        cl_program program;               // program
        cl_kernel stencil_kernel;                 // kernel
        cl_kernel inout_kernel;                 // kernel

        cl_int err;
	value *out_h  = (value *)malloc(numberSamples*sizeof(value));
	value *u_h  = (value *)malloc(VOLUME*sizeof(value));
	value *u1_h  = (value *)malloc(VOLUME*sizeof(value));
	if((out_h == NULL) || (u_h == NULL)  || (u1_h == NULL) )
        {
		printf("\host memory alloc failed...\n");
		exit(EXIT_FAILURE);
	}

        char outputtxt[59];
        // initialize host memory (will be written on GPU memory as well)
        int xi;
	int count = 0;
        for(xi=0;xi<VOLUME;xi++)
        {
            u_h[xi] = 0.0; 
            u1_h[xi] = 0.0; 
	    count++;
            if(xi<numberSamples)
            {
              out_h[xi] = 0.0;
            }
        }
        // read instead from file -- only one sample run!
        char fileToRead[256];

        if(useFileFlag)
        {
//        sprintf(fileToRead,"%s/originalRoom%d-%d-%d-N4410-iter4409.fl",dir,Nx,Ny,Nz);
            sprintf(fileToRead,"/home/s1147290/workspace/lift_data_stencils/datasets/acoustic/roomtminus1.txt");
            readFloatsFromFile(u_h, fileToRead, VOLUME);
            sprintf(fileToRead,"");

//            sprintf(fileToRead,"%s/original1Room%d-%d-%d-N4410-iter4409.fl",dir,Nx,Ny,Nz);
            sprintf(fileToRead,"/home/s1147290/workspace/lift_data_stencils/datasets/acoustic/roomt.txt");
            readFloatsFromFile(u1_h, fileToRead, VOLUME);
            sprintf(fileToRead,"");

//            sprintf(fileToRead,"%s/outRoom%d-%d-%d-N4410-iter4409.fl",dir,Nx,Ny,Nz);
            sprintf(fileToRead,"/home/s1147290/workspace/lift_data_stencils/datasets/acoustic/output.txt");
            readFloatsFromFile(out_h, fileToRead, numberSamples);
        }
        /*
        printMatWithSizes(u_h,Nx,Ny,Nz);
        printMatWithSizes(u1_h,Nx,Ny,Nz);
*/

        printToTimingFileName("original");
       

        // Number of work items in each local work group
        size_t localSize[3] = { Bx,By,Bz};
        size_t localSizeIO[3] = { 1, 1, 1 };
    
        // Number of total work items - localSize must be divisor
        size_t globalSize[3] = { Nx, Ny, Nz };
        size_t globalSizeIO[3] = { 1, 1, 1 };
     
        programBuildStart = getTime();

        // Get Platform
        platform = get_platform(platform_idx);

        // Create a context  
       /* context = createContext(platform); 
        err_check(err);*/

        device_id = create_device(device_idx,context,platform);
        
        // Create a context  
        context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
        err_check(err);
     
        // Create a command queue 
        queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
        printConstants();
        //printDeviceInfo(device_id);
	puts(infoString);

       // Create the compute program from the source buffer
        
        FILE* stencil_handle = fopen(STENCIL_PROGRAM_NAME,"r");
        char* program_buffer[2];
    
        fseek(stencil_handle,0,SEEK_END);
        size_t program_size[2];
        program_size[0] = ftell(stencil_handle);
        rewind(stencil_handle);
    
        program_buffer[0] = (char*)malloc(program_size[0]+1);
        fread(program_buffer[0], sizeof(char), program_size[0], stencil_handle);
        fclose(stencil_handle);
    
       
        FILE* inout_handle = fopen(INOUT_PROGRAM_NAME,"r");
    
        fseek(inout_handle,0,SEEK_END);
        program_size[1] = ftell(inout_handle);
        rewind(inout_handle);
    
        program_buffer[1] = (char*)malloc(program_size[1]+1);
        fread(program_buffer[1], sizeof(char), program_size[1], inout_handle);
        fclose(inout_handle);
   

        program = clCreateProgramWithSource(context, 2,
                             &program_buffer, (size_t*)&program_size, &err);
        err_check(err);
    
        free(program_buffer[0]);
        free(program_buffer[1]);

      char options[150];
      sprintf(options,"-DNx=%d -DNy=%d -DNz=%d -DRx=%d -DRy=%d -DRz=%d -DSx=%d -DSy=%d -DSz=%d -DBLOCK_X=%d -DBLOCK_Y=%d -DBLOCK_Z=%d",Nx,Ny,Nz,Rx,Ry,Rz,Sx,Sy,Sz,BLOCK_X,BLOCK_Y,BLOCK_Z);

        err = clBuildProgram(program, 0, NULL, options, NULL, NULL);
        
        if(err != CL_SUCCESS)
        {
          size_t length;
          char buffer[2048];
          clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &length);
          printf("--- Build log --- %s\n ",buffer);
          exit(1);
        }
        
        stencil_kernel = clCreateKernel(program, STENCIL_KERNEL_FUNC, &err);
        err_check(err);
        inout_kernel = clCreateKernel(program, INOUT_KERNEL_FUNC, &err);
        err_check(err);
        programBuildEnd = getTime();
        programBuildTotal = programBuildEnd-programBuildStart;
        
        //-------------------------------------------
	// Initialise memory on device

        dataCopyInitStart = getTime();
        
        u_d = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size, NULL, NULL);
        u1_d = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size, NULL, NULL);
        out_d = clCreateBuffer(context, CL_MEM_READ_WRITE, numberSamples*sizeof(value), NULL, NULL);
        cf_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(coeffs_type), NULL, NULL);

        cl_mem *u_ptr = &u_d;
        cl_mem *u1_ptr = &u1_d;
        cl_mem *dummy;


        err |= clEnqueueWriteBuffer(queue, u_d, CL_TRUE, 0,
                                  mem_size, u_h, 0, NULL, NULL);
        err |= clEnqueueWriteBuffer(queue, u1_d, CL_TRUE, 0,
                                  mem_size, u1_h, 0, NULL, NULL);
        err |= clEnqueueWriteBuffer(queue, out_d, CL_TRUE, 0,
                                  numberSamples*sizeof(value), out_h, 0, NULL, NULL);
        err |= clEnqueueWriteBuffer(queue, cf_d, CL_TRUE, 0,
                                   sizeof(coeffs_type), cf_h, 0, NULL, NULL);

        err  = clSetKernelArg(stencil_kernel, 0, sizeof(cl_mem), &u_d);
        err_check(err);
        err |= clSetKernelArg(stencil_kernel, 1, sizeof(cl_mem), &u1_d);
        err_check(err);
        err |= clSetKernelArg(stencil_kernel, 2, sizeof(cl_mem), &cf_d);
        err_check(err);
 
        err |= clSetKernelArg(inout_kernel, 0, sizeof(cl_mem), &u_d);
        err_check(err);
        err |= clSetKernelArg(inout_kernel, 1, sizeof(cl_mem), &out_d);
        err_check(err);
        err |= clSetKernelArg(inout_kernel, 2, sizeof(value), &ins);
        err_check(err);
        err |= clSetKernelArg(inout_kernel, 3, sizeof(int), &n);
        err_check(err);
        dataCopyInitEnd = getTime();
        dataCopyInitTotal = dataCopyInitEnd - dataCopyInitStart;

	//-------------------------------------------
	// Compute scheme
	//-------------------------------------------
	
	
        int jj;
        startKernels = getTime();
        kernel1Time = 0.0;
        kernel2Time = 0.0;
	dataCopyBtwTotal = 0.0;
	
        cl_event kernel1Events[numberSamples];
        cl_event kernel2Events[numberSamples];

        for(jj=0;jj<iter;jj++)
        {
        for(n=0;n<iterations;n++)
	{
            err = clEnqueueNDRangeKernel(queue, stencil_kernel, 3, NULL, &globalSize, &localSize,
                                                              0, NULL, &kernel1Events[n]);
            err_check(err);
            ins = 0.0;
	    if(n<alength)
            {
              ins = si_h[n];
            }
           
           int k = 4409;
            err |= clSetKernelArg(inout_kernel, 2, sizeof(value), &ins);
            err |= clSetKernelArg(inout_kernel, 3, sizeof(int), &k);
            
            err = clEnqueueNDRangeKernel(queue, inout_kernel, 3, NULL, &globalSizeIO, &localSizeIO,
                                                              0, NULL, &kernel2Events[n]);
                                                                                
            dummy = u1_ptr;
            u1_ptr = u_ptr;
            u_ptr = dummy;


            err  = clSetKernelArg(stencil_kernel, 0, sizeof(cl_mem), u_ptr);
             err |= clSetKernelArg(stencil_kernel, 1, sizeof(cl_mem), u1_ptr);
            err |= clSetKernelArg(inout_kernel, 0, sizeof(cl_mem), u_ptr);
	
	}
        }
    
    // Finish up OpenCL    
    err = clFinish(queue);

    endKernels = getTime();
    kernelsTime = endKernels-startKernels;
    

    dataCopyBackStart = getTime();     
    // Read the results from the device
    clEnqueueReadBuffer(queue, out_d, CL_TRUE, 0,
                               numberSamples*sizeof(value), out_h, 0, NULL, NULL );
    clEnqueueReadBuffer(queue, u1_d, CL_TRUE, 0,
                              VOLUME*sizeof(value), u_h, 0, NULL, NULL );
    clEnqueueReadBuffer(queue, u_d, CL_TRUE, 0,
                              VOLUME*sizeof(value), u1_h, 0, NULL, NULL );
    dataCopyBackEnd = getTime();     
    dataCopyBackTotal = dataCopyBackEnd - dataCopyBackStart;


    endTime = getTime();
    

    totalTime = (value) (endTime-startTime);
    
    for(jj=numberSamples-10;jj<numberSamples;jj++)
    {
        printf("%.14lf\n",out_h[jj]);
    }
//    writeBinaryDataToFile(u_h,getOutputFileName(OPENCL_STR,"room","bin"),VOLUME);       
//    writeBinaryDataToFile(out_h,getOutputFileName(OPENCL_STR,"receiver","bin"),numberSamples);       
    
    kernel1Time = getTimeForAllEvents(iterations,kernel1Events);
    kernel2Time = getTimeForAllEvents(iterations,kernel2Events);
    dataCopyBtwTotal = 0.0; 
    dataCopyTotal = dataCopyInitTotal + dataCopyBtwTotal + dataCopyBackTotal;
    printf("KERNEL TIME: %f\n",kernel1Time/iterations);




//    char outputtxt[59];
/*
    sprintf(outputtxt,"originalRoom%d-%d-%d-N%d-iter%d.fl",Nx,Ny,Nz,numberSamples,iterations);
    writeFloatsToFile(u_h,outputtxt,VOLUME);       
    sprintf(outputtxt,"");

    sprintf(outputtxt,"originalRoom%d-%d-%d-N%d-iter%d-nozero.fl",Nx,Ny,Nz,numberSamples,iterations);
    writeFloatsToFileNoZero(u_h,outputtxt,VOLUME);       
    sprintf(outputtxt,"");
    
    sprintf(outputtxt,"original1Room%d-%d-%d-N%d-iter%d.fl",Nx,Ny,Nz,numberSamples,iterations);
    writeFloatsToFile(u1_h,outputtxt,VOLUME);       
    sprintf(outputtxt,"");

    sprintf(outputtxt,"original1Room%d-%d-%d-N%d-iter%d-nozero.fl",Nx,Ny,Nz,numberSamples,iterations);
    writeFloatsToFileNoZero(u1_h,outputtxt,VOLUME);       
    sprintf(outputtxt,"");*/

    sprintf(outputtxt,"outRoom%d-%d-%d-N%d-iter%d.fl",Nx,Ny,Nz,numberSamples,iterations);
    writeFloatsToFile(out_h,outputtxt,numberSamples);       

    printToString;
    printOutputs;
    writeTimingsToFile; 


    clReleaseMemObject(out_d);
    clReleaseMemObject(u_d);
    clReleaseMemObject(u1_d);

    clReleaseProgram(program);
    clReleaseKernel(stencil_kernel);
    clReleaseKernel(inout_kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    
    //-------------------------------------------
    // Free memory
    free(si_h);free(out_h);free(u_h);free(u1_h);
	
    exit(EXIT_SUCCESS);
}

