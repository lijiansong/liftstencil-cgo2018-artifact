
// High-level hash: a81e23369b1a39f5dce1bdfbb8295ca0a8a99358ebf481beae60a7b999b7f3f3
// Low-level hash: 4de9a2ab3c1e0e21826c0fae78415ea9b119dc9e53f2a7d4773704b3fa78b37f

#atf::var<int> v_M_0 = 512
#atf::var<int> v_N_1 = 512
#atf::var<int> v_O_2 = 8

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"

#atf::abort_condition "atf::cond::evaluations(1)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::input "atf::scalar<int>(v_O_2)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
  { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }
}
float idfloat(float x){
  { return x; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__53, const global float* restrict v__54, global float* v__78, int v_M_0, int v_N_1, int v_O_2){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__58; 
  float v__60; 
  float v__62; 
  float v__64; 
  float v__66; 
  float v__68; 
  float v__70; 
  float v__72; 
  float v__74; 
  /* Private Memory */
  float v__59; 
  float v__61; 
  float v__63; 
  float v__65; 
  float v__67; 
  float v__69; 
  float v__71; 
  float v__73; 
  float v__75; 
  float v__77; 
  for (int v_gl_id_50 = get_global_id(2); v_gl_id_50 < v_O_2; v_gl_id_50 = (v_gl_id_50 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_51 = get_global_id(1); v_gl_id_51 < v_N_1; v_gl_id_51 = (v_gl_id_51 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_52 = get_global_id(0); v_gl_id_52 < v_M_0; v_gl_id_52 = (v_gl_id_52 + GLOBAL_SIZE_0)) {
        float v_tmp_104 = 0.86186665f; 
        v__58 = v_tmp_104; 
        v__59 = idfloat(v__58); 
        float v_tmp_106 = 0.03413333f; 
        v__60 = v_tmp_106; 
        v__61 = idfloat(v__60); 
        float v_tmp_107 = 0.03413333f; 
        v__62 = v_tmp_107; 
        v__63 = idfloat(v__62); 
        float v_tmp_108 = 0.03413333f; 
        v__64 = v_tmp_108; 
        v__65 = idfloat(v__64); 
        float v_tmp_109 = 0.03413333f; 
        v__66 = v_tmp_109; 
        v__67 = idfloat(v__66); 
        float v_tmp_110 = 5.333333E-4f; 
        v__68 = v_tmp_110; 
        v__69 = idfloat(v__68); 
        float v_tmp_111 = 5.333333E-4f; 
        v__70 = v_tmp_111; 
        v__71 = idfloat(v__70); 
        float v_tmp_112 = 0.3413333f; 
        v__72 = v_tmp_112; 
        v__73 = idfloat(v__72); 
        float v_tmp_113 = 80.0f; 
        v__74 = v_tmp_113; 
        v__75 = idfloat(v__74); 
        v__77 = calculateHotspot(v__53[((v_M_0 * v_N_1 * ( (v_gl_id_50 >= 0) ? ( (v_gl_id_50 < v_O_2) ? v_gl_id_50 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_51 >= 0) ? ( (v_gl_id_51 < v_N_1) ? v_gl_id_51 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_M_0) ? v_gl_id_52 : (-1 + v_M_0) ) : 0 ))], v__59, v__53[((v_M_0 * v_N_1 * ( (v_gl_id_50 >= 0) ? ( (v_gl_id_50 < v_O_2) ? v_gl_id_50 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((-1 + v_gl_id_51) >= 0) ? ( ((-1 + v_gl_id_51) < v_N_1) ? (-1 + v_gl_id_51) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_M_0) ? v_gl_id_52 : (-1 + v_M_0) ) : 0 ))], v__61, v__53[((v_M_0 * v_N_1 * ( (v_gl_id_50 >= 0) ? ( (v_gl_id_50 < v_O_2) ? v_gl_id_50 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((1 + v_gl_id_51) >= 0) ? ( ((1 + v_gl_id_51) < v_N_1) ? (1 + v_gl_id_51) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_M_0) ? v_gl_id_52 : (-1 + v_M_0) ) : 0 ))], v__63, v__53[((v_M_0 * v_N_1 * ( ((1 + v_gl_id_50) >= 0) ? ( ((1 + v_gl_id_50) < v_O_2) ? (1 + v_gl_id_50) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_51 >= 0) ? ( (v_gl_id_51 < v_N_1) ? v_gl_id_51 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_M_0) ? v_gl_id_52 : (-1 + v_M_0) ) : 0 ))], v__65, v__53[((v_M_0 * v_N_1 * ( ((-1 + v_gl_id_50) >= 0) ? ( ((-1 + v_gl_id_50) < v_O_2) ? (-1 + v_gl_id_50) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_51 >= 0) ? ( (v_gl_id_51 < v_N_1) ? v_gl_id_51 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_M_0) ? v_gl_id_52 : (-1 + v_M_0) ) : 0 ))], v__67, v__53[((v_M_0 * v_N_1 * ( (v_gl_id_50 >= 0) ? ( (v_gl_id_50 < v_O_2) ? v_gl_id_50 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_51 >= 0) ? ( (v_gl_id_51 < v_N_1) ? v_gl_id_51 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_52) >= 0) ? ( ((1 + v_gl_id_52) < v_M_0) ? (1 + v_gl_id_52) : (-1 + v_M_0) ) : 0 ))], v__69, v__53[((v_M_0 * v_N_1 * ( (v_gl_id_50 >= 0) ? ( (v_gl_id_50 < v_O_2) ? v_gl_id_50 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_51 >= 0) ? ( (v_gl_id_51 < v_N_1) ? v_gl_id_51 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_52) >= 0) ? ( ((-1 + v_gl_id_52) < v_M_0) ? (-1 + v_gl_id_52) : (-1 + v_M_0) ) : 0 ))], v__71, v__73, v__54[(v_gl_id_52 + (v_M_0 * v_N_1 * v_gl_id_50) + (v_M_0 * v_gl_id_51))], v__75); 
        v__78[(v_gl_id_52 + (v_M_0 * v_N_1 * v_gl_id_50) + (v_M_0 * v_gl_id_51))] = id(v__77); 
      }
    }
  }
}}


