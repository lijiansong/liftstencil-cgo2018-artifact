
// High-level hash: 284ef0a6d98ad58650682e3ad7d3bac4d79f7e238545a35053188d6815088240
// Low-level hash: 1afe5c602b878b1af95c1060f1045ccd7eec76e36f11e0d6c6acd83bc056ea05

#atf::var<int> v_M_129 = 8194
#atf::var<int> v_N_130 = 8194

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(900)"
#atf::abort_condition "atf::cond::evaluations(1)"


#atf::tp name "v_TP_397" \
 type "int" \
 range "atf::interval<int>(4096,4096)" \
 constraint "atf::divides((-2+v_M_129))"
#atf::tp name "v_TP_396" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides((-2+v_N_130))"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(8192,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(256,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 32; }"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_129*v_N_130))"
#atf::ocl::input "atf::buffer<float>(9)"
#atf::ocl::input "atf::buffer<float>((4+(-2*v_M_129)+(-2*v_N_130)+(v_M_129*v_N_130)))"
#atf::ocl::input "atf::scalar<int>(v_M_129)"
#atf::ocl::input "atf::scalar<int>(v_N_130)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple2_float_float;
#endif

float idfloat(float x){
  { return x; }
}
Tuple2_float_float idTuple2_float_float(Tuple2_float_float x){
  typedef Tuple2_float_float Tuple;
  
  { return x; }
}
float add(float x, float y){
  { return x+y; }
}
float mult(float l, float r){
  { return l * r; }
}
kernel void KERNEL(const global float* restrict v__422, const global float* restrict v__423, global float* v__434, int v_M_129, int v_N_130){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__424; 
  /* Private Memory */
  float v__425_0;
  
  Tuple2_float_float v__428_0;
  
  float v__430_0;
  
  for (int v_wg_id_416 = get_group_id(1); v_wg_id_416 < ((-2 + v_N_130) / v_TP_396); v_wg_id_416 = (v_wg_id_416 + NUM_GROUPS_1)) {
    for (int v_wg_id_417 = get_group_id(0); v_wg_id_417 < ((-2 + v_M_129) / v_TP_397); v_wg_id_417 = (v_wg_id_417 + NUM_GROUPS_0)) {
      for (int v_l_id_418 = get_local_id(1); v_l_id_418 < v_TP_396; v_l_id_418 = (v_l_id_418 + LOCAL_SIZE_1)) {
        for (int v_l_id_419 = get_local_id(0); v_l_id_419 < v_TP_397; v_l_id_419 = (v_l_id_419 + LOCAL_SIZE_0)) {
          float v_tmp_510 = 0.0f; 
          v__424 = v_tmp_510; 
          v__425_0 = idfloat(v__424); 
          /* reduce_seq */
          /* unroll */
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[0], v__422[((v_l_id_419 % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[1], v__422[(1 + (v_l_id_419 % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[2], v__422[(2 + (v_l_id_419 % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * (v_l_id_418 % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[3], v__422[(((v_TP_397 + v_l_id_419) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[4], v__422[(1 + ((v_TP_397 + v_l_id_419) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[5], v__422[(2 + ((v_TP_397 + v_l_id_419) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((1 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[6], v__422[(((v_l_id_419 + (2 * v_TP_397)) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[7], v__422[(1 + ((v_l_id_419 + (2 * v_TP_397)) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          v__428_0 = idTuple2_float_float((Tuple2_float_float){v__423[8], v__422[(2 + ((v_l_id_419 + (2 * v_TP_397)) % v_TP_397) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396))) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((2 + v_l_id_418) % (2 + v_TP_396)))) % ((-2 + v_M_129) / v_TP_397))))]}); 
          v__430_0 = mult(v__428_0._0, v__428_0._1); 
          v__425_0 = add(v__425_0, v__430_0); 
          /* end unroll */
          /* end reduce_seq */
          /* map_seq */
          /* unroll */
          v__434[(v_l_id_419 + (-2 * v_TP_396 * v_wg_id_416) + (-2 * ((((-2 + v_M_129) / v_TP_397) * ((v_l_id_418 + (v_TP_396 * v_wg_id_417)) % v_TP_396)) / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_397) * ((v_l_id_418 + (v_TP_396 * v_wg_id_417)) % v_TP_396)) / ((-2 + v_M_129) / v_TP_397))) + (-2 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_M_129 * v_TP_396 * v_wg_id_416) + (v_M_129 * (v_wg_id_417 / ((-2 + v_M_129) / v_TP_397))) + (v_TP_397 * ((v_wg_id_417 + (((-2 + v_M_129) / v_TP_397) * ((v_l_id_418 + (v_TP_396 * v_wg_id_417)) % v_TP_396))) % ((-2 + v_M_129) / v_TP_397))))] = idfloat(v__425_0); 
          /* end unroll */
          /* end map_seq */
        }
      }
    }
  }
}}


