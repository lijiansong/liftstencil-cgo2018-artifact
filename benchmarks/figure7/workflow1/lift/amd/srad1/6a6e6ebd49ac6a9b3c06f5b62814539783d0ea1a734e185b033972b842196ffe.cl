
// High-level hash: 31ff27fb924a89bd4d0df28312610f98591a6d1a9f843028dd12d1ebc8db9146
// Low-level hash: 6a6e6ebd49ac6a9b3c06f5b62814539783d0ea1a734e185b033972b842196ffe

#atf::var<int> v_M_101 = 502
#atf::var<int> v_N_102 = 458

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(900)"
#atf::abort_condition "atf::cond::evaluations(1)"


#atf::tp name "v_TP_123" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(v_M_101)"
#atf::tp name "v_TP_122" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(v_N_102)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(16,16)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 32; }"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::scalar<int>(v_M_101)"
#atf::ocl::input "atf::scalar<int>(v_N_102)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float subtract(float l, float r){
  { return l - r; }
}
float calculateScoeff(float dN, float dS, float dE, float dW, float jC, float q0sqr){
  { float g2 = (dN*dN + dS*dS + dW*dW + dE*dE) / (jC * jC);float l = (dN + dS + dW + dE ) / jC; float num = (0.5*g2) - ((1.0/16.0)*(l*l)); float  den = 1 + (0.25*l);float qsqr = num/(den*den); den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));float coeff = 1.0 / (1.0+den);  if(coeff > 1) { return 1.0f; } else if(coeff < 0 ) { return 0.0f; } else { return coeff;}  }
}
float idfloat(float x){
  { return x; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__136, global float* v__149, int v_M_101, int v_N_102){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__145; 
  /* Private Memory */
  float v__138_0;
  
  float v__140_0;
  
  float v__142_0;
  
  float v__144_0;
  
  float v__146_0;
  
  float v__148_0;
  
  for (int v_gl_id_132 = get_global_id(1); v_gl_id_132 < (v_N_102 / v_TP_122); v_gl_id_132 = (v_gl_id_132 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_133 = get_global_id(0); v_gl_id_133 < (v_M_101 / v_TP_123); v_gl_id_133 = (v_gl_id_133 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_134 = 0; v_i_134 < v_TP_122; v_i_134 = (1 + v_i_134)) {
        /* map_seq */
        for (int v_i_135 = 0; v_i_135 < v_TP_123; v_i_135 = (1 + v_i_135)) {
          v__138_0 = subtract(v__136[((v_M_101 * ( ((-1 + (((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( (((v_i_135 % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( (((v_i_135 % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? ((v_i_135 % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * (v_i_134 % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))], v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))]); 
          v__140_0 = subtract(v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_i_135 + (2 * v_TP_123)) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_i_135 + (2 * v_TP_123)) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_i_135 + (2 * v_TP_123)) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((2 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))], v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))]); 
          v__142_0 = subtract(v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))], v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))]); 
          v__144_0 = subtract(v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((-1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((-1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (-1 + ((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))], v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))]); 
          float v_tmp_786 = 0.05378722f; 
          v__145 = v_tmp_786; 
          v__146_0 = idfloat(v__145); 
          v__148_0 = calculateScoeff(v__138_0, v__140_0, v__142_0, v__144_0, v__136[((v_M_101 * ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) >= 0) ? ( ((-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) < v_N_102) ? (-1 + (v_gl_id_133 / (v_M_101 / v_TP_123)) + (((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122))) / (v_M_101 / v_TP_123)) + (v_TP_122 * v_gl_id_132)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) >= 0) ? ( ((((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) < v_M_101) ? (((v_TP_123 + v_i_135) % v_TP_123) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((1 + v_i_134) % (2 + v_TP_122)))) % (v_M_101 / v_TP_123)))) : (-1 + v_M_101) ) : 0 ))], v__146_0); 
          v__149[(v_i_135 + (v_M_101 * v_TP_122 * v_gl_id_132) + (v_M_101 * (v_gl_id_133 / (v_M_101 / v_TP_123))) + (v_M_101 * (((v_M_101 / v_TP_123) * ((v_i_134 + (v_TP_122 * v_gl_id_133)) % v_TP_122)) / (v_M_101 / v_TP_123))) + (v_TP_123 * ((v_gl_id_133 + ((v_M_101 / v_TP_123) * ((v_i_134 + (v_TP_122 * v_gl_id_133)) % v_TP_122))) % (v_M_101 / v_TP_123))))] = id(v__148_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


