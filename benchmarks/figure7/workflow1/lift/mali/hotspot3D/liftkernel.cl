float mult(float l, float r){
  { return l * r; }
}
float add(float x, float y){
  { return x+y; }
}
float id(float x){
  { return x; }
}
kernel void hotspotOpt1(const global float* restrict v__55, const global float* restrict v__56, float v__57, float v__58, float v__59, float v__60, float v__61, float v__62, float v__63, global float* v__104, int v_M_0, int v_N_1, int v_O_2){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__94;
  float v__99;
  /* Private Memory */
  float v__69;
  float v__71;
  float v__73;
  float v__75;
  float v__77;
  float v__79;
  float v__81;
  float v__83;
  float v__85;
  float v__87;
  float v__89;
  float v__91;
  float v__93;
  float v__96;
  float v__98;
  float v__101;
  float v__103;
  for (int v_gl_id_52 = get_global_id(2);v_gl_id_52<v_O_2;v_gl_id_52 = (v_gl_id_52 + get_global_size(2))){
    for (int v_gl_id_53 = get_global_id(1);v_gl_id_53<v_N_1;v_gl_id_53 = (v_gl_id_53 + get_global_size(1))){
      for (int v_gl_id_54 = get_global_id(0);v_gl_id_54<v_M_0;v_gl_id_54 = (v_gl_id_54 + get_global_size(0))){
        v__69 = mult(v__55[((v_M_0 * v_N_1 * ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_O_2) ? v_gl_id_52 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_53 >= 0) ? ( (v_gl_id_53 < v_N_1) ? v_gl_id_53 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_54 >= 0) ? ( (v_gl_id_54 < v_M_0) ? v_gl_id_54 : (-1 + v_M_0) ) : 0 ))], v__63);
        v__71 = mult(v__55[((v_M_0 * v_N_1 * ( ((-1 + v_gl_id_52) >= 0) ? ( ((-1 + v_gl_id_52) < v_O_2) ? (-1 + v_gl_id_52) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_53 >= 0) ? ( (v_gl_id_53 < v_N_1) ? v_gl_id_53 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_54 >= 0) ? ( (v_gl_id_54 < v_M_0) ? v_gl_id_54 : (-1 + v_M_0) ) : 0 ))], v__58);
        v__73 = add(v__69, v__71);
        v__75 = mult(v__55[((v_M_0 * v_N_1 * ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_O_2) ? v_gl_id_52 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((-1 + v_gl_id_53) >= 0) ? ( ((-1 + v_gl_id_53) < v_N_1) ? (-1 + v_gl_id_53) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_54 >= 0) ? ( (v_gl_id_54 < v_M_0) ? v_gl_id_54 : (-1 + v_M_0) ) : 0 ))], v__59);
        v__77 = add(v__73, v__75);
        v__79 = mult(v__55[((v_M_0 * v_N_1 * ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_O_2) ? v_gl_id_52 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_53 >= 0) ? ( (v_gl_id_53 < v_N_1) ? v_gl_id_53 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_54) >= 0) ? ( ((-1 + v_gl_id_54) < v_M_0) ? (-1 + v_gl_id_54) : (-1 + v_M_0) ) : 0 ))], v__62);
        v__81 = add(v__77, v__79);
        v__83 = mult(v__55[((v_M_0 * v_N_1 * ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_O_2) ? v_gl_id_52 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_53 >= 0) ? ( (v_gl_id_53 < v_N_1) ? v_gl_id_53 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_54) >= 0) ? ( ((1 + v_gl_id_54) < v_M_0) ? (1 + v_gl_id_54) : (-1 + v_M_0) ) : 0 ))], v__61);
        v__85 = add(v__81, v__83);
        v__87 = mult(v__55[((v_M_0 * v_N_1 * ( (v_gl_id_52 >= 0) ? ( (v_gl_id_52 < v_O_2) ? v_gl_id_52 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((1 + v_gl_id_53) >= 0) ? ( ((1 + v_gl_id_53) < v_N_1) ? (1 + v_gl_id_53) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_54 >= 0) ? ( (v_gl_id_54 < v_M_0) ? v_gl_id_54 : (-1 + v_M_0) ) : 0 ))], v__60);
        v__89 = add(v__85, v__87);
        v__91 = mult(v__55[((v_M_0 * v_N_1 * ( ((1 + v_gl_id_52) >= 0) ? ( ((1 + v_gl_id_52) < v_O_2) ? (1 + v_gl_id_52) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_53 >= 0) ? ( (v_gl_id_53 < v_N_1) ? v_gl_id_53 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_54 >= 0) ? ( (v_gl_id_54 < v_M_0) ? v_gl_id_54 : (-1 + v_M_0) ) : 0 ))], v__57);
        v__93 = add(v__89, v__91);
        float v_tmp_325 = 80.0f;
        v__94 = v_tmp_325;
        v__96 = mult(v__94, v__61);
        v__98 = add(v__93, v__96);
        float v_tmp_326 = 8.3333325E-5f;
        v__99 = v_tmp_326;
        v__101 = mult(v__56[(v_gl_id_54 + (v_M_0 * v_N_1 * v_gl_id_52) + (v_M_0 * v_gl_id_53))], v__99);
        v__103 = add(v__98, v__101);
        v__104[(v_gl_id_54 + (v_M_0 * v_N_1 * v_gl_id_52) + (v_M_0 * v_gl_id_53))] = id(v__103);
      }
    }
  }
}}
