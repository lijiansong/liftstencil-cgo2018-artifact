
// High-level hash: a81e23369b1a39f5dce1bdfbb8295ca0a8a99358ebf481beae60a7b999b7f3f3
// Low-level hash: 4de9a2ab3c1e0e21826c0fae78415ea9b119dc9e53f2a7d4773704b3fa78b37f

#atf::var<int> v_M_0 = 512
#atf::var<int> v_N_1 = 512
#atf::var<int> v_O_2 = 8

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,10000) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::abort_condition "atf::cond::evaluations(1)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(8,8)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(8,8)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::input "atf::scalar<int>(v_O_2)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
  { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }
}
float idfloat(float x){
  { return x; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__81, const global float* restrict v__82, global float* v__172, int v_M_0, int v_N_1, int v_O_2){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__96; 
  float v__152; 
  float v__154; 
  float v__156; 
  float v__158; 
  float v__160; 
  float v__162; 
  float v__164; 
  float v__166; 
  /* Private Memory */
  float v__151; 
  float v__153; 
  float v__155; 
  float v__157; 
  float v__159; 
  float v__161; 
  float v__163; 
  float v__165; 
  float v__167; 
  float v__171; 
  for (int v_gl_id_71 = get_global_id(2); v_gl_id_71 < v_O_2; v_gl_id_71 = (v_gl_id_71 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_77 = get_global_id(1); v_gl_id_77 < v_N_1; v_gl_id_77 = (v_gl_id_77 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_79 = get_global_id(0); v_gl_id_79 < v_M_0; v_gl_id_79 = (v_gl_id_79 + GLOBAL_SIZE_0)) {
        float v_tmp_277 = 0.86186665f; 
        v__96 = v_tmp_277; 
        v__151 = idfloat(v__96); 
        float v_tmp_295 = 0.03413333f; 
        v__152 = v_tmp_295; 
        v__153 = idfloat(v__152); 
        float v_tmp_296 = 0.03413333f; 
        v__154 = v_tmp_296; 
        v__155 = idfloat(v__154); 
        float v_tmp_297 = 0.03413333f; 
        v__156 = v_tmp_297; 
        v__157 = idfloat(v__156); 
        float v_tmp_300 = 0.03413333f; 
        v__158 = v_tmp_300; 
        v__159 = idfloat(v__158); 
        float v_tmp_301 = 5.333333E-4f; 
        v__160 = v_tmp_301; 
        v__161 = idfloat(v__160); 
        float v_tmp_302 = 5.333333E-4f; 
        v__162 = v_tmp_302; 
        v__163 = idfloat(v__162); 
        float v_tmp_303 = 0.3413333f; 
        v__164 = v_tmp_303; 
        v__165 = idfloat(v__164); 
        float v_tmp_304 = 80.0f; 
        v__166 = v_tmp_304; 
        v__167 = idfloat(v__166); 
        v__171 = calculateHotspot(v__81[((v_M_0 * v_N_1 * ( (v_gl_id_71 >= 0) ? ( (v_gl_id_71 < v_O_2) ? v_gl_id_71 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_77 >= 0) ? ( (v_gl_id_77 < v_N_1) ? v_gl_id_77 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_79 >= 0) ? ( (v_gl_id_79 < v_M_0) ? v_gl_id_79 : (-1 + v_M_0) ) : 0 ))], v__151, v__81[((v_M_0 * v_N_1 * ( (v_gl_id_71 >= 0) ? ( (v_gl_id_71 < v_O_2) ? v_gl_id_71 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((-1 + v_gl_id_77) >= 0) ? ( ((-1 + v_gl_id_77) < v_N_1) ? (-1 + v_gl_id_77) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_79 >= 0) ? ( (v_gl_id_79 < v_M_0) ? v_gl_id_79 : (-1 + v_M_0) ) : 0 ))], v__153, v__81[((v_M_0 * v_N_1 * ( (v_gl_id_71 >= 0) ? ( (v_gl_id_71 < v_O_2) ? v_gl_id_71 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( ((1 + v_gl_id_77) >= 0) ? ( ((1 + v_gl_id_77) < v_N_1) ? (1 + v_gl_id_77) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_79 >= 0) ? ( (v_gl_id_79 < v_M_0) ? v_gl_id_79 : (-1 + v_M_0) ) : 0 ))], v__155, v__81[((v_M_0 * v_N_1 * ( ((1 + v_gl_id_71) >= 0) ? ( ((1 + v_gl_id_71) < v_O_2) ? (1 + v_gl_id_71) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_77 >= 0) ? ( (v_gl_id_77 < v_N_1) ? v_gl_id_77 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_79 >= 0) ? ( (v_gl_id_79 < v_M_0) ? v_gl_id_79 : (-1 + v_M_0) ) : 0 ))], v__157, v__81[((v_M_0 * v_N_1 * ( ((-1 + v_gl_id_71) >= 0) ? ( ((-1 + v_gl_id_71) < v_O_2) ? (-1 + v_gl_id_71) : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_77 >= 0) ? ( (v_gl_id_77 < v_N_1) ? v_gl_id_77 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_79 >= 0) ? ( (v_gl_id_79 < v_M_0) ? v_gl_id_79 : (-1 + v_M_0) ) : 0 ))], v__159, v__81[((v_M_0 * v_N_1 * ( (v_gl_id_71 >= 0) ? ( (v_gl_id_71 < v_O_2) ? v_gl_id_71 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_77 >= 0) ? ( (v_gl_id_77 < v_N_1) ? v_gl_id_77 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_79) >= 0) ? ( ((1 + v_gl_id_79) < v_M_0) ? (1 + v_gl_id_79) : (-1 + v_M_0) ) : 0 ))], v__161, v__81[((v_M_0 * v_N_1 * ( (v_gl_id_71 >= 0) ? ( (v_gl_id_71 < v_O_2) ? v_gl_id_71 : (-1 + v_O_2) ) : 0 )) + (v_M_0 * ( (v_gl_id_77 >= 0) ? ( (v_gl_id_77 < v_N_1) ? v_gl_id_77 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_79) >= 0) ? ( ((-1 + v_gl_id_79) < v_M_0) ? (-1 + v_gl_id_79) : (-1 + v_M_0) ) : 0 ))], v__163, v__165, v__82[(v_gl_id_79 + (v_M_0 * v_N_1 * v_gl_id_71) + (v_M_0 * v_gl_id_77))], v__167); 
        v__172[(v_gl_id_79 + (v_M_0 * v_N_1 * v_gl_id_71) + (v_M_0 * v_gl_id_77))] = id(v__171); 
      }
    }
  }
}}


