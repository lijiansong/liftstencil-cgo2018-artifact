
// High-level hash: aa18ec92e2e0d9faabc866c1fb29ea5e49286a12f9dde71ae4003773fec8a4e5
// Low-level hash: 07aad9209f110c4d7ca0195ba7f9132fae3c2f3c66445cf8e4cebd0f7bf3d3f3

#atf::var<int> v_M_0 = 4096
#atf::var<int> v_N_1 = 4096

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(900)"

#atf::abort_condition "atf::cond::evaluations(1)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(4096,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 4; }"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float hotspot(float power, float top, float bottom, float left, float right, float center){
  
#define MAX_PD  (3.0e6)
#define PRECISION   0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
#define FACTOR_CHIP 0.5

    /* chip parameters  */
    const float t_chip = 0.0005f;
    const float chip_height = 0.016f;
    const float chip_width = 0.016f;
    /* ambient temperature, assuming no package at all  */
    const float amb_temp = 80.0f;

    float row = 512.0f;
    float col = 512.0f;

    float grid_height = chip_height / row;
    float grid_width = chip_width / col;

    float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
    float Rx = grid_width / (2.0 * K_SI * t_chip * grid_height);
    float Ry = grid_height / (2.0 * K_SI * t_chip * grid_width);
    float Rz = t_chip / (K_SI * grid_height * grid_width);

    float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
    float stepl = PRECISION / max_slope;

    float step_div_Cap=stepl/Cap;
    float Rx_1=1/Rx;
    float Ry_1=1/Ry;
    float Rz_1=1/Rz;

    return center +
       step_div_Cap * (power + (bottom + top - 2.0f * center) * Ry_1 +
               (right + left - 2.0f * center) * Rx_1 + (amb_temp - center) * Rz_1);

    
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__23, const global float* restrict v__24, global float* v__33, int v_M_0, int v_N_1){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__31; 
  for (int v_gl_id_17 = get_global_id(0); v_gl_id_17 < v_N_1; v_gl_id_17 = (v_gl_id_17 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_18 = get_global_id(1); v_gl_id_18 < v_M_0; v_gl_id_18 = (v_gl_id_18 + GLOBAL_SIZE_1)) {
      v__31 = hotspot(v__24[(v_gl_id_18 + (v_M_0 * v_gl_id_17))], v__23[((v_M_0 * ( ((-1 + v_gl_id_17) >= 0) ? ( ((-1 + v_gl_id_17) < v_N_1) ? (-1 + v_gl_id_17) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_0) ? v_gl_id_18 : (-1 + v_M_0) ) : 0 ))], v__23[((v_M_0 * ( ((1 + v_gl_id_17) >= 0) ? ( ((1 + v_gl_id_17) < v_N_1) ? (1 + v_gl_id_17) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_0) ? v_gl_id_18 : (-1 + v_M_0) ) : 0 ))], v__23[((v_M_0 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_1) ? v_gl_id_17 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_18) >= 0) ? ( ((-1 + v_gl_id_18) < v_M_0) ? (-1 + v_gl_id_18) : (-1 + v_M_0) ) : 0 ))], v__23[((v_M_0 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_1) ? v_gl_id_17 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_18) >= 0) ? ( ((1 + v_gl_id_18) < v_M_0) ? (1 + v_gl_id_18) : (-1 + v_M_0) ) : 0 ))], v__23[((v_M_0 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_1) ? v_gl_id_17 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_0) ? v_gl_id_18 : (-1 + v_M_0) ) : 0 ))]); 
      v__33[(v_gl_id_18 + (v_M_0 * v_gl_id_17))] = id(v__31); 
    }
  }
}}


