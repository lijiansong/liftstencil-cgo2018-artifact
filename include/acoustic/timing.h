#include <sys/time.h>

double getTime() // same timing call as for stream
{
        struct timeval tp;
//        struct timezone tzp;
        int i;

        i = gettimeofday(&tp,NULL);
        return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}


double get_current_time() // borrowed from stencil
{
    static int start = 0, startu = 0;
    struct timeval tval;
    double result;
 
    if (gettimeofday(&tval, NULL) == -1)
       result = -1.0;
    else if(!start) {
       start = tval.tv_sec;
       startu = tval.tv_usec;
       result = 0.0;
    }
    else
       result = (double) (tval.tv_sec - start) + 1.0e-6*(tval.tv_usec - startu);
 
    return result;
}
