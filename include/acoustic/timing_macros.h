#ifndef TIMING_MACROS_H
#define TIMING_MACROS_H

char timingOutputFile[MAX_STR_LEN];
char timingString[MAX_STR_LEN] = "";
char infoString[MAX_STR_LEN] = "";
char gitDir[MAX_STR_LEN] = "";

#define printOutputs puts(timingString); 

#define printToString sprintf(timingString,"Program Build: %.14f\nKernel1: %.14f\nKernel2: %.14f\nKernels: %.14f\nData Copy To: %.14f\nData Copy Between: %.14f\nData Copy Back: %.14f\nData Copy Total: %.14f\nTotal time: %5.6lf \nNF: %d\nBandwidth: %4.6f\nGFLOPS: %4.6f\n",programBuildTotal,kernel1Time,kernel2Time,kernelsTime,dataCopyInitTotal, dataCopyBtwTotal, dataCopyBackTotal, dataCopyTotal, totalTime,numberSamples,getBandWidth(kernel1Time), getGFLOPS(kernel1Time));

/*#define collectEvents kernel1Time = getTimeForAllEvents(numberSamples,kernel1Events);\
                      kernel2Time = getTimeForAllEvents(numberSamples,kernel2Events);\
                      dataCopyBtwTotal =  getTimeForAllEvents(numberSamples,copyU1Events) +\
                        getTimeForAllEvents(numberSamples,copyUDEvents) +\
                        getTimeForAllEvents(numberSamples,copyDummyEvents);\ 
                      dataCopyTotal = dataCopyInitTotal + dataCopyBtwTotal + dataCopyBackTotal;
*/
#define writeTimingsToFile writeStringsToFile(timingString,infoString,getTimingFileName());


double startKernel1, endKernel1, startKernel2, endKernel2, startKernels, endKernels, kernel1Time, kernel2Time, kernelsTime, startTime, endTime, totalTime;
double dataCopyInitStart, dataCopyInitEnd, dataCopyInitTotal, dataCopyBtwStart, dataCopyBtwEnd, dataCopyBtwTotal, dataCopyBackStart, dataCopyBackEnd, dataCopyBackTotal, dataCopyTotal, programBuildStart, programBuildEnd, programBuildTotal;



#endif
