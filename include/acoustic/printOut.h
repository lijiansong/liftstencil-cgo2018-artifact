#ifndef PRINTOUT_H
#define PRINTOUT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void printMat(value* mat)
{
    for(int kk=0;kk<Nz;kk++)
    {
        for(int jj=0;jj<Ny;jj++)
        {
            for(int ii=0;ii<Nx;ii++)
            {
	         int cp   = kk*Nx*Ny+(jj*Nx+ii);
                 printf(" (%.2f) ",mat[cp]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

void printMatWithSizes(value* mat, int nx, int ny, int nz)
{
    for(int kk=0;kk<nz;kk++)
    {
        for(int jj=0;jj<ny;jj++)
        {
            for(int ii=0;ii<nx;ii++)
            {
	         int cp   = kk*nx*ny+(jj*nx+ii);
                 printf(" (%.8f) ",mat[cp]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

void printMatWithSizesInt(int* mat, int nx, int ny, int nz)
{
    for(int kk=0;kk<nz;kk++)
    {
        for(int jj=0;jj<ny;jj++)
        {
            for(int ii=0;ii<nx;ii++)
            {
	         int cp   = kk*nx*ny+(jj*nx+ii);
                 printf(" (%d) ",mat[cp]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

void printAs1D(value* mat)
{
    for(int i=0; i<Nx*Ny*Nz;i++)
    {
                 printf(" (%.2f) ",mat[i]);

    }
    printf("\n");
}

void printAs1DWithSize(value* mat, int size)
{
    for(int i=0; i<size;i++)
    {
        printf(" (%.2f) ",mat[i]);
        printf("\n");

    }
    printf("\n");
}


#endif
