#ifndef cl_utils_h
#define cl_utils_h

#include<CL/cl.h>
#include <stdio.h>
#include "Audio.h"

#define ACC_PROC CL_DEVICE_TYPE_ACCELERATOR 
#define CPU_PROC CL_DEVICE_TYPE_CPU 
#define GPU_PROC CL_DEVICE_TYPE_GPU 

char* getProcessorVersion()
{
    int size = 100;
    char hostname[size];
    getHostName(hostname,size);

    if(strstr(hostname,"agaete") || strstr(hostname,"pookie"))
    {
        return CPU_PROC;
    }
    else
    {
        return GPU_PROC;
    }
}

const char *getErrorString(cl_int error)
{
switch(error){
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}

void err_check( int err ) {
    if ( err != CL_SUCCESS ) {
            printf("Error: %d ( %s )\n",err,getErrorString(err));
            exit(1);
    }
}

void print_thread_info(cl_device_id device_id)
{
        char buffer[5000];
        size_t bufi;
        size_t workitem_size[3];
        
     clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(bufi), &bufi, NULL);
        printf("MAX_WORKGROUP_SIZE: %zu\n",bufi);
/*        clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(bufi), bufi, NULL);
        printf("MAX_WORK_ITEM_SIZES: %d\n",bufi);*/
         clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
             printf(  " CL_DEVICE_MAX_WORK_ITEM_SIZES:\t%zu / %zu / %zu \n", workitem_size[0], workitem_size[1], workitem_size[2]);
}

int getPlatformID(int platformCount)
{
    return 0;
//    return platformCount - 1;
}


//ORIGINAL
cl_device_id create_device_org() {

   cl_platform_id platform;
   cl_platform_id *platforms;
   cl_device_id dev;
   cl_uint platformCount;
   int err;
   char* info;
   size_t infoSize;
   char buffer[10245];


   const char* attributeNames[5] = { "Name", "Vendor",
             "Version", "Profile", "Extensions" };
   const cl_platform_info attributeTypes[5] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR,
             CL_PLATFORM_VERSION, CL_PLATFORM_PROFILE, CL_PLATFORM_EXTENSIONS };
   const int attributeCount = sizeof(attributeNames) / sizeof(char*);
   /* Identify a platform */

    clGetPlatformIDs(5, NULL, &platformCount);



   int platformID = getPlatformID(platformCount);//platformCount - 1; 
     
    // get all platforms
    platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id) * platformCount);

    clGetPlatformIDs(platformCount, platforms, NULL);

    printf("Platforms: %d\n",platformCount);
    for (int i = 0; i < platformCount; i++) 
    {
        printf("n %d. Platform \n", i+1);
        for (int j = 0; j < attributeCount; j++) 
        {
          clGetPlatformInfo(platforms[i], attributeTypes[j], 0, NULL, &infoSize);
          info = (char*) malloc(infoSize);
          clGetPlatformInfo(platforms[i], attributeTypes[j], infoSize, info, NULL);
          printf("  %d.%d %-11s: %s\n", i+1, j+1, attributeNames[j], info);
          free(info);
        }
    }
    
   // printf("platform id: %d \n", platforms[platformID]);
   if(err < 0) {
      perror("Couldn't identify a platform \n");
      err_check(err);
      exit(1);
   } 

   /* Access a device */
   err = clGetDeviceIDs(platforms[platformID], getProcessorVersion(), 1, &dev, NULL);
   clGetDeviceInfo(dev, CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL);
   printf("driver: %s\n",buffer);
   if(err == CL_DEVICE_NOT_FOUND) {
      err = clGetDeviceIDs(platforms[platformID], getProcessorVersion(), 1, &dev, NULL);
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);   
   }

    free(platforms);
   return dev;
}



cl_platform_id get_platform(int platform_id)
{
   cl_platform_id *platforms;
   cl_uint platformCount;
   int err;
   char* info;
   size_t infoSize;
   char buffer[10245];

   const char* attributeNames[5] = { "Name", "Vendor",
             "Version", "Profile", "Extensions" };
   const cl_platform_info attributeTypes[5] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR,
             CL_PLATFORM_VERSION, CL_PLATFORM_PROFILE, CL_PLATFORM_EXTENSIONS };
   const int attributeCount = sizeof(attributeNames) / sizeof(char*);
   /* Identify a platform */

    clGetPlatformIDs(5, NULL, &platformCount);
     
    // get all platforms
    platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id) * platformCount);

    clGetPlatformIDs(platformCount, platforms, NULL);
        
    printf("n %d. Platform \n", platform_id);
    for (int j = 0; j < attributeCount; j++) 
    {
        clGetPlatformInfo(platforms[platform_id], attributeTypes[j], 0, NULL, &infoSize);
        info = (char*) malloc(infoSize);
        clGetPlatformInfo(platforms[platform_id], attributeTypes[j], infoSize, info, NULL);
        printf("  %d.%d %-11s: %s\n", platform_id+1, j+1, attributeNames[j], info);
        free(info);
    }
    
   if(err < 0) {
      perror("Couldn't identify a platform \n");
      err_check(err);
      exit(1);
   } 

   return platforms[platform_id];
}

cl_context createContext(cl_platform_id platform)
{
   int err;
  cl_context_properties context_properties[3] = {CL_CONTEXT_PLATFORM,
  					       (cl_context_properties)platform, 0};
  return clCreateContextFromType(context_properties, CL_DEVICE_TYPE_GPU, NULL, NULL, &err);
}

cl_device_id create_device( int device_id, cl_context context, cl_platform_id platform ) {

   cl_device_id dev;
   int err;
   char buffer[10245];

  // Create a GPU context
    /*
  if (err != CL_SUCCESS) err_check(err);
  
  // Get and print the chosen device (if there are multiple devices, choose the first one)
  // Access a device 
  size_t devices_size;
  err = clGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL, &devices_size);
  if (err != CL_SUCCESS) err_check(err);
  cl_device_id *devices = (cl_device_id *)malloc(devices_size);
  err = clGetContextInfo(context, CL_CONTEXT_DEVICES, devices_size, devices, NULL);
  if (err != CL_SUCCESS) err_check(err);
  dev = devices[device_id];
  err = clGetDeviceInfo(dev, CL_DEVICE_NAME, sizeof(buffer), buffer, NULL);
  if (err != CL_SUCCESS) err_check(err);
  printf("Device: %s\n", buffer);
  return dev;
   */

   err = clGetDeviceIDs(platform, getProcessorVersion(), 1, &dev, NULL);
   clGetDeviceInfo(dev, CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL);
   printf("driver: %s\n",buffer);
   if(err == CL_DEVICE_NOT_FOUND) {
      err = clGetDeviceIDs(platform, getProcessorVersion(), 1, &dev, NULL);
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);   
   }

   return dev;
}

/* Create program from a file and compile it */
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
   int err;

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file */
   program = clCreateProgramWithSource(ctx, 1, 
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program */
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}

long getTimeForAllEvents(int numEvents, cl_event* events)
{
    long time = 0;
    cl_int err;

    err = clWaitForEvents(numEvents,events);
    
    cl_ulong start,end;
    
    for(int k=0; k<numEvents; k++)
    {
        err = clGetEventProfilingInfo(events[k],CL_PROFILING_COMMAND_END, 
                    sizeof(cl_ulong), &end, NULL);
        err = clGetEventProfilingInfo(events[k],CL_PROFILING_COMMAND_START, 
                    sizeof(cl_ulong), &start, NULL);
        time += (end-start);
    }

    return time;
}

#endif

