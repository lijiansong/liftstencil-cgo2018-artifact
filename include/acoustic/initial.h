#ifndef INITIAL_H
#define INITIAL_H
#include "constants.h"


#define STENCIL_KERNEL_FUNC "UpdateScheme"
#define INOUT_KERNEL_FUNC "inout"
#define PROGRAM_NAME "kernelsGrid.cl"

#define data_directory "/workspace/phd/data/"
#define mic_directory "/home-hydra/h022/s1147290/workspace/phd/"

#define CUDA_STR "cuda"
#define UNOPT_CUDA_STR "unopt_cuda"
#define OPENCL_STR "opencl"
#define OPENCLCPP_STR  "openclcpp" 
#define ABSTRACT_STR "abstract"
#define TARGETDP_STR "targetDP" // need to add "cuda" or "CC" dep if using OMP???

#define GET_NEIGHBOR_SUM(u, cp) u[cp-1]+u[cp+1]+u[cp-Nx]+u[cp+Nx]+u[cp-AREA]+u[cp+AREA]

#define CALCULATE_INDEX(x,y,z) z*AREA+(y*Nx+x);



#endif
