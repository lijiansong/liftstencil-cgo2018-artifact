#ifndef AUDIO_H
#define AUDIO_H

#if defined(_OPENMP)
#include <omp.h>
#endif
#include <pwd.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include "initial.h"
#include "timing_macros.h"

value getBandWidth(double kernelTime)
{
    // 9 memory accesses in kernel 1, 2 grids
    value bandWidth = (VOLUME*sizeof(double)*1e-9*3)/(kernelTime/numberSamples); 
    return bandWidth;
}

value getGFLOPS(double kernelTime)
{
    // 13 floating point operations in kernel 1
    value gflops = (VOLUME*1e-9*13)/(kernelTime/numberSamples); 
    return gflops;
}

void printConstants()
{
    printf("Room size: %d %d %d \n",Nx,Ny,Nz);
    printf("Work Group size: %d %d %d \n",Bx,By,Bz);
    printf("Number of samples: %d \n",numberSamples);
}


enum BOOLEAN { FALSE, TRUE };
char dataOutputFile[MAX_STR_LEN];

void checkMakeDir(char* outputDir)
{
    struct stat st = {0};

    if (stat(outputDir, &st) == -1) 
    {
        mkdir(outputDir, 0777);
    }
}

const char* getHomeDirectory()
{
    struct passwd *pw = getpwuid(getuid());
    return pw->pw_dir;
}

void getHostName(char* hostname, int size)
{
    gethostname(hostname,size);
}

void getDataDirectory(char* dataDir)
{
    int size = 100;
    char hostname[size];
    getHostName(hostname,size);

    if(strstr(hostname,"mic0") || strstr(hostname,"phi-mic1"))
    {
//         sprintf(dir,"./");
        sprintf(dataDir,"%sroom_code/data/",mic_directory);
//        printf("%s\n",dir);
    }
    else
    {
        sprintf(dataDir,"%s%s",getHomeDirectory(),data_directory); 
    }
}

void getTimeStamp(char* stamp, int size)
{
    char cmd[150];
    sprintf(cmd,"date +%%s");
    FILE* fp;
    fp = popen(cmd,"r");
    fgets(stamp,size-1,fp);
    pclose(fp);
    stamp[strcspn(stamp, "\n")] = 0;
}

void printGitRepo()
{
    char cmd[150];
    char repo[150];
    int lengthOfGitRepo = 11; // actually 10!
    char endArr[] = "/";
    int size = 100;
    char hostname[size];
    getHostName(hostname,size);
    if(strstr(hostname,"mic0") || strstr(hostname,"phi-mic1"))
    {
        sprintf(cmd,"cat %s.git/refs/heads/master",mic_directory);
    }
    else
    {
        sprintf(cmd,"git rev-parse HEAD");
    }
    FILE* fp;
    fp = popen(cmd,"r");
    if(fp == NULL)
    {
        printf("Problem getting git repo!\n");
    }
    fgets(gitDir,lengthOfGitRepo,fp);
    pclose(fp);
    repo[strcspn(gitDir, "\n")] = 0;
    strcat(gitDir,endArr);
}

void getGitRepo(char* repo)
{
    char cmd[150];
    int lengthOfGitRepo = 11; // actually 10!
    char endArr[] = "/";
    int size = 100;
    char hostname[size];
    getHostName(hostname,size);
    if(strstr(hostname,"mic0") || strstr(hostname,"phi-mic1"))
    {
            sprintf(repo,"%s",gitDir);
    //    sprintf(cmd,"cat %s.git/refs/heads/master",mic_directory);
    }
    else
    {
        sprintf(cmd,"git rev-parse HEAD");
    FILE* fp;
    fp = popen(cmd,"r");
    if(fp == NULL)
    {
        printf("Problem getting git repo!\n");
    }
    fgets(repo,lengthOfGitRepo,fp);
    pclose(fp);
    repo[strcspn(repo, "\n")] = 0;
    strcat(repo,endArr);
    }
}

char* getPlatform(char* hostname)
{
    if(strstr(hostname, "kepler"))
    {
        return "nvidia_kepler";
    }
    else if(strstr(hostname,"casper"))
    {
        return "nvidia_maxwell";
    }
    else if(strstr(hostname,"phi"))
    {
        return "xeon_phi";
    }
    else if(strstr(hostname,"supersonic"))
    {
        return "AMD_E5530";
    }
    else if(strstr(hostname,"suzuka"))
    {
        return "AMD_i7_4470K";
    }
    else if(strstr(hostname,"fuji"))
    {
        return "AMD_R280";
    }
    else if(strstr(hostname,"monza"))
    {
        return "AMD_7970";
    }
    else if(strstr(hostname,"spa"))
    {
        return "xeon_phi";
    }
    else if(strstr(hostname,"monaco"))
    {
        return "nvidia_tesla";
    }
    else
    {
        return hostname;
    }
}


void CreateSineWave(int duration, value* si_h)
{
    int n;

    for(n=0;n<duration;n++)
    {
        si_h[n] = 0.5*(1.0-cos(2.0*pi*n/(value)duration));
    }
}

char* getOutputFileName(char* program_type, char* fileType, char* ext)
{
    int size = 100;
    char hostname[size];
    getHostName(hostname,size);
    char* platform = getPlatform(hostname);
    dataOutputFile[0] = 0;
    sprintf(dataOutputFile,"%s-%s-%s-A%dx%dx%d-S%dx%dx%dx-R%dx%dx%d-NF%d.%s",program_type,platform,fileType,Nx,Ny,Nz,Sx,Sy,Sz,Rx,Ry,Rz,numberSamples,ext);
    return dataOutputFile;
}

void printToTimingFileName(char* program_type)
{
    int size = 100;
    char hostname[size];
    char timestamp[size];
    getHostName(hostname,size);
    getTimeStamp(timestamp,size);
    
    char* platform = getPlatform(hostname);
    sprintf(timingOutputFile,"%s-%s-timings-A%dx%dx%d-S%dx%dx%dx-R%dx%dx%d-NF%d-%s.txt",program_type,platform,Nx,Ny,Nz,Sx,Sy,Sz,Rx,Ry,Rz,numberSamples,timestamp);
//    sprintf(timingOutputFile,"%s-%s-timings-NF%d-%s.txt",program_type,platform,numberSamples,timestamp);
}

char* getTimingFileName()
{
    return timingOutputFile;
}

char* concatStrings(char* str1, char* str2)
{
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    
    char* wholeStr=(char*)malloc(len1+len2+1);
    strcpy(wholeStr,str1);
    strcat(wholeStr,str2);
    return wholeStr;
}

void getFullFilePath(char* path, char* fileString)
{

    char gitDir[500];
    getGitRepo(gitDir);
    getDataDirectory(path);
    strcat(path,gitDir);
    checkMakeDir(path);
    strcat(path,fileString);

}

void writeStringsToFile(char* timingStr, char* infoStr, char* fileString)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,fileString);

    FILE * file;
    file = fopen(wholeFile, "w");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }
    if(infoStr != "")
    {   
        fprintf(file, "%s\n", infoStr);
    }
    fprintf(file, "%s\n", timingStr);

    fclose(file);
}


void writeDoublesToFile(double *data, char* output, int ndata)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,output);
    
    
    FILE * file;
    file = fopen(wholeFile, "w");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        fprintf(file, "%f\n", data[i]);
    }

    fclose(file);
}

void writeFloatsToFile(float *data, char* output, int ndata)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,output);
    
    
    FILE * file;
    file = fopen(wholeFile, "w");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        fprintf(file, "%.32f\n", data[i]);
    }

    fclose(file);
}

void readFloatsFromFile(float *data, char* readFile, int ndata)
{
    FILE * file;
    file = fopen(readFile, "r");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",readFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        fscanf(file, "%f\n", &data[i]);
    }

    fclose(file);
}

void writeFloatsToFileNoZero(float *data, char* output, int ndata)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,output);
    
    
    FILE * file;
    file = fopen(wholeFile, "w");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        if(data[i]>0.0)
        {
            fprintf(file, "%f\n", data[i]);
        }
    }

    fclose(file);
}
/*void writeBinaryDataToFile(float *data, char* output, int ndata)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,output);

    FILE * file;
    file = fopen(wholeFile, "wb");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        fwrite(&data[i], sizeof(float),1,file);
    }

    fclose(file);
}*/

void writeBinaryDataToFile(value *data, char* output, int ndata)
{
    char wholeFile[1024];
    getFullFilePath(wholeFile,output);
    printf("wholeFile: %s\n",wholeFile);
    FILE * file;
    file = fopen(wholeFile, "wb");
    
    if(file == NULL)
    {
        printf("Error opening file: %s!",wholeFile);
        exit(1);
    }

    int i;
    for(i=0; i<ndata; i++)
    {
        fwrite(&data[i], sizeof(value),1,file);
    }

    fclose(file);
}

#if defined(_OPENMP)
int printOMPThreads()
{

    int tid, nthreads;
    #pragma omp parallel private(nthreads, tid)
    {
        tid = omp_get_thread_num();
        printf("Hello World from thread = %d\n", tid);

        if (tid == 0) 
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }    

    }
    return nthreads;
}
#endif

#endif
