library(data.table)
library(plyr)
library(reshape2)
library(ggplot2)

# we'll use this function to extract info from our data path
# thanks, stack overflow!
split_path <- function(x) if (dirname(x)==x) x else c(basename(x),split_path(dirname(x))) 

# hard code some strings so we don't have to think about them 
ACOUSTICSTR <- "acoustic"
HOTSPOTSTR <- "hotspot"
HOTSPOT3DSTR <- "hotspot3D"
SRADSTR <- "srad"
STENCIL2DSTR <- "stencil2d"

# directory with data
dir <- "../../output_data"
ext <- "out"

# directory to output plot(s)
plotDir <- "../../plots"


# pull out all relevant files
dataFiles <- system(paste("find ",dir,"/*"," | grep ",ext,"$",sep=""),intern=TRUE)

# create initial data frame
columns <- c("platform", "version", "benchmark", "runtime") # version = { lift, reference }
frame <- data.frame(matrix(ncol = length(columns)))
colnames(frame) <- columns

# loop over files 
for (i in 1:length(dataFiles))
{
    # save whole path, then will carve out the info we need
    wholePathName <- dataFiles[i]
    pathArray <- split_path(wholePathName)
    platform <- pathArray[3] 
    version <- pathArray[2] 
    benchmark <- pathArray[1] 
    benchmark <- strsplit(benchmark,"[.]")[[1]][1] # throw away the extension in this really convoluted way

    medValue <- 0
    # now open the file and get the results
    if (!file.size(wholePathName) == 0) 
    {
        values <- read.csv(wholePathName, header=FALSE)
        medValue <- median(values[[1]]) 
    } 
    # add row to the dataframe 
    frame[i,] <- c(platform, version, benchmark, medValue)
}

# add new column for data sizes
frame$size <- 0

# then tediously add in all the sizes
frame$size[grepl(ACOUSTICSTR,frame$benchmark)] <- 13238272
frame$size[grepl(HOTSPOTSTR,frame$benchmark)] <- 67108864
frame$size[grepl(HOTSPOT3DSTR,frame$benchmark)] <- 2097152
frame$size[grepl(SRADSTR,frame$benchmark)] <- 229916
frame$size[grepl(STENCIL2DSTR,frame$benchmark)] <- 16777216

# do the plot
frame$size[frame$platform=="mali" & frame$version=="lift" & frame$benchmark=="hotspot2D"] <- 4096*4096
frame$size[frame$platform=="mali" & frame$version=="reference" & frame$benchmark=="hotspot2D"] <- 4096*4096

frame <- transform(frame, runtime_over_size=(as.numeric(frame$size)/as.numeric(frame$runtime)))
frame$runtime_over_size[!is.finite(frame$runtime_over_size)] <- 0
frame <- aggregate(runtime_over_size ~ benchmark + platform + runtime + size + version, frame, FUN=max)
frame$platform <- as.factor(frame$platform)

frame$platform = factor(frame$platform, levels=levels(frame$platform)[c(3,1,2)])

# for debugging purposes:
# frame[order(frame$benchmark,frame$platform),]

plotGE <- ggplot(frame, aes(x=benchmark, y=runtime_over_size, fill=platform, alpha=version)) +
    geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
    scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD")) +
    scale_alpha_manual(values = c(0.6, 1.0)) +
    #scale_fill_brewer("Platform", palette="YlOrRd") +
    facet_wrap(~ platform, scales = "free") +
    # scale_y_log10(breaks=c(1,2,3,4,5,10,20)) +
    ylab("Gigaelements per Second") +
    xlab("Platform") +
    xlab(NULL) +
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )

ggsave(paste(plotDir,"/","workflow1-figure7.pdf",sep=""), width=10, height=2.5)
