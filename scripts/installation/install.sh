#!/bin/bash
pushd $ROOTDIR/tools/ > /dev/null
$ROOTDIR/scripts/installation/build_llvm.sh
$ROOTDIR/scripts/installation/build_ppcg.sh
$ROOTDIR/scripts/installation/build_atf.sh
popd > /dev/null
