#!/bin/bash

### README ##############################################
# REQUIRES bench.c, gold.txt, run.sh and tuner.sh
# bench.c - input c ppcg file
# gold.txt - last 4 lines of ppcg host error report
# run.sh - script executing tuner.sh
# tuner.sh - atfc tune-script to tune ppcg opencl

# requires also scripts used in (c/ocl)_ppcg.sh
#########################################################

FOLDER=$PWD
NAME="$(echo $FOLDER | sed 's/\// /g' | awk '{print $(NF-1)}')"
SUF=.c
FILE=$NAME$SUF

SCRIPT=ppcg_tune_script.sh

# copy c and gold.txt to tune dir
cp $FILE gold.txt $ROOTDIR/scripts/comparison_with_ppcg/run.sh $ATF/build
# copy and rename tune script
cp tuner.sh $ATF/build/$SCRIPT

# compile benchmark once to create executable
cd $ATF/build
compile_ppcg_c.sh $FILE
compile_ppcg_ocl.sh *host.c

# tune benchmark
./atfc -I -i $SCRIPT >> tune.out

# copy results
mv cost.csv meta.csv tune.out $FOLDER

# clean
rm result.txt
rm gold.txt
rm $SCRIPT
rm $NAME
rm $FILE
rm run.sh
rm costfile.txt
rm *kernel.cl
rm *host.c

cd - > /dev/null
