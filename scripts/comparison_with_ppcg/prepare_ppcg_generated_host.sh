#!/bin/bash
HOST="$(ls | grep host)"
: "${OCL_PLATFORM_ID:?Need to set OCL_PLATFORM_ID}"
: "${OCL_DEVICE_ID:?Need to set OCL_DEVICE_ID}"

############### CODE TO INSERT
echo '        unsigned long long elapsed = 18446744073709551615ULL;' > profile.txt
echo '        cl_ulong time_start, time_end;' >> profile.txt
echo '        clWaitForEvents(1, &mainKernelEvt);' >> profile.txt
echo '        clGetEventProfilingInfo(mainKernelEvt, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);' >> profile.txt
echo '        clGetEventProfilingInfo(mainKernelEvt, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);' >> profile.txt
echo '        elapsed = (time_end - time_start);' >> profile.txt
echo '        if(elapsed == 0) elapsed = 18446744073709551615ULL;' >> profile.txt
echo '        printf("[DEBUG] kernel runtime: %llu\n",elapsed);' >> profile.txt

echo '         size_t global_work_size[3];' > ndranges.txt
echo '         size_t block_size[3];' >> ndranges.txt
echo '         if(argc == 7) {' >> ndranges.txt
echo '             global_work_size[0] = atoi(argv[1]);' >> ndranges.txt
echo '             global_work_size[1] = atoi(argv[2]);' >> ndranges.txt
echo '             global_work_size[2] = atoi(argv[3]); ' >> ndranges.txt
echo '             block_size[0] = atoi(argv[4]);' >> ndranges.txt
echo '             block_size[1] = atoi(argv[5]);' >> ndranges.txt
echo '             block_size[2] = atoi(argv[6]);' >> ndranges.txt
echo '         } else {' >> ndranges.txt
echo '             global_work_size[0] = TMPGS0;' >> ndranges.txt
echo '             global_work_size[1] = TMPGS1;' >> ndranges.txt
echo '             global_work_size[2] = TMPGS2;' >> ndranges.txt
echo '             block_size[0] = TMPLS0;' >> ndranges.txt
echo '             block_size[1] = TMPLS1;' >> ndranges.txt
echo '             block_size[2] = TMPLS2;' >> ndranges.txt
echo '         }' >> ndranges.txt

echo '      char deviceName[1024];' > deviceName.txt
echo '      clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(deviceName), deviceName, NULL);' >> deviceName.txt
echo '      printf("Using: %s\n",deviceName);' >> deviceName.txt


EVENT_NAME=mainKernelEvt
cat $HOST > newHost.c
cat newHost.c | grep clEnqueueNDRangeKernel | while read -r LINE ; do
    # declare event before enqueueNDRangeKernel
    #KERNEL_NAME="$(echo $LINE | sed 's/ /\n/g' | grep kernel | sed 's/,//g' | sed 's/k/K/g')"
    PRE='cl_event '
    SEMICOLON=';'
    DECL_EVENT=$PRE$EVENT_NAME$SEMICOLON
    #echo $DECL_EVENT
    sed -i "/$LINE/ { N; s/$LINE\n/$DECL_EVENT\n&/ }" newHost.c

    # insert event when executing kernel
    NEW_LINE="$(echo $LINE | sed "s/NULL, NULL/NULL, $EVENT_NAME/")"
    #echo $NEW_LINE
    sed -i "s/$LINE/$NEW_LINE/g" newHost.c
done

sed -i "s/, $EVENT_NAME/, \&$EVENT_NAME/g" newHost.c
sed -i "/clEnqueueNDRangeKernel/r profile.txt" newHost.c
rm profile.txt

sed -i "s/clCreateCommandQueue(context, device, 0/clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE/g" newHost.c

#TODO only works for benchmarks wich use one kernel
############# MODIFY GLOBAL AND LOCAL SIZES ##################################################################
# get global sizes
cat newHost.c | grep "size_t global_work_size" | cut -d "{" -f2 | cut -d "}" -f1 | sed 's/, /\n/g' > GS.txt
GS0="$(cat GS.txt | head -n 1)"
GS1="$(cat GS.txt | head -n 2 | tail -n 1)"
GS2=""
if [ "$(cat GS.txt | wc -l)" -eq 2 ]; then
        GS2="1"
else
        GS2="$(cat GS.txt | head -n 3 | tail -n 1)"
fi

# get local sizes
cat newHost.c | grep "size_t block_size" | cut -d "{" -f2 | cut -d "}" -f1 | sed 's/, /\n/g' > LS.txt
LS0="$(cat LS.txt | head -n 1)"
LS1="$(cat LS.txt | head -n 2 | tail -n 1)"
LS2=""
if [ "$(cat LS.txt | wc -l)" -eq 2 ]; then
        LS2="1"
else
        LS2="$(cat LS.txt | head -n 3 | tail -n 1)"
fi

sed -i "/size_t global_work_size/d" newHost.c
sed -i "/size_t block_size/d" newHost.c
sed -i "/cl_kernel kernel0/r ndranges.txt" newHost.c
sed -i "s/TMPGS0/$GS0/g" newHost.c
sed -i "s/TMPGS1/$GS1/g" newHost.c
sed -i "s/TMPGS2/$GS2/g" newHost.c
sed -i "s/TMPLS0/$LS0/g" newHost.c
sed -i "s/TMPLS1/$LS1/g" newHost.c
sed -i "s/TMPLS2/$LS2/g" newHost.c

############# INSERT DEVICE NAME PRINTING ####################################################################
sed -i "/device = opencl_create_device/r deviceName.txt" newHost.c

# insert device and platform id here:
sed -i "s/opencl_create_device(1)/opencl_create_device_with_ids($OCL_PLATFORM_ID,$OCL_DEVICE_ID)/" newHost.c

# clean up
cat newHost.c > $HOST
rm ndranges.txt
rm deviceName.txt
rm GS.txt
rm LS.txt
rm newHost.c
