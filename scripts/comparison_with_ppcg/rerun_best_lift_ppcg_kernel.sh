#!/bin/bash
### README #######################################################################################
# This script needs to be executed in *Cl folder which contains the Lift-generated kernels 
# It uses the ppcg-generated OpenCL host- and verification code to evaluate the best kernel
#
# requirements:
# * folder structure:
#   base/bench/input_size/architecture/benchCl
#                                        ^ pwd
# * ppcg input c code needs to be in 'input_size' folder (e.g., small or big)
# * artifact/scripts in PATH
# * summary.txt in benchCl
# * kernel files and kernel.cost.csv files in benchCl
#
###################################################################################################

### INIT ##########################################################################################
RESULTS=results
# TODO this script should be executed in *Cl folders. rename three ours stuff?
BENCH="$(basename $PWD | sed 's/Cl//g' | sed 's/threeHours-//g')"
SIZE="$(echo $PWD | sed 's/\// /g' | awk '{ print $(NF-2) }')"
ARCH="$(echo $PWD | sed 's/\// /g' | awk '{ print $(NF-1) }')"
echo [INFO] Processing $BENCH
echo "[INFO] Running best found configuration for $ARCH ($SIZE input size)"

mkdir $RESULTS
# path to required host and gold files (structure: bench/input_size/architecture/benchCl)
#                                                             ^ ppcg-files          ^ pwd
PPCG_FILES=../..
cp $PPCG_FILES/$BENCH.c $RESULTS
cp $PPCG_FILES/gold.txt $RESULTS

cd $RESULTS
# generate ppcg ocl-host program which executes the best found lift kernel
compile_ppcg_c.sh $BENCH.c 
compile_ppcg_ocl.sh *host.c #&> /dev/null

# copy best lift kernel into results folder
KERNEL="$(cat ../summary.txt | tail -1 | awk '{print $1}')"
cp ../$KERNEL .
cp ../$KERNEL.cost.csv .

### CREATE BEST KERNEL ############################################################################
HEADER="$(cat ${KERNEL}.cost.csv | head -n 1)"
KEY="$(echo $HEADER | sed 's/;/ /g' | wc -w)" #last csv field contains runtime
cat $KERNEL.cost.csv | sort -n -r -k $KEY --field-separator=';' > sorted.csv #sort slowest to fastest

CONFLINE=1
echo "#################################" >> reproduction.log
CONFLINE=$((CONFLINE+1))
CONFIG="$(cat sorted.csv | tail -n $CONFLINE | head -1 | sed 's/;/ /g' )"
RUNTIME="$(echo $CONFIG | awk '{print $NF}')"
echo "[INFO] Using parameter configuration: $(echo $CONFIG | sed '$s/\w*$//')"
echo "Testing $CONFIG" >> reproduction.log

cp $KERNEL best.cl
# remove atf directives
sed -i '/atf::/d' best.cl
sed -i '/\\/d' best.cl
sed -i '/^$/d' best.cl

# replace all tuning parameters with the best found values
echo $HEADER | sed 's/\_/\\\_/g' | tr ";" "\n" | head -n -1 > header
echo $CONFIG | tr " " "\n" | head -n -1 > config
paste header config | while read n k; do sed -i "s/$n/$k/g" best.cl; done 

# remove auxiliary files
rm header
rm config

######### FIGURE OUT NDRANGES FOR THE CURRENT BEST KERNEL
# which benchmark has which dimensionality?
if      [ "$BENCH" == "grad2d" ] || 
        [ "$BENCH" == "gaussian" ] ||
        [ "$BENCH" == "j2d5pt" ] ||
        [ "$BENCH" == "j2d9pt" ]
then
    DIM=2
elif    [ "$BENCH" == "heat3d" ] ||
        [ "$BENCH" == "j3d13pt" ] ||
        [ "$BENCH" == "j3d7pt" ] ||
        [ "$BENCH" == "poisson3d" ] 
then
    DIM=3
else
    echo "something went really wrong..."
    exit -1
fi

NDRANGES="$(echo $CONFIG | sed s/'\w*$'//)"
# if 2d app but tiling applied still 6 args - drop two tiling args
if [ "$DIM" -eq 2 -a "$(echo $NDRANGES | wc -w)" -eq 6 ]; then
    NDRANGES="$(echo $NDRANGES | sed '$s/\w*$//' | sed -e 's/[[:space:]]*$//' |sed '$s/\w*$//')"
fi

# if only 2d ndranges are provided insert 1s
LEN="$(echo $NDRANGES | wc -w)"
if [ "$LEN" -eq 4 ]; then        
    NDRANGES="$(echo $NDRANGES | awk '{$3="1 "$3;print $0}' | awk '{$5=$5" 1";print $0}')"
fi

# assert len is now 6 everytime
LEN="$(echo $NDRANGES | wc -w)"
if [ "$LEN" -ne 6 ]; then        
    echo "something went really wrong, exit..."
    exit -1
fi

######### RUN KERNEL AND CHECK RUNTIME
SUF=_kernel.cl
cp best.cl $BENCH$SUF
sed -i 's/KERNEL/kernel0/g' $BENCH$SUF

echo [INFO] checking correctness...
./$BENCH $NDRANGES > tmp.txt 
cat tmp.txt | head -1 >> reproduction.log
cat tmp.txt | tail -4 > result.txt

TIME="$(cat tmp.txt | head -2 | tail -1 | awk '{print $4}')"

### COMPARE RESULTS ##########################################################
diff -y gold.txt result.txt  >> reproduction.log
GOLDMAX="$(cat gold.txt | tail -1 | awk '{print $4}')"
OURMAX="$(cat result.txt | tail -1 | awk '{print $4}')"
RESULT="$(python -c "print (abs($GOLDMAX-$OURMAX) > 0.005)")"
if [ $RESULT == False ] ; then
        echo -e "[INFO] Result check: \e[32mPASSED\e[0m"
        echo -e "PASSED" >> reproduction.log
else
        echo -e "\e[31mResult check: FAILED!\e[0m"
        echo -e "FAILED" >> reproduction.log
        python -c "print (abs($GOLDMAX-$OURMAX))" >> reproduction.log
fi

: "${ITERATIONS:?Need to set ITERATIONS}"
echo "[INFO] Measuring kernel $ITERATIONS times"
for i in $(seq 1 $ITERATIONS); do
        echo "[INFO] Executing run ($i/$ITERATIONS)"
        ./$BENCH $NDRANGES >> out.txt
done
cat out.txt | grep DEBUG | awk '{print $4}' > lift.runtime
rm tmp.txt

# clean
rm gold.txt
rm *host.c
rm *kernel.cl
rm result.txt
rm sorted.csv
rm $BENCH

cd - > /dev/null
