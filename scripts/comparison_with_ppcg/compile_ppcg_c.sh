#!/bin/bash
if [ "$#" -ne 1 ]; then
        echo "Illegal number of parameters (ppcg input code?)"
	exit -1
fi

: "${PPCG:?Need to set PPCG}"
ppcg --target=opencl $1 
prepare_ppcg_generated_host.sh
