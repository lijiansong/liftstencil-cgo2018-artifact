#!/bin/bash
if [ $# -ne 1 ]
  then
    echo "No arguments supplied - which host code to compile?"
		exit -1
fi

: "${PPCG:?Need to set PPCG}"
CODE=$1
OUT="$(echo $CODE | sed 's/_host.c//g')"
echo $OUT
gcc -std=c99 $CODE -o $OUT $PPCG/ocl_utilities.c -I$PPCG -lOpenCL -lm
