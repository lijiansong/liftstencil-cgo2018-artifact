#!/bin/bash
# check input
if [ $# -ne 1 ]
then
    echo "No arguments supplied - which architecture? (nvidia | amd | arm)?"
    exit -1
fi
if ! ([ "$1" == "nvidia" ] || [ "$1" == "amd" ] || [ "$1" == "arm" ])
then 
    echo "Wrong architecture! valid: (nvidia | amd | arm)?"
    exit -1
fi

# adjust input to folder structure
if [ "$1" == "nvidia" ]			
then
				ARCH="kepler"
fi
if [ "$1" == "amd" ]			
then
				ARCH="tahiti"
fi
if [ "$1" == "arm" ]			
then
				ARCH="mali"
fi

cd $ROOTDIR/benchmarks/figure8/workflow1
############################################
# 1. Run reference implementations
############################################
echo "Running PPCG references"
run_all_best_ppcg_kernels.sh $ARCH
