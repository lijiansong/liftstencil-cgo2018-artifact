## ATF library CMake project file
## 
## To use this when using directly from a parent CMake project, do:
##	add_subdirectory(atf)
##	target_link_libraries(<project> ${LIBATF_LIBRARIES})
##
## There is no need to manually set include directories, as this is automatically done.
##

cmake_minimum_required(VERSION 3.1)
project(atf)

# Set CMake module path
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

## PROJECT OPTIONS
## These can be toggled by on the command line, e.g:
## -DLIBATF_DISABLE_CUDA=OFF

# This will disable all CUDA related functionality.
# Useful when the CUDA SDK is not installed.
option(LIBATF_DISABLE_CUDA "Disable CUDA wrapper" ON)

# This will enable the saxpy example project.
option(LIBATF_BUILD_EXAMPLES "Build example projects" OFF)

# This will disable all warnings. The CL headers produce a lot of them.
option(LIBATF_DISABLE_WARNINGS "Disable compiler warnings" ON)

# This will enable the unit test suite.
option(LIBATF_ENABLE_TESTS "Enable test suite" OFF)



## PROJECT SETUP

# Source files
file(GLOB_RECURSE SOURCE_FILES LIST_DIRECTORIES false src/*.cpp)

# Build as library
add_library(atf ${SOURCE_FILES})

# Add include directory as public dependency
target_include_directories(atf PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

# Add include/atf as private dependency to make referencing to internal atf header
# in source files easier
target_include_directories(atf PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include/atf)

# Require support for at least C++14
set_property(TARGET atf PROPERTY CXX_STANDARD 14)
set_property(TARGET atf PROPERTY CXX_STANDARD_REQUIRED ON)

# Provide CMake variables to make usage of library easier
set(LIBATF_LIBRARIES atf CACHE INTERNAL "")

# Definitions
if(LIBATF_DISABLE_CUDA)
	target_compile_definitions(atf PUBLIC LIBATF_DISABLE_CUDA=1)
endif()

if(LIBATF_DISABLE_WARNINGS)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -w")
endif()



## DEPENDENCIES

# libpthread on unices or msys
# TODO does this work on MacOSX?
if((NOT WIN32) OR MSYS)
	target_link_libraries(atf pthread)
endif()

# Setup OpenCL dependency
find_package(OpenCL REQUIRED)
target_include_directories(atf PUBLIC ${OpenCL_INCLUDE_DIRS})
target_link_libraries(atf ${OpenCL_LIBRARY})

# Setup Python libraries dependency. We force 2.7 here to disallow accidental inclusion
# of Python3 (which happens on some Linux systems for some reason)
find_package(PythonLibs 2.7 REQUIRED)
target_include_directories(atf PUBLIC ${PYTHON_INCLUDE_DIR})
target_link_libraries(atf ${PYTHON_LIBRARIES})

# Setup CUDA dependency, if not disabled.
if(NOT LIBATF_DISABLE_CUDA)
	find_package(CUDA REQUIRED)
	find_package(NVRTC REQUIRED)
	target_link_libraries(atf ${CUDA_LIBRARIES} cuda ${CUDA_NVRTC_LIB})
	target_include_directories(atf PUBLIC ${CUDA_INCLUDE_DIRS})
endif()


## EXAMPLE PROJECTS
if(LIBATF_BUILD_EXAMPLES)
	add_subdirectory(examples)
endif()




## TESTS
if(LIBATF_ENABLE_TESTS)
	# Include catch subproject
	add_subdirectory(catch)

	# Enable testing and register all tests located in /tests/
	enable_testing()
	add_subdirectory(tests)

	# Create pecial testing target that shows test output
	add_custom_target(test_verbose COMMAND ${CMAKE_CTEST_COMMAND} --verbose)
endif()





