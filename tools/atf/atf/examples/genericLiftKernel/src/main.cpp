#include <atf.h>
#include <string>

int main(int argc, char **args) {
  if (argc == 2) {

    std::ifstream inputStream(args[1]);
    std::stringstream buffer;
    buffer << inputStream.rdbuf();
    std::string KERNEL(buffer.str());

    std::cout << "Kernel: " << KERNEL << "\n";

    const int N = 1024;

    auto GS = atf::tp("GS", atf::interval<int>(1, N));

    auto LS = atf::tp("LS", atf::interval<int>(1, N), atf::divides(GS));
    auto cf_KERNEL = atf::cf::ocl(
        {"NVIDIA", atf::cf::device_info::GPU, 0}, {KERNEL, "KERNEL"},
        inputs(atf::buffer<float>(N), // v__1 (InputArray)
               atf::buffer<float>(N), // v__2 (OutputArray)
               atf::scalar<int>()),   // v_N_0
        atf::cf::GS(GS), atf::cf::LS(LS));

    auto best_config = atf::annealing(
        atf::cond::duration<std::chrono::seconds>(2))(GS, LS)(cf_KERNEL);
  } else {
    std::cout << "Usage: " << args[0] << " [path_of_cl_kernel]\n";
  }
}
