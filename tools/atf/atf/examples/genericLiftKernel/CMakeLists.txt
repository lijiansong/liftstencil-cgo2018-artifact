## ATF library Example: genericLiftKernel
##

cmake_minimum_required(VERSION 3.1)
project(genericLiftKernel)


## PROJECT SETUP

# Source files
file(GLOB_RECURSE SOURCE_FILES LIST_DIRECTORIES false src/*.cpp)

# Build as executable
add_executable(genericLiftKernel ${SOURCE_FILES})

# Require support for at least C++14
set_property(TARGET genericLiftKernel PROPERTY CXX_STANDARD 14)
set_property(TARGET genericLiftKernel PROPERTY CXX_STANDARD_REQUIRED ON)


## DEPENDENCIES

# ATF library
target_link_libraries(genericLiftKernel ${LIBATF_LIBRARIES})






