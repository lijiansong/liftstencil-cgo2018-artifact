// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

// Dominique Bönninghoff, Patrick Untiet, ..

/*
        TODO:
                (I) 	Problem mit der Größe des Resultstrings lösen: Mit
   der zurzeitigen Implementation kann man das nicht genau vorraussehen. Man
   kann nur die Längen aller normalen Intervalle aufaddieren. Hier vielleicht
   "genug" Extraplatz schaffen und dann die trotzdem auftretenden Allokation in
   Kauf nehmen? Oder: Umbauen von Stateless zu Stateful: rope kann in Labelnodes
   Wert speichern, dann in O(N) Länge errechnen, dann linearisieren, danach
   refresh

                (II)	rope::split nutzt [begin, end) Konvention. Besser
   wäre [begin, begin+len), so wie string_view. => ändern
*/

#ifndef rope_h
#define rope_h

#pragma once

#include "string_view.hpp"
#include <algorithm>
#include <atf/tp_value.hpp>
#include <atf/value_type.hpp>
#include <initializer_list>
#include <numeric>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#define USE_TOTAL_LENGTH

namespace atf {
namespace detail {
// node class for string rope
template <typename TChar, typename TTraits = ::std::char_traits<TChar>>
class rope_node {

public:
  // public typedefs -----
  using string_type = ::std::basic_string<TChar, TTraits>;
  using view_type = basic_string_view<TChar, TTraits>;
  using size_type = typename view_type::size_type;
  using config_type = ::atf::configuration;

private:
  // members -----
  view_type m_view;   // view, if this is a plain string node
  string_type m_name; // name, if this node is an insertion point

  // private typedefs -----
  using TThis = rope_node<TChar, TTraits>;

public:
  // constructors/destructors -----
  rope_node() = default;
  rope_node(const TThis &) = default;
  rope_node(TThis &&) = default;

  rope_node(const view_type &p_view) : m_view(p_view), m_name() {}
  rope_node(const string_type &p_name) : m_view(), m_name(p_name) {}
  rope_node(const view_type &p_view, const string_type &p_name)
      : m_view(p_view), m_name(p_name) {}

  ~rope_node() = default;

  // assignment -----
  TThis &operator=(const TThis &) = default;
  TThis &operator=(TThis &&) = default;

  // gets the size of the underlying string_view
  size_type view_size() const noexcept;

  // checks whether the underlying view is empty
  bool view_empty() const noexcept;

  /*
  splits the underlying view, removing the characters in [p_beg, p_end), i.e.
  truncates this view to [original_begin, p_beg)
  and returns a new view consisting of [p_end, original_end).

  throws ::std::out_of_range if p_end > view_size().

  Behaviour is undefined if p_end < p_beg.
  */
  view_type split(size_type p_beg, size_type p_end);

  // sets this node's name.
  void set_name(const string_type &p_name) noexcept;

  // sets the underlying view
  void set_view(const view_type &p_name) noexcept;

  // gets this node's name
  const string_type &get_name() const noexcept;

  // gets this node's view
  const view_type &get_view() const noexcept; // TODO remove this? might result
                                              // in dangling view pointer if used
                                              // incorrectly

  // looks for this node's name in given config and replaces with its assigned
  // value if present.  otherwise, converts underlying view to string_type and
  // returns it.
  string_type get_str(const config_type & = config_type{}) const;
}; // class rope_node -----

// begin rope_node impl
// -----------------------------------------------------------------------------
template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::view_size() const noexcept -> size_type {
  return m_view.size();
}

template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::view_empty() const noexcept -> bool {
  return m_view.empty();
}

template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::split(size_type p_beg, size_type p_end)
    -> view_type {
  view_type second{};
  if (p_end != m_view.size()) {
    second = m_view.substr(p_end);
  }
  m_view.remove_suffix(m_view.size() - p_beg);
  return second;
}

template <typename TChar, typename TTraits>
void rope_node<TChar, TTraits>::set_name(const string_type &p_name) noexcept {
  m_name = p_name;
}

template <typename TChar, typename TTraits>
void rope_node<TChar, TTraits>::set_view(const view_type &p_view) noexcept {
  m_view = p_view;
}

template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::get_name() const noexcept
    -> const string_type & {
  return m_name;
}

template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::get_view() const noexcept -> const view_type & {
  return m_view;
}

template <typename TChar, typename TTraits>
auto rope_node<TChar, TTraits>::get_str(const config_type &p_config) const
    -> string_type {
  // If it is an interval node, return that.
  if (get_name().empty())
    return get_view().to_string();

  using value_t = ::atf::value_type;
  auto it = p_config.find(m_name);
  if (it != p_config.end()) {
    return static_cast<std::string>(it->second.value());
  } else
    throw ::std::runtime_error(
        "Given program uses undefined tuning parameters!");
}
  // end rope_node impl
  // ------------------------------------------------------------------------------------
#ifdef USE_TOTAL_LENGTH
const auto size_sum = [](auto fst, auto snd) { return fst + snd.view_size(); };
#endif
// class to hold a rope of string nodes for simple insertion and deletion
template <typename TChar, typename TTraits = ::std::char_traits<TChar>>
class basic_rope {
public:
  // public typedefs -----
  using view_type = basic_string_view<TChar, TTraits>;
  using string_type = ::std::basic_string<TChar, TTraits>;
  using node_type = rope_node<TChar, TTraits>;
  using container_type = ::std::vector<node_type>;
  using size_type = typename container_type::size_type;
  using view_size_type = typename view_type::size_type;
  using value_type = node_type;
  using reference = typename container_type::reference;
  using const_reference = typename container_type::const_reference;
  using iterator = typename container_type::iterator;
  using const_iterator = typename container_type::const_iterator;
  using reverse_iterator = typename container_type::reverse_iterator;
  using const_reverse_iterator =
      typename container_type::const_reverse_iterator;
  using config_type = ::atf::configuration;

private:
  // private typedefs -----
  using TThis = basic_rope<TChar, TTraits>;
  // members -----
  container_type m_cont;
#ifdef USE_TOTAL_LENGTH
  view_size_type m_total_length; // TODO benchmark whether this is worth it
#endif
public:
  // constructors/destructor/assignment -----
  basic_rope() = default;
  basic_rope(const TThis &) = default;
  basic_rope(TThis &&) = default;
  TThis &operator=(const TThis &) = default;
  TThis &operator=(TThis &&) = default;
  ~basic_rope() = default;
  // constructor taking a base view
  basic_rope(const view_type &p_view)
      : m_cont(1, p_view)
#ifdef USE_TOTAL_LENGTH
        ,
        m_total_length(p_view.size())
#endif
  {
  }

  // container interface -----
  void set_container(const container_type &);
  void assign(const size_type, const node_type &);
  template <typename It> void assign(It, It);
  void assign(::std::initializer_list<node_type>);
  // element access -----
  reference at(const size_type);
  const_reference at(const size_type) const;
  reference operator[](const size_type);
  const_reference operator[](const size_type) const;
  reference front();
  const_reference front() const;
  reference back();
  const_reference back() const;
  // iterators -----
  iterator begin();
  const_iterator begin() const;
  const_iterator cbegin() const;
  iterator end();
  const_iterator end() const;
  const_iterator cend() const;
  reverse_iterator rbegin();
  const_reverse_iterator rbegin() const;
  const_reverse_iterator crbegin() const;
  reverse_iterator rend();
  const_reverse_iterator rend() const;
  const_reverse_iterator crend() const;
  // capacity -----
  bool empty() const noexcept;
  size_type size() const noexcept;
  size_type max_size() const noexcept;
  void reserve(const size_type);
  size_type capacity() const noexcept;
  void shrink_to_fit();
  // modifiers -----
  void clear() noexcept;
  iterator insert(const_iterator, const node_type &);
  iterator insert(const_iterator, node_type &&);
  iterator insert(const_iterator, size_type, const node_type &);
  template <typename It> iterator insert(const_iterator, It, It);
  iterator insert(const_iterator, ::std::initializer_list<node_type>);
  template <typename... Args> iterator emplace(const_iterator, Args &&...);
  iterator erase(const_iterator);
  iterator erase(const_iterator, const_iterator);
  void push_back(const node_type &);
  void push_back(node_type &&);
  template <typename... Args> void emplace_back(Args &&...);
  void pop_back();
  void resize(size_type);
  void resize(size_type, const node_type &);
  void swap(TThis &);
  // end container interface -----
  // rope operations
  /*
  split the view pointed to by p_pos, removing [p_beg, p_end) from the view,
  resulting in *p_pos referring to [orig_begin, p_beg) and a new view (inserted
  after p_pos) referring to [p_end, orig_end).

  See rope_node::split for details.
  */
  iterator split(iterator p_pos, view_size_type p_beg, view_size_type p_end);
  /*
  Splits the view pointed to by p_pos and inserts an insertion point with label
  p_name between the two subviews.

  See rope_node::split && rope::split for details.
  */
  // iterator split(iterator p_pos, view_size_type p_beg, view_size_type p_end,
  // const string_type& p_name);

  // overload accecpting name as view instead of string
  iterator split(iterator p_pos, view_size_type p_beg, view_size_type p_end,
                 const view_type &p_name);

  /*
  Splits the view pointed to by p_pos with an insertion point for each of the
  3-tuples in p_split_points.

  Is semantically equivalent to calling split repeatedly for each of the tuples
  in p_split_points.

  If t1 is located before t2 in p_split_points, then the "end" index of t1 shall
  be less than the "begin" value of t2 (i.e. the tuples should be ordered and no
  split shall discard characters required for following splits).
  */
  iterator
  split(iterator p_pos,
        const std::vector<std::tuple<view_size_type, view_size_type, view_type>>
            &p_split_points);

  /*
  builds a string by appending all of the nodes' contents in sequence. All named
  nodes look for a name-value-pair (with its name being equal to theirs) in
  p_config and return a string version of the associated value. If no fitting
  name is found, the view's contents are returned instead.
  */
  string_type build_string(const config_type &p_config = config_type{}) const;

  // sets the view of all insertion points whose name is equal to p_name to
  // p_view.
  void set(const string_type &p_name, const view_type &p_view);
};

// begin rope impl
// -----------------------------------------------------------------------------------------------------------------------
// container interface -----
template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::set_container(const container_type &p_cont) {
  m_cont = p_cont;
#ifdef USE_TOTAL_LENGTH
  m_total_length = ::std::accumulate(m_cont.cbegin(), m_cont.cend(),
                                     static_cast<view_size_type>(0), size_sum);
#endif
}
template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::assign(const size_type p_num,
                                        const node_type &p_val) {
  m_cont.assign(p_num, p_val);
#ifdef USE_TOTAL_LENGTH
  m_total_length = ::std::accumulate(m_cont.cbegin(), m_cont.cend(),
                                     static_cast<view_size_type>(0), size_sum);
#endif
}

template <typename TChar, typename TTraits>
template <typename It>
void basic_rope<TChar, TTraits>::assign(It p_beg, It p_end) {
  m_cont.assign(p_beg, p_end);
#ifdef USE_TOTAL_LENGTH
  m_total_length = ::std::accumulate(m_cont.cbegin(), m_cont.cend(),
                                     static_cast<view_size_type>(0), size_sum);
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::assign(
    ::std::initializer_list<node_type> p_il) {
  m_cont.assign(p_il);
#ifdef USE_TOTAL_LENGTH
  m_total_length = ::std::accumulate(m_cont.cbegin(), m_cont.cend(),
                                     static_cast<view_size_type>(0), size_sum);
#endif
}

// element access -----
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::at(const size_type p_index) -> reference {
  return m_cont.at(p_index);
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::at(const size_type p_index) const
    -> const_reference {
  return m_cont.at(p_index);
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::operator[](const size_type p_index)
    -> reference {
  return m_cont[p_index];
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::operator[](const size_type p_index) const
    -> const_reference {
  return m_cont[p_index];
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::front() -> reference {
  return m_cont.front();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::front() const -> const_reference {
  return m_cont.front();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::back() -> reference {
  return m_cont.back();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::back() const -> const_reference {
  return m_cont.back();
}

// iterators -----
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::begin() -> iterator {
  return m_cont.begin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::begin() const -> const_iterator {
  return m_cont.begin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::cbegin() const -> const_iterator {
  return m_cont.cbegin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::end() -> iterator {
  return m_cont.end();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::end() const -> const_iterator {
  return m_cont.end();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::cend() const -> const_iterator {
  return m_cont.cend();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::rbegin() -> reverse_iterator {
  return m_cont.rbegin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::rbegin() const -> const_reverse_iterator {
  return m_cont.rbegin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::crbegin() const -> const_reverse_iterator {
  return m_cont.crbegin();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::rend() -> reverse_iterator {
  return m_cont.rend();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::rend() const -> const_reverse_iterator {
  return m_cont.rend();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::crend() const -> const_reverse_iterator {
  return m_cont.crend();
}

// capacity -----
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::empty() const noexcept -> bool {
  return m_cont.empty();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::size() const noexcept -> size_type {
  return m_cont.size();
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::max_size() const noexcept -> size_type {
  return m_cont.max_size();
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::reserve(const size_type p_size) {
  m_cont.reserve(p_size);
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::capacity() const noexcept -> size_type {
  return m_cont.capacity;
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::shrink_to_fit() {
  m_cont.shrink_to_fit();
}

// modifiers -----
template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::clear() noexcept {
  m_cont.clear;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::insert(const_iterator p_pos,
                                        const node_type &p_val) -> iterator {
  auto result = m_cont.insert(p_pos, p_val);
#ifdef USE_TOTAL_LENGTH
  m_total_length += result->size();
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::insert(const_iterator p_pos, node_type &&p_val)
    -> iterator {
  auto result = m_cont.insert(p_pos, ::std::move(p_val));
#ifdef USE_TOTAL_LENGTH
  m_total_length += result->size();
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::insert(const_iterator p_pos, size_type p_num,
                                        const node_type &p_val) -> iterator {
  auto result = m_cont.insert(p_pos, p_num, p_val);
#ifdef USE_TOTAL_LENGTH
  m_total_length += p_num * p_val.size();
#endif
  return result;
}

template <typename TChar, typename TTraits>
template <typename It>
auto basic_rope<TChar, TTraits>::insert(const_iterator p_pos, It p_beg,
                                        It p_end) -> iterator {
  auto result = m_cont.insert(p_pos, p_beg, p_end);
#ifdef USE_TOTAL_LENGTH
  m_total_length +=
      ::std::accumulate(p_beg, p_end, static_cast<view_size_type>(0), size_sum);
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::insert(const_iterator p_it,
                                        ::std::initializer_list<node_type> p_il)
    -> iterator {
  auto result = m_cont.insert(p_it, p_il);
#ifdef USE_TOTAL_LENGTH
  m_total_length += ::std::accumulate(p_il.begin, p_il.end(),
                                      static_cast<view_size_type>(0), size_sum);
#endif
  return result;
}

template <typename TChar, typename TTraits>
template <typename... Args>
auto basic_rope<TChar, TTraits>::emplace(const_iterator p_pos,
                                         Args &&... p_args) -> iterator {
  auto result = m_cont.emplace(p_pos, ::std::forward<Args>(p_args)...);
#ifdef USE_TOTAL_LENGTH
  m_total_length += result->size();
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::erase(const_iterator p_pos) -> iterator {
#ifdef USE_TOTAL_LENGTH
  auto sz = p_pos->size();
#endif
  auto result = m_cont.erase(p_pos);
#ifdef USE_TOTAL_LENGTH
  m_total_length -= sz;
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::erase(const_iterator p_beg,
                                       const_iterator p_end) -> iterator {
#ifdef USE_TOTAL_LENGTH
  auto sz =
      ::std::accumulate(p_beg, p_end, static_cast<view_size_type>(0), size_sum);
#endif
  auto result = m_cont.erase(p_beg, p_end);
#ifdef USE_TOTAL_LENGTH
  m_total_length -= sz;
#endif
  return result;
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::push_back(const node_type &p_val) {
  m_cont.push_back(p_val);
#ifdef USE_TOTAL_LENGTH
  m_total_length += p_val.size();
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::push_back(node_type &&p_val) {
#ifdef USE_TOTAL_LENGTH
  auto sz = p_val.size();
#endif
  m_cont.push_back(::std::move(p_val));
#ifdef USE_TOTAL_LENGTH
  m_total_length += sz;
#endif
}

template <typename TChar, typename TTraits>
template <typename... Args>
void basic_rope<TChar, TTraits>::emplace_back(Args &&... p_args) {
  m_cont.emplace_back(::std::forward<Args>(p_args)...);
#ifdef USE_TOTAL_LENGTH
  m_total_length += m_cont.back().size();
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::pop_back() {
#ifdef USE_TOTAL_LENGTH
  auto sz = m_cont.back().size();
#endif
  m_cont.pop_back();
#ifdef USE_TOTAL_LENGTH
  m_total_length -= sz;
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::resize(size_type p_size) {
  m_cont.resize(p_size);
#ifdef USE_TOTAL_LENGTH
  m_total_length = 0; // default string views are empty
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::resize(size_type p_size,
                                        const node_type &p_val) {
  m_cont.resize(p_size, p_val);
#ifdef USE_TOTAL_LENGTH
  m_total_length = p_size * p_val.size();
#endif
}

template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::swap(TThis &p_other) {
  auto t_cont = ::std::move(m_cont);
  m_cont = ::std::move(p_other.m_cont);
  p_other.m_cont = ::std::move(t_cont);
#ifdef USE_TOTAL_LENGTH
  auto t_total_length = m_total_length;
  m_total_length = p_other.m_total_length;
  p_other.m_total_length = t_total_length;
#endif
}

// rope operations -----
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::split(iterator p_pos, view_size_type p_beg,
                                       view_size_type p_end) -> iterator {
  auto new_view = p_pos->split(p_beg, p_end); // split views...
  ++p_pos;                                    // insert second half after first
  auto result = m_cont.insert(p_pos, node_type(new_view));
#ifdef USE_TOTAL_LENGTH
  m_total_length -= (p_end - p_beg);
#endif
  return result;
}

/*
template <	typename TChar,
                        typename TTraits>
auto basic_rope<TChar,TTraits>::split(iterator p_pos, view_size_type p_beg,
view_size_type p_end, const string_type& p_name)
        -> iterator
{
        return split(p_pos, p_beg, p_end, view_type(p_name));
}
*/
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::split(iterator p_pos, view_size_type p_beg,
                                       view_size_type p_end,
                                       const view_type &p_name) -> iterator {
  auto new_view = p_pos->split(p_beg, p_end); // split views...
  ++p_pos;                                    // insert second half after first
  auto result = m_cont.insert(p_pos, node_type(new_view));
  result = m_cont.insert(result, node_type(static_cast<string_type>(
                                     p_name))); // add insertion point
  ++result; // result points to insertion point -> move to second half of view
#ifdef USE_TOTAL_LENGTH
  m_total_length -= (p_end - p_beg);
#endif
  return result;
}

template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::split(
    iterator p_pos,
    const std::vector<std::tuple<view_size_type, view_size_type, view_type>>
        &p_split_points) -> iterator {
#ifdef USE_TOTAL_LENGTH
  auto &l_total_length = m_total_length;
#endif
  view_size_type index = 0; // count the amount of caharacters that have been
                            // discarded by splits to adjust bounds later
  auto split_lambda = [this, &index, &p_pos
#ifdef USE_TOTAL_LENGTH
                       ,
                       &l_total_length
#endif
  ](const auto &split_params) {
    auto beg = ::std::get<0>(split_params) - index;
    auto end = ::std::get<1>(split_params) - index;
    const auto &view = ::std::get<2>(split_params);
    index += end; // adjust count of passed characters (final interval starts at
                  // end, all characters before that are done)
#ifdef USE_TOTAL_LENGTH
    l_total_length -= (end - beg);
#endif
    if (!view.empty())
      p_pos = this->split(p_pos, beg, end, view);
    else
      p_pos = this->split(p_pos, beg, end);
  }; //split lambda
  ::std::for_each(p_split_points.begin(), p_split_points.end(), split_lambda);
  return p_pos;
}

/*
builds a string by appending all of the nodes' contents in sequence. All named
nodes look for a name-value-pair (with its name being equal to theirs) in
p_config and return a string version of the associated value. If no fitting name
is found, the view's contents are returned instead.
*/
template <typename TChar, typename TTraits>
auto basic_rope<TChar, TTraits>::build_string(const config_type &p_config) const
    -> string_type {
  string_type result;
#ifdef USE_TOTAL_LENGTH
  // result.reserve(m_total_length); //minimize memory reallocation  //TODO fix
  // underflow
#endif
  auto append_lambda = [&result, &p_config](const auto &p_node) {
    result += p_node.get_str(p_config);
  }; //TODO use string::append instead of operator+= ?
  ::std::for_each(m_cont.cbegin(), m_cont.cend(), append_lambda);

  return result;
}

// sets the view of all insertion points whose name is equal to p_name to
// p_view.
template <typename TChar, typename TTraits>
void basic_rope<TChar, TTraits>::set(const string_type &p_name,
                                     const view_type &p_view) {
#ifdef USE_TOTAL_LENGTH
  auto &l_total_length = m_total_length;
  auto replace_lambda = [&p_name, &p_view, &l_total_length](auto &p_node) {
    if (p_node.get_name() == p_name) {
      auto old_sz = p_node.view_size();
      p_node.set_view(p_view);
      if (old_sz > p_view.size()) {
        l_total_length -= (old_sz - p_view.size());
      } else {
        l_total_length += (p_view.size() - old_sz);
      }
    }
  }; //replace_lambda
#else
  auto replace_lambda = [&p_name, &p_view](auto &p_node) {
    if (p_node.get_name() == p_name) {
      p_node.set_view(p_view);
    }
  }; //replace_lambda
#endif
  ::std::for_each(m_cont.begin(), m_cont.end(), replace_lambda);
} // rope::set
// end rope impl
// -----------------------------------------------------------------------------------------------------------------------
// TODO add comparison operators for rope

using rope = basic_rope<char>;
} // namespace detail
} // namespace atf

namespace std {
template <typename TChar, typename TTraits>
void swap(::atf::detail::basic_rope<TChar, TTraits> p_lhs,
          ::atf::detail::basic_rope<TChar, TTraits> p_rhs) {
  p_lhs.swap(p_rhs);
}
} // namespace std

#endif // header guard
