###################################################################
# libatf test template
#
# This template automatically registers a test. It is included
# by all test subprojects.
###################################################################

# Set minimum required CMAKE version
cmake_minimum_required(VERSION 3.1)

# Deduce test identifier from directory name
get_filename_component(TEST_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
set(TEST_IDENT "${TEST_NAME}_test")

# Register test project
project(${TEST_IDENT})

# Register executable and link to libatf libraries
add_executable(${TEST_IDENT} test.cpp)
target_link_libraries(${TEST_IDENT} catch ${LIBATF_LIBRARIES})

# Require support for at least C++14.
set_property(TARGET ${TEST_IDENT} PROPERTY CXX_STANDARD 14)
set_property(TARGET ${TEST_IDENT} PROPERTY CXX_STANDARD_REQUIRED ON)

# Add test to test suite
add_test(NAME ${TEST_NAME} COMMAND ${TEST_IDENT})
