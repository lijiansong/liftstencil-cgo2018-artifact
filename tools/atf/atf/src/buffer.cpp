// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <buffer.hpp>

namespace atf {
namespace internal {
auto entropy_source() -> ::std::random_device & {
  static ::std::random_device g_rd{};

  return g_rd;
}

// Seed using entropy source
sequence_generator_base::sequence_generator_base()
    : m_Engine{entropy_source()()} {}

auto sequence_generator_base::engine() -> ::std::default_random_engine & {
  return m_Engine;
}
} // namespace internal

auto from_file(const ::std::string &p_path) -> internal::from_file_t {
  return {p_path};
}

auto random_data(::std::size_t p_size) -> internal::random_data_t {
  return {p_size};
}
} // namespace atf
