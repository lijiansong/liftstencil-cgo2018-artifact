#!/bin/bash
pushd `dirname $0` > /dev/null
rm -rf ./build && \
mkdir build && \
cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DLIBATF_BUILD_EXAMPLES=On -DLIBATF_DISABLE_WARNINGS=On ./.. && \
make && \
popd > /dev/null
