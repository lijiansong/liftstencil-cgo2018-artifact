#if 0

//R"(
//#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_3 ( GM_SIZE_3 / (NUM_WG_3 * LM_SIZE_3) )
//#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_2 ( GM_SIZE_2 / (NUM_WG_2 * LM_SIZE_2) )
//#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1 ( GM_SIZE_1 / (NUM_WG_1 * LM_SIZE_1) )
//
//#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 ( LM_SIZE_3 / (NUM_WI_3 * PM_SIZE_3) )
//#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_2 ( LM_SIZE_2 / (NUM_WI_2 * PM_SIZE_2) )
//#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_1 ( LM_SIZE_1 / (NUM_WI_1 * PM_SIZE_1) )
//
//
//// given by view
//#define LM_SIZE_input_1_dim_1 LM_SIZE_3
//#define LM_SIZE_input_1_dim_2 LM_SIZE_1
//
//#define LM_SIZE_input_2_dim_1 LM_SIZE_1
//#define LM_SIZE_input_2_dim_2 LM_SIZE_2
//
//
//#define PM_SIZE_input_1_dim_1 PM_SIZE_3
//#define PM_SIZE_input_1_dim_2 PM_SIZE_1
//
//#define PM_SIZE_input_2_dim_1 PM_SIZE_1
//#define PM_SIZE_input_2_dim_2 PM_SIZE_2
//
//
//
//// helper
//#define LM_BLOCK_SIZE_3 (LM_SIZE_3 * NUM_WI_GLB_3)
//#define LM_BLOCK_SIZE_2 (LM_SIZE_2 * NUM_WI_GLB_2)
//#define LM_BLOCK_SIZE_1 (LM_SIZE_1 * NUM_WI_GLB_1)
//
//#define PM_BLOCK_SIZE_3 (PM_SIZE_3 * NUM_WI_LCL_3)
//#define PM_BLOCK_SIZE_2 (PM_SIZE_2 * NUM_WI_LCL_2)
//#define PM_BLOCK_SIZE_1 (PM_SIZE_1 * NUM_WI_LCL_1)
//
//
//#define NUM_LM_ITERATIONS_3 (LM_SIZE_3 / NUM_WI_LCL_3)
//#define NUM_LM_ITERATIONS_2 (LM_SIZE_2 / NUM_WI_LCL_2)
//#define NUM_LM_ITERATIONS_1 (LM_SIZE_1 / NUM_WI_LCL_1)
//
//#define NUM_PM_ITERATIONS_3 PM_SIZE_3
//#define NUM_PM_ITERATIONS_2 PM_SIZE_2
//#define NUM_PM_ITERATIONS_1 PM_SIZE_1
//
////TODO LÖSCHEN
////#define NUM_PM_ITERATIONS_3 (PM_SIZE_3 / NUM_WI_LCL_3)
////#define NUM_PM_ITERATIONS_2 (PM_SIZE_2 / NUM_WI_LCL_2)
////#define NUM_PM_ITERATIONS_1 (PM_SIZE_1 / NUM_WI_LCL_1)
////
////#define NUM_LM_ITERATIONS_3 LM_SIZE_3
////#define NUM_LM_ITERATIONS_2 LM_SIZE_2
////#define NUM_LM_ITERATIONS_1 LM_SIZE_1
//
//
//
//
//
//
//
//
//#define NUM_WI_GLB_3 (NUM_WG_3 * NUM_WI_LCL_3)
//#define NUM_WI_GLB_2 (NUM_WG_2 * NUM_WI_LCL_2)
//#define NUM_WI_GLB_1 (NUM_WG_1 * NUM_WI_LCL_1)
//
//#define NUM_WI_LCL_3 NUM_WI_3
//#define NUM_WI_LCL_2 NUM_WI_2
//#define NUM_WI_LCL_1 NUM_WI_1
//
//#define NUM_WI_PRV_3 1
//#define NUM_WI_PRV_2 1
//#define NUM_WI_PRV_1 1
//
//
//
////inline void update_LM_cache( __global float[ GM_SIZE_3          ][ GM_SIZE_1          ] glb_input_1,       __global  float[ GM_SIZE_1          ][ GM_SIZE_2          ] glb_input_2,
////                             __local  float[ LM_SIZE_input_1_dim_1 ][ LM_SIZE_input_1_dim_2 ] lcl_cache_input_1, __local   float[ LM_SIZE_input_2_dim_1 ][ LM_SIZE_input_1_dim_2 ] lcl_cache_input_2,
////                             const int WI_ID_GLB_3, const int WI_ID_GLB_2, const int WI_ID_GLB_1,
////                             const int id_lm_block_3,     const int id_lm_block_2,     const int id_lm_block_1
////                           )
//inline void update_LM_cache( __global const float* restrict glb_input_1,       __global const float* restrict glb_input_2,
//                             __local        float* restrict lcl_cache_input_1, __local        float* restrict lcl_cache_input_2,
//                             const int WI_ID_LCL_3, const int WI_ID_LCL_2, const int WI_ID_LCL_1,
//                             const int WI_ID_GLB_3, const int WI_ID_GLB_2, const int WI_ID_GLB_1,
//                             const int id_lm_block_3,     const int id_lm_block_2,     const int id_lm_block_1
//                           )
//{
//barrier(CLK_LOCAL_MEM_FENCE );
//#if 0
//  // offsets to LM block
//  const int OFFSET_3 = id_lm_block_3 * LM_BLOCK_SIZE_3;
//  const int OFFSET_2 = id_lm_block_2 * LM_BLOCK_SIZE_2;
//  const int OFFSET_1 = id_lm_block_1 * LM_BLOCK_SIZE_1;
//  
//  
//  // cache first input
//  for( int i_3 = 0 ; i_3 < NUM_LM_ITERATIONS_3 ; ++i_3 )
//    for( int i_1 = 0 ; i_1 < NUM_LM_ITERATIONS_1 ; ++i_1 )
//    {
//      const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
//
//      const int glb_index_3 = WI_ID_GLB_3 + i_3 * NUM_WI_GLB_3;
//      const int glb_index_1 = WI_ID_GLB_1 + i_1 * NUM_WI_GLB_1;
//      
//      lcl_cache_input_1[ lcl_index_3 * LM_SIZE_input_1_dim_2 + lcl_index_1 ] =
//        glb_input_1[ (OFFSET_3 + glb_index_3) * GM_SIZE_1 + (OFFSET_1 + glb_index_1) ];
//    }
//
//  // cache second input
//  for( int i_1 = 0 ; i_1 < NUM_LM_ITERATIONS_1 ; ++i_1 )
//    for( int i_2 = 0 ; i_2 < NUM_LM_ITERATIONS_2 ; ++i_2 )
//    {
//      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
//      const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//
//      const int glb_index_1 = WI_ID_GLB_1 + i_1 * NUM_WI_GLB_1;
//      const int glb_index_2 = WI_ID_GLB_2 + i_2 * NUM_WI_GLB_2;
//      
//      lcl_cache_input_2[ lcl_index_1 * LM_SIZE_input_2_dim_2 + lcl_index_2 ] =
//        glb_input_2[ (OFFSET_1 + glb_index_1) * GM_SIZE_2 + (OFFSET_2 + glb_index_2) ];
//        printf("i_1 = %i, i_2 = %i, glb_input_2[ %i ] = %f\n", i_1, i_2, (OFFSET_1 + glb_index_1) * GM_SIZE_2 + (OFFSET_2 + glb_index_2), glb_input_2[ (OFFSET_1 + glb_index_1) * GM_SIZE_2 + (OFFSET_2 + glb_index_2) ] );
//    }
//
//barrier(CLK_LOCAL_MEM_FENCE );
//  
//// LÖSCHEN
//
//////printf("LM_SIZE_3 * LM_SIZE_1 = %i\n", LM_SIZE_3 * LM_SIZE_1);
////  for( int i = 0 ; i < LM_SIZE_3 * LM_SIZE_1 ; ++i )
////  {
////    printf("lcl_cache_input_1[ i ] = %f\n", lcl_cache_input_1[ i ] );
////    lcl_cache_input_1[ i ] = 1;
////  }
////printf("\n\n");
////
//////printf("LM_SIZE_1 * LM_SIZE_2 = %i\n", LM_SIZE_1 * LM_SIZE_2);
////  for( int i = 0 ; i < LM_SIZE_1 * LM_SIZE_2 ; ++i )
////  {
////    printf("lcl_cache_input_2[ i ] = %f\n", lcl_cache_input_2[ i ] );
////    lcl_cache_input_2[ i ] = 1;
////  }
//
//#else
////printf("LM_SIZE_3 * LM_SIZE_1 = %i\n", LM_SIZE_3 * LM_SIZE_1);
//  for( int i = 0 ; i < LM_SIZE_3 * LM_SIZE_1 ; ++i )
//    lcl_cache_input_1[ i ] = 1;
//
////printf("LM_SIZE_1 * LM_SIZE_2 = %i\n", LM_SIZE_1 * LM_SIZE_2);
//  for( int i = 0 ; i < LM_SIZE_1 * LM_SIZE_2 ; ++i )
//    lcl_cache_input_2[ i ] = 1;
//#endif
//
//  barrier( CLK_LOCAL_MEM_FENCE );
//}
//
//
////inline void update_PM_cache( __local   const float[ LM_SIZE_input_1_dim_1 ][ LM_SIZE_input_1_dim_2 ] restrict lcl_cache_input_1, __local   const float[ LM_SIZE_input_2_dim_1 ][ LM_SIZE_input_1_dim_2 ] restrict lcl_cache_input_2,
////                             __private       float[ PM_SIZE_input_1_dim_1 ][ PM_SIZE_input_1_dim_2 ] restrict prv_cache_input_1, __private       float[ PM_SIZE_input_2_dim_1 ][ PM_SIZE_input_1_dim_2 ] restrict prv_cache_input_2,
////                             const int WI_ID_LCL_3, const int WI_ID_LCL_2, const int WI_ID_LCL_1,
////                             const int id_pm_block_3,     const int id_pm_block_2,     const int id_pm_block_1
////                           )
//inline void update_PM_cache( __local   const float* restrict lcl_cache_input_1, __local   const float* restrict lcl_cache_input_2,
//                             __private       float* restrict prv_cache_input_1, __private       float* restrict prv_cache_input_2,
//                             const int WI_ID_PRV_3, const int WI_ID_PRV_2, const int WI_ID_PRV_1,
//                             const int WI_ID_LCL_3, const int WI_ID_LCL_2, const int WI_ID_LCL_1,
//                             const int id_pm_block_3,     const int id_pm_block_2,     const int id_pm_block_1
//                           )
//
//{
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//
//
////for( int i = 0 ; i < LM_SIZE_input_1_dim_1 ; ++i )
////  for( int j = 0 ; j < LM_SIZE_input_1_dim_2 ; ++j )
////    printf("lcl_cache_input_1 = %f\n", lcl_cache_input_1[ i * LM_SIZE_input_1_dim_2 + j ] );
////
////for( int i = 0 ; i < LM_SIZE_input_2_dim_1 ; ++i )
////  for( int j = 0 ; j < LM_SIZE_input_2_dim_2 ; ++j )
////    printf("lcl_cache_input_2 = %f\n", lcl_cache_input_2[ i * LM_SIZE_input_2_dim_2 + j ] );
//
//
//  // offsets to LM block
//  const int OFFSET_3 = id_pm_block_3 * PM_BLOCK_SIZE_3;
//  const int OFFSET_2 = id_pm_block_2 * PM_BLOCK_SIZE_2;
//  const int OFFSET_1 = id_pm_block_1 * PM_BLOCK_SIZE_1;
//  
//  
//        //printf("NUM_PM_ITERATIONS_3 = %i, NUM_PM_ITERATIONS_1 = %i\n", NUM_PM_ITERATIONS_3, NUM_PM_ITERATIONS_1 );
//  // cache first input
//  for( int i_3 = 0 ; i_3 < NUM_PM_ITERATIONS_3 ; ++i_3 )
//    for( int i_1 = 0 ; i_1 < NUM_PM_ITERATIONS_1 ; ++i_1 )
//    {
//      const int prv_index_3 = WI_ID_PRV_3 + i_3 * NUM_WI_PRV_3;
//      const int prv_index_1 = WI_ID_PRV_1 + i_1 * NUM_WI_PRV_1;
//    
//      const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
//      
//      prv_cache_input_1[ prv_index_3 * PM_SIZE_input_1_dim_2 + prv_index_1 ] =
//        lcl_cache_input_1[ (OFFSET_3 + lcl_index_3) * LM_SIZE_input_1_dim_2 + (OFFSET_1 + lcl_index_1) ];
//      
////printf("index_3 = %i, index_1 = %i\n", index_3, index_1 );
////        printf("lcl_cache_input_1[ %i ][ %i ] = %f\n", (OFFSET_3 + index_3), (OFFSET_1 + index_1), lcl_cache_input_1[ (OFFSET_3 + index_3) * LM_SIZE_input_1_dim_2 + (OFFSET_1 + index_1) ] );
//    }
//
//  // cache second input
//  for( int i_1 = 0 ; i_1 < NUM_PM_ITERATIONS_1 ; ++i_1 )
//    for( int i_2 = 0 ; i_2 < NUM_PM_ITERATIONS_2 ; ++i_2 )
//    {
//      const int prv_index_1 = WI_ID_PRV_1 + i_1 * NUM_WI_PRV_1;
//      const int prv_index_2 = WI_ID_PRV_2 + i_2 * NUM_WI_PRV_2;
//    
//      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
//      const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//      
//      prv_cache_input_2[ prv_index_1 * PM_SIZE_input_2_dim_2 + prv_index_2 ] =
//        lcl_cache_input_2[ (OFFSET_1 + lcl_index_1) * LM_SIZE_input_2_dim_2 + (OFFSET_2 + lcl_index_2) ];
//      
////        printf("lcl_cache_input_2 = %f\n", lcl_cache_input_2[ (OFFSET_1 + index_1) * LM_SIZE_input_2_dim_2 + (OFFSET_2 + index_2) ] );
//    }
//  
//  
//// LÖSCHEN
////  for( int i = 0 ; i < PM_SIZE_3 * PM_SIZE_1 ; ++i )
//   // printf(" unten -> index = %i\n", i );
////    printf("prv_cache_input_1[ %i ] = %f, WI_ID = %i \n", i, prv_cache_input_1[ i ], get_global_id(2) );
//
////  for( int i = 0 ; i < PM_SIZE_1 * PM_SIZE_2 ; ++i )
////    printf("prv_cache_input_2[ %i ] = %f, WI_ID = %i \n", i, prv_cache_input_2[ i ], get_global_id(2) );
//
//
//
//#if 0
//  for( int i = 0 ; i < PM_SIZE_3 * PM_SIZE_1 ; ++i )
//    prv_cache_input_1[ i ] = 1;
//
//  for( int i = 0 ; i < PM_SIZE_1 * PM_SIZE_2 ; ++i )
//    prv_cache_input_2[ i ] = 1;
//#endif
//}
//
//
//// view
//struct pair
//{
//  float const* lhs;
//  float const* rhs;
//};
//
//
//inline const struct pair view( __private const float* restrict in_1,
//                               __private const float* restrict in_2,
//                               const int i_3,
//                               const int i_2,
//                               const int i_1
//                             )
//{
//  struct pair p;
//  
//  p.lhs = &in_1[ i_3 * PM_SIZE_input_1_dim_2 + i_1 ];
//  p.rhs = &in_2[ i_1 * PM_SIZE_input_2_dim_2 + i_2 ];
//
////  printf("in_1[%i] = %f, i_3 = %i, PM_SIZE_input_1_dim_2 = %i, i_1 = %i\n", i_3 * PM_SIZE_input_1_dim_2 + i_1, in_1[ i_3 * PM_SIZE_input_1_dim_2 + i_1 ], i_3, PM_SIZE_input_1_dim_2, i_1 );
////  printf("lhs = %f, rhs=%f, res = %f\n", *(p.lhs), *(p.rhs), *(p.lhs) * *(p.rhs) );
//  
//  return p;
//}
//
//// user function (TODO pre-user function: add, ... , stencils, etc. )
//inline float f( const struct pair p )
//{
//
//// printf("  lhs = %f, rhs=%f, res = %f\n", *(p.lhs), *(p.rhs), *(p.lhs) * *(p.rhs) );
//  
//  return *(p.lhs) * *(p.rhs);
//}
//
//
////__kernel void md_gemm( __global float[ GM_SIZE_3 ][ GM_SIZE_1 ]           restrict glb_input_1,
////                       __global float[ GM_SIZE_1 ][ GM_SIZE_2 ]           restrict glb_input_2,
////                       __global float[ GM_SIZE_3 ][ GM_SIZE_2 ][NUM_WG_1] restrict glb_output
////                     )
//__kernel void gemm_1( __global const float* restrict glb_input_1,
//                      __global const float* restrict glb_input_2,
//                      __global       float* restrict glb_output
//                    )
//{
//  // cache memory
//  __local float lcl_cache_input_1[ LM_SIZE_input_1_dim_1 ][ LM_SIZE_input_1_dim_2 ];
//  __local float lcl_cache_input_2[ LM_SIZE_input_2_dim_1 ][ LM_SIZE_input_2_dim_2 ];
//
////printf("LM_SIZE_input_1_dim_1 = %i, LM_SIZE_input_1_dim_2 = %i\n", LM_SIZE_input_1_dim_1, LM_SIZE_input_1_dim_2 );
////printf("LM_SIZE_input_2_dim_1 = %i, LM_SIZE_input_2_dim_2 = %i\n", LM_SIZE_input_2_dim_1, LM_SIZE_input_2_dim_2 );
//
//  __private float prv_cache_input_1[ PM_SIZE_input_1_dim_1 ][ PM_SIZE_input_1_dim_2 ];
//  __private float prv_cache_input_2[ PM_SIZE_input_2_dim_1 ][ PM_SIZE_input_2_dim_2 ];
//
//  
//  // memory for intermediate results
//  __private float tmp_res_prv[ PM_SIZE_3 ][ PM_SIZE_2 ]; // for schematical implementation -> [ 1 * PM_SIZE_3 ][ 1 * PM_SIZE_2 ][ 1 ]
//  __local   float tmp_res_lcl[ LM_SIZE_3 ][ LM_SIZE_2 ][ NUM_WI_LCL_1 ];
//  
//  
//  // WG/WI ids
//  const int WG_ID_3 = get_group_id( 2 );
//  const int WG_ID_2 = get_group_id( 1 );
//  const int WG_ID_1 = get_group_id( 0 );
//
//  const int WI_ID_GLB_3 = get_global_id( 2 );
//  const int WI_ID_GLB_2 = get_global_id( 1 );
//  const int WI_ID_GLB_1 = get_global_id( 0 );
//  
//  const int WI_ID_LCL_3 = get_local_id( 2 );
//  const int WI_ID_LCL_2 = get_local_id( 1 );
//  const int WI_ID_LCL_1 = get_local_id( 0 );
//
//  const int WI_ID_PRV_3 = 0;
//  const int WI_ID_PRV_2 = 0;
//  const int WI_ID_PRV_1 = 0;
//
//  
//  
//  // clean tmp_res_prv
//  for( int i_3 = 0 ; i_3 < PM_SIZE_3 / NUM_WI_PRV_3 ; ++i_3 )
//    for( int i_2 = 0 ; i_2 < PM_SIZE_2 / NUM_WI_PRV_2 ; ++i_2 )
//    {
//      const int prv_index_3 = i_3;
//      const int prv_index_2 = i_2;
//    
//      tmp_res_prv[ prv_index_3 ][ prv_index_2 ] = 0;
//    }
//
//  // clean tmp_res_lcl
//  for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
//    for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
//      {
//        const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//        const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//        
//        tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = 0;
//        
//        barrier(CLK_LOCAL_MEM_FENCE );
////printf("init -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", lcl_index_3, lcl_index_2, lcl_index_1, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ lcl_index_1 ] );
//      }
//  
//  
//  // iteration over local memory blocks
//  for( int id_lm_block_3 = 0 ; id_lm_block_3 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_3 ; ++id_lm_block_3 )
//    for( int id_lm_block_2 = 0 ; id_lm_block_2 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_2 ; ++id_lm_block_2 )
//    {
//      for( int id_lm_block_1 = 0 ; id_lm_block_1 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1 ; ++id_lm_block_1 )
//      {
////printf("\n  vorher -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", 0, 0, 0, tmp_res_lcl[0][0][0] );
//
////printf("\n  vorher -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f, WI_ID = %i \n", 0, 0, 0, tmp_res_lcl[0][0][0], get_global_id(1) );
//        update_LM_cache( glb_input_1, glb_input_2,
//                         lcl_cache_input_1, lcl_cache_input_2,
//                         WI_ID_LCL_3, WI_ID_LCL_2, WI_ID_LCL_1,
//                         WI_ID_GLB_3, WI_ID_GLB_2, WI_ID_GLB_1,
//                         id_lm_block_3, id_lm_block_2, id_lm_block_1
//                       );
//        
////printf("\n  nacher -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f, WI_ID = %i \n", 0, 0, 0, tmp_res_lcl[0][0][0], get_global_id(1) );
//        
//        // iteration over private memory blocks
//        for( int id_pm_block_3 = 0 ; id_pm_block_3 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 ; ++id_pm_block_3 )
//          for( int id_pm_block_2 = 0 ; id_pm_block_2 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_2 ; ++id_pm_block_2 )
//          {
//            for( int id_pm_block_1 = 0 ; id_pm_block_1 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_1 ; ++id_pm_block_1 )
//            {
////printf("    NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 = %i\n", NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 );
//              update_PM_cache( lcl_cache_input_1, lcl_cache_input_2,
//                               prv_cache_input_1, prv_cache_input_2,
//                               WI_ID_PRV_3, WI_ID_PRV_2, WI_ID_PRV_1,
//                               WI_ID_LCL_3, WI_ID_LCL_2, WI_ID_LCL_1,
//                               id_pm_block_3, id_pm_block_2, id_pm_block_1
//                             );
//              
//              // computation of a WI
//              for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
//                for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
//                  for( int i_1 = 0 ; i_1 < PM_SIZE_1 ; ++i_1 )
//                  {
//                    tmp_res_prv[ i_3 ][ i_2 ] += f( view( prv_cache_input_1, prv_cache_input_2, i_3, i_2, i_1 ) );
////if( get_global_id(1) == 0 )
////printf("res = %f, WI_ID = %i\n", tmp_res_prv[ i_3 ][ i_2 ], get_global_id(1) );
// }
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//            } // end for-loop "i_pm_1"
//            
//// printf("res = %f\n", *(p.lhs) * *(p.rhs));
//
////if( get_global_id(1) == 0 )
////for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
////  for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
////    printf("tmp_res_prv[ %i ][ %i ] = %f\n", i_3, i_3, tmp_res_prv[ i_3 ][ i_2 ] );
//
//        
//            // i) copy intermediate results from private to local memory, and ii) clean "tmp_res_prv"
//            for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
//              for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
//              {
////if( get_global_id(1) == 0 )
////printf("PM_SIZE_3=%i, PM_SIZE_2=%i\n", PM_SIZE_3, PM_SIZE_2 );
//                const int OFFSET_3 = id_pm_block_3 * PM_BLOCK_SIZE_3;
//                const int OFFSET_2 = id_pm_block_2 * PM_BLOCK_SIZE_2;
//                
//                const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//                const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//              
//                const int prv_index_3 = i_3;
//                const int prv_index_2 = i_2;
//
////printf("\n  -> WI_ID_2 = %i , tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", get_global_id(1), lcl_index_3, lcl_index_2, WI_ID_LCL_1, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] );
//
//                tmp_res_lcl[ OFFSET_3 + lcl_index_3 ][ OFFSET_2 + lcl_index_2 ][ WI_ID_LCL_1 ] += tmp_res_prv[ prv_index_3 ][ prv_index_2 ];
////printf("WI_ID_LCL_1 = %i, tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", get_global_id(2), lcl_index_3, lcl_index_2, WI_ID_LCL_1, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] );
////printf("WI_ID_LCL_2 = %i, tmp_res_prv[ %i ][ %i ] = %f\n", WI_ID_LCL_2, prv_index_3, prv_index_2, tmp_res_prv[ prv_index_3 ][ prv_index_2 ]);
//                tmp_res_prv[ prv_index_3 ][ prv_index_2 ] = 0;
//                
//
////barrier( CLK_LOCAL_MEM_FENCE );
////if( get_global_id(2) == 1 )
////printf("WI_ID = %i, tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", WI_ID_LCL_1, lcl_index_3, lcl_index_2, WI_ID_LCL_1, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] );
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//
//              }
////printf("ENDE\n");
//              barrier( CLK_LOCAL_MEM_FENCE );
//
////if( get_global_id(1) == 0 )
////{
////for( int lcl_index_3 = 0 ; lcl_index_3 < LM_SIZE_3 ; ++lcl_index_3 )
////  for( int lcl_index_2 = 0 ; lcl_index_2 < LM_SIZE_2 ; ++lcl_index_2 )
////printf("  unten tmp_res_lcl[%i][%i] = %f\n", lcl_index_3, lcl_index_2, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
////printf("  ENDE \n" );
////}
//            
//            
//          } // end for-loop "i_pm_2"
//        
//        
//        // parallel reduction of intermediate results
//        for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 ) //+= NUM_WI_LCL_3 )
//          for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 ) //+= NUM_WI_LCL_2 )
//            for( int stride = NUM_WI_LCL_1 / 2 ; stride > 0 ; stride /= 2 )
//            {
////printf("DRIN!, PM_SIZE_3 = %i, PM_SIZE_2 = %i\n", PM_SIZE_3, PM_SIZE_2 );
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//
//            const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//            const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//
////printf(" lcl_index_3 = %i, lcl_index_2 = %i \n", lcl_index_3, lcl_index_2 );
//
//              if( WI_ID_LCL_1 < stride )
//                tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] += tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 + stride ];
//              
////              printf("tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = %f\n", tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] );
//              
//              barrier( CLK_LOCAL_MEM_FENCE );
//              
//            //  printf("lhs = %f, rhs = %f\n", tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ], tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 + stride ] );
//              
//            }
//        
//        // clean tmp_res_lcl in dimensions k > 0
//        for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
//          for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
//            if( WI_ID_LCL_1 > 0 )
//            {
//              const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//              const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//
//  
//              tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = 0;
////printf("tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", lcl_index_3, lcl_index_2, WI_ID_LCL_1, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//            }
//
////if( get_global_id(1) == 0 )
////  for( int lcl_index_3 = 0 ; lcl_index_3 < LM_SIZE_3 ; ++lcl_index_3 )
////    for( int lcl_index_2 = 0 ; lcl_index_2 < LM_SIZE_2 ; ++lcl_index_2 )
////  printf(" tmp_res_lcl[%i][%i] = %f\n", lcl_index_3, lcl_index_2, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//      } // end for-loop "id_lm_block_1"
//
//        
//        
//        // i) copy intermediate results from local memory to global memory, and ii) clean "tmp_res_lcl"
//        if( WI_ID_LCL_1 == 0 )
//        for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
//          for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
//          {
//            const int OFFSET_3 = id_lm_block_3 * LM_BLOCK_SIZE_3;
//            const int OFFSET_2 = id_lm_block_2 * LM_BLOCK_SIZE_2;
//
//            const int glb_index_3 = WI_ID_GLB_3 + i_3 * NUM_WI_GLB_3;
//            const int glb_index_2 = WI_ID_GLB_2 + i_2 * NUM_WI_GLB_2;
//
//            const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
//            const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
//          
//            glb_output[ (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
//                        (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
//                        WG_ID_1                                             // dim 3
//                      ] += tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ];
////printf("WI_ID = %i, glb_output[ %i ][ %i ][ %i ] = %f, i_2=%i, i_3=%i, id_lm_block_1=%i, id_lm_block_2=%i, id_lm_block_3=%i, NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1= %i\n", get_global_id(0), (OFFSET_3 + glb_index_3), (OFFSET_2 + glb_index_2), WG_ID_1,
////                            glb_output[ (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
////                                        (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
////                                        WG_ID_1                                             // dim 3
////                                      ],
////                                      i_2, i_3, id_lm_block_1, id_lm_block_2, id_lm_block_3, NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1
////                                      );
////glb_output[0]=99;
////printf("tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", lcl_index_3, lcl_index_2, 0, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//
//            barrier( CLK_GLOBAL_MEM_FENCE );
//                                
//  //for( int lcl_index_3 = 0 ; lcl_index_3 < 4 ; ++lcl_index_3 )
//  //  for( int lcl_index_2 = 0 ; lcl_index_2 < 4 ; ++lcl_index_2 )
//  //printf(" tmp_res_lcl[%i][%i] = %f\n", lcl_index_3, lcl_index_2, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//
//  //printf("tmp_res_lcl[%i][%i] = %f\n", lcl_index_3, lcl_index_2, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//            
//            // clean "tmp_res_lcl"
////printf("del -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", lcl_index_3, lcl_index_2, 0, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );
//            tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = 0;
////printf("del -> tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", lcl_index_3, lcl_index_2, 0, tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ] );            
//            barrier( CLK_LOCAL_MEM_FENCE );
//
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//          } // end for-loop "i_2"
//barrier(CLK_LOCAL_MEM_FENCE );
//barrier(CLK_GLOBAL_MEM_FENCE );
//    } // end for-loop "i_lm_2"
//
//}
//
//
//__kernel void gemm_2( __global float* output_1,
//                      __global float* output_2
//                    )
//{
//}
//
//)";

#else


R"(
#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_3 ( GM_SIZE_3 / (NUM_WG_3 * LM_SIZE_3) )
#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_2 ( GM_SIZE_2 / (NUM_WG_2 * LM_SIZE_2) )
#define NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1 ( GM_SIZE_1 / (NUM_WG_1 * LM_SIZE_1) )

#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 ( LM_SIZE_3 / (NUM_WI_3 * PM_SIZE_3) )
#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_2 ( LM_SIZE_2 / (NUM_WI_2 * PM_SIZE_2) )
#define NUM_WI_ITERATIONS_OVER_PM_BLOCKS_1 ( LM_SIZE_1 / (NUM_WI_1 * PM_SIZE_1) )


// given by view
#define LM_SIZE_input_1_dim_1 LM_SIZE_3
#define LM_SIZE_input_1_dim_2 LM_SIZE_1

#define LM_SIZE_input_2_dim_1 LM_SIZE_1
#define LM_SIZE_input_2_dim_2 LM_SIZE_2


#define PM_SIZE_input_1_dim_1 PM_SIZE_3
#define PM_SIZE_input_1_dim_2 PM_SIZE_1

#define PM_SIZE_input_2_dim_1 PM_SIZE_1
#define PM_SIZE_input_2_dim_2 PM_SIZE_2



// helper
#define LM_BLOCK_SIZE_3 (LM_SIZE_3 * NUM_WG_3)
#define LM_BLOCK_SIZE_2 (LM_SIZE_2 * NUM_WG_2)
#define LM_BLOCK_SIZE_1 (LM_SIZE_1 * NUM_WG_1)

#define PM_BLOCK_SIZE_3 (PM_SIZE_3 * NUM_WI_LCL_3)
#define PM_BLOCK_SIZE_2 (PM_SIZE_2 * NUM_WI_LCL_2)
#define PM_BLOCK_SIZE_1 (PM_SIZE_1 * NUM_WI_LCL_1)


#define NUM_WI_GLB_3 (NUM_WG_3 * NUM_WI_LCL_3)
#define NUM_WI_GLB_2 (NUM_WG_2 * NUM_WI_LCL_2)
#define NUM_WI_GLB_1 (NUM_WG_1 * NUM_WI_LCL_1)

#define NUM_WI_LCL_3 NUM_WI_3
#define NUM_WI_LCL_2 NUM_WI_2
#define NUM_WI_LCL_1 NUM_WI_1

#define NUM_WI_PRV_3 1
#define NUM_WI_PRV_2 1
#define NUM_WI_PRV_1 1



inline void update_LM_cache( __global const float* restrict glb_input_1,       __global const float* restrict glb_input_2,
                             __local        float* restrict lcl_cache_input_1, __local        float* restrict lcl_cache_input_2,
                             const int WI_ID_LCL_3, const int WI_ID_LCL_2, const int WI_ID_LCL_1,
                             const int WI_ID_GLB_3, const int WI_ID_GLB_2, const int WI_ID_GLB_1,
                             const int id_lm_block_3,     const int id_lm_block_2,     const int id_lm_block_1
                           )
{
#if 1
  barrier( CLK_LOCAL_MEM_FENCE );
  
  // offsets to LM block
  const int OFFSET_3 = id_lm_block_3 * LM_BLOCK_SIZE_3;
  const int OFFSET_2 = id_lm_block_2 * LM_BLOCK_SIZE_2;
  const int OFFSET_1 = id_lm_block_1 * LM_BLOCK_SIZE_1;
  
  
  // cache first input
  for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
    for( int i_1 = 0 ; i_1 < LM_SIZE_1 / NUM_WI_LCL_1 ; ++i_1 )
    {
      const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;

      const int glb_index_3 = WI_ID_GLB_3 + i_3 * NUM_WI_GLB_3;
      const int glb_index_1 = WI_ID_GLB_1 + i_1 * NUM_WI_GLB_1;
      
      lcl_cache_input_1[ lcl_index_3 * LM_SIZE_input_1_dim_2 + lcl_index_1 ] =
        glb_input_1[ (OFFSET_3 + glb_index_3) * GM_SIZE_1 +(OFFSET_1 + glb_index_1) ];
    }

  // cache second input
  //#pragma unroll
  for( int i_1 = 0 ; i_1 < LM_SIZE_1 / NUM_WI_LCL_1 ; ++i_1 )
    //#pragma unroll
    for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
    {
      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
      const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;

      const int glb_index_1 = WI_ID_GLB_1 + i_1 * NUM_WI_GLB_1;
      const int glb_index_2 = WI_ID_GLB_2 + i_2 * NUM_WI_GLB_2;
      
      lcl_cache_input_2[ lcl_index_1 * LM_SIZE_input_2_dim_2 + lcl_index_2 ] =
        glb_input_2[ (OFFSET_1 + glb_index_1) * GM_SIZE_2 + (OFFSET_2 + glb_index_2) ];
    }
  
  barrier( CLK_LOCAL_MEM_FENCE );
#endif
}


inline void update_PM_cache( __local   const float* restrict lcl_cache_input_1, __local   const float* restrict lcl_cache_input_2,
                             __private       float* restrict prv_cache_input_1, __private       float* restrict prv_cache_input_2,
                             const int WI_ID_PRV_3, const int WI_ID_PRV_2, const int WI_ID_PRV_1,
                             const int WI_ID_LCL_3, const int WI_ID_LCL_2, const int WI_ID_LCL_1,
                             const int id_pm_block_3,     const int id_pm_block_2,     const int id_pm_block_1
                           )

{

#if 1
  // offsets to LM block
  const int OFFSET_3 = id_pm_block_3 * PM_BLOCK_SIZE_3;
  const int OFFSET_2 = id_pm_block_2 * PM_BLOCK_SIZE_2;
  const int OFFSET_1 = id_pm_block_1 * PM_BLOCK_SIZE_1;
  
  
        //printf("PM_SIZE_3 = %i, PM_SIZE_1 = %i\n", PM_SIZE_3, PM_SIZE_1 );
  // cache first input
  //#pragma unroll
  for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
    //#pragma unroll
    for( int i_1 = 0 ; i_1 < PM_SIZE_1 ; ++i_1 )
    {
      const int prv_index_3 = WI_ID_PRV_3 + i_3 * NUM_WI_PRV_3;
      const int prv_index_1 = WI_ID_PRV_1 + i_1 * NUM_WI_PRV_1;
    
      const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
      
      prv_cache_input_1[ prv_index_3 * PM_SIZE_input_1_dim_2 + prv_index_1 ] =
        lcl_cache_input_1[ (OFFSET_3 + lcl_index_3) * LM_SIZE_input_1_dim_2 + (OFFSET_1 + lcl_index_1) ];
    }

  // cache second input
  //#pragma unroll
  for( int i_1 = 0 ; i_1 < PM_SIZE_1 ; ++i_1 )
    //#pragma unroll
    for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
    {
      const int prv_index_1 = WI_ID_PRV_1 + i_1 * NUM_WI_PRV_1;
      const int prv_index_2 = WI_ID_PRV_2 + i_2 * NUM_WI_PRV_2;
    
      const int lcl_index_1 = WI_ID_LCL_1 + i_1 * NUM_WI_LCL_1;
      const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
      
      prv_cache_input_2[ prv_index_1 * PM_SIZE_input_2_dim_2 + prv_index_2 ] =
        lcl_cache_input_2[ (OFFSET_1 + lcl_index_1) * LM_SIZE_input_2_dim_2 + (OFFSET_2 + lcl_index_2) ];
    }
#else
  //#pragma unroll
  for( int i = 0 ; i < PM_SIZE_3 * PM_SIZE_1 ; ++i )
    prv_cache_input_1[ i ] = 1;
  //#pragma unroll
  for( int i = 0 ; i < PM_SIZE_1 * PM_SIZE_2 ; ++i )
    prv_cache_input_2[ i ] = 1;
#endif
}


// view
struct pair
{
  float const* lhs;
  float const* rhs;
};


inline const struct pair view( __private const float* restrict in_1,
                               __private const float* restrict in_2,
                               const int i_3,
                               const int i_2,
                               const int i_1
                             )
{
  struct pair p;
  
  p.lhs = &in_1[ i_3 * PM_SIZE_input_1_dim_2 + i_1 ];
  p.rhs = &in_2[ i_1 * PM_SIZE_input_2_dim_2 + i_2 ];
  
  return p;
}

// user function (TODO pre-user function: add, ... , stencils, etc. )
inline float f( const struct pair p )
{
  return *(p.lhs) * *(p.rhs);
}


__kernel void gemm_1( __global const float* restrict glb_input_1,
                      __global const float* restrict glb_input_2,
                      __global       float* restrict glb_output
                    )
{

  for( size_t i = 0 ; i  < GM_SIZE_3 * GM_SIZE_2 ; ++i )
    glb_output[ i ]  =  0;

  // cache memory
  __local float lcl_cache_input_1[ LM_SIZE_input_1_dim_1 ][ LM_SIZE_input_1_dim_2 ];
  __local float lcl_cache_input_2[ LM_SIZE_input_2_dim_1 ][ LM_SIZE_input_2_dim_2 ];

  __private float prv_cache_input_1[ PM_SIZE_input_1_dim_1 ][ PM_SIZE_input_1_dim_2 ];
  __private float prv_cache_input_2[ PM_SIZE_input_2_dim_1 ][ PM_SIZE_input_2_dim_2 ];
  
  // memory for intermediate results
  __private float tmp_res_prv[ PM_SIZE_3 ][ PM_SIZE_2 ]; // for schematical implementation -> [ 1 * PM_SIZE_3 ][ 1 * PM_SIZE_2 ][ 1 ]
  __local   float tmp_res_lcl[ LM_SIZE_3 ][ LM_SIZE_2 ][ NUM_WI_LCL_1 ];
  
  // WG/WI ids
  const int WG_ID_3 = get_group_id( 2 );
  const int WG_ID_2 = get_group_id( 1 );
  const int WG_ID_1 = get_group_id( 0 );

  const int WI_ID_GLB_3 = get_global_id( 2 );
  const int WI_ID_GLB_2 = get_global_id( 1 );
  const int WI_ID_GLB_1 = get_global_id( 0 );
  
  const int WI_ID_LCL_3 = get_local_id( 2 );
  const int WI_ID_LCL_2 = get_local_id( 1 );
  const int WI_ID_LCL_1 = get_local_id( 0 );

  const int WI_ID_PRV_3 = 0;
  const int WI_ID_PRV_2 = 0;
  const int WI_ID_PRV_1 = 0;

  // clean tmp_res_prv
  //#pragma unroll
  for( int i_3 = 0 ; i_3 < PM_SIZE_3 / NUM_WI_PRV_3 ; ++i_3 )
    //#pragma unroll
    for( int i_2 = 0 ; i_2 < PM_SIZE_2 / NUM_WI_PRV_2 ; ++i_2 )
    {
      const int prv_index_3 = i_3;
      const int prv_index_2 = i_2;
    
      tmp_res_prv[ prv_index_3 ][ prv_index_2 ] = 0;
    }

  // clean tmp_res_lcl
  //#pragma unroll
  for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
    //#pragma unroll
    for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
    {
      const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
      const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
      
      tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = 0;
    }
  barrier( CLK_LOCAL_MEM_FENCE );
  
  // iteration over local memory blocks
  //#pragma unroll
  for( int id_lm_block_3 = 0 ; id_lm_block_3 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_3 ; ++id_lm_block_3 )
    //#pragma unroll
    for( int id_lm_block_2 = 0 ; id_lm_block_2 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_2 ; ++id_lm_block_2 )
    {
      //#pragma unroll
      for( int id_lm_block_1 = 0 ; id_lm_block_1 < NUM_WG_ITERATIONS_OVER_LM_BLOCKS_1 ; ++id_lm_block_1 )
      {
        update_LM_cache( glb_input_1, glb_input_2,
                         lcl_cache_input_1, lcl_cache_input_2,
                         WI_ID_LCL_3, WI_ID_LCL_2, WI_ID_LCL_1,
                         WI_ID_GLB_3, WI_ID_GLB_2, WI_ID_GLB_1,
                         id_lm_block_3, id_lm_block_2, id_lm_block_1
                       );
        
        // iteration over private memory blocks
        //#pragma unroll
        for( int id_pm_block_3 = 0 ; id_pm_block_3 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_3 ; ++id_pm_block_3 )
          //#pragma unroll
          for( int id_pm_block_2 = 0 ; id_pm_block_2 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_2 ; ++id_pm_block_2 )
          {
            //#pragma unroll
            for( int id_pm_block_1 = 0 ; id_pm_block_1 < NUM_WI_ITERATIONS_OVER_PM_BLOCKS_1 ; ++id_pm_block_1 )
            {
              update_PM_cache( lcl_cache_input_1, lcl_cache_input_2,
                               prv_cache_input_1, prv_cache_input_2,
                               WI_ID_PRV_3, WI_ID_PRV_2, WI_ID_PRV_1,
                               WI_ID_LCL_3, WI_ID_LCL_2, WI_ID_LCL_1,
                               id_pm_block_3, id_pm_block_2, id_pm_block_1
                             );
              
              // computation of a WI
              //#pragma unroll
              for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
                //#pragma unroll
                for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
                  //#pragma unroll
                  for( int i_1 = 0 ; i_1 < PM_SIZE_1 ; ++i_1 )
{
                    tmp_res_prv[ i_3 ][ i_2 ] += f( view( prv_cache_input_1, prv_cache_input_2, i_3, i_2, i_1 ) );
//printf("res = %f\n", f( view( prv_cache_input_1, prv_cache_input_2, i_3, i_2, i_1 ) ) );
}
            } // end for-loop "i_pm_1"
            
            // i) copy intermediate results from private to local memory, and ii) clean "tmp_res_prv"
            //#pragma unroll
            for( int i_3 = 0 ; i_3 < PM_SIZE_3 ; ++i_3 )
              //#pragma unroll
              for( int i_2 = 0 ; i_2 < PM_SIZE_2 ; ++i_2 )
              {
                const int OFFSET_3 = id_pm_block_3 * PM_BLOCK_SIZE_3;
                const int OFFSET_2 = id_pm_block_2 * PM_BLOCK_SIZE_2;
                
                const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
                const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
              
                const int prv_index_3 = i_3;
                const int prv_index_2 = i_2;

//if( get_global_id(0) == 0 )
//{
                tmp_res_lcl[ OFFSET_3 + lcl_index_3 ][ OFFSET_2 + lcl_index_2 ][ WI_ID_LCL_1 ] += tmp_res_prv[ prv_index_3 ][ prv_index_2 ];
//if( get_global_id(0) == 1 )
//printf("prv = %f\n", tmp_res_prv[ prv_index_3 ][ prv_index_2 ] );
//printf("tmp_res_lcl[ %i ][ %i ][ %i ] = %f\n", OFFSET_3 + lcl_index_3, OFFSET_2 + lcl_index_2, WI_ID_LCL_1, tmp_res_lcl[ OFFSET_3 + lcl_index_3 ][ OFFSET_2 + lcl_index_2 ][ WI_ID_LCL_1 ] );
                tmp_res_prv[ prv_index_3 ][ prv_index_2 ] = 0;
//}
//barrier( CLK_LOCAL_MEM_FENCE );
//if( get_global_id(0) == 0 )
//printf("lcl = %f\n", tmp_res_lcl[ OFFSET_3 + lcl_index_3 ][ OFFSET_2 + lcl_index_2 ][ WI_ID_LCL_1 ] );
              }
            barrier( CLK_LOCAL_MEM_FENCE );
            
          } // end for-loop "i_pm_2"
      } // end for-loop "id_lm_block_1"
      
    // parallel reduction of intermediate results
    //#pragma unroll
    for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 ) //+= NUM_WI_LCL_3 )
      //#pragma unroll
      for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 ) //+= NUM_WI_LCL_2 )
        //#pragma unroll
        for( int stride = NUM_WI_LCL_1 / 2 ; stride > 0 ; stride /= 2 )
        {
          const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
          const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;

          if( WI_ID_LCL_1 < stride )
            tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] += tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 + stride ];

          barrier( CLK_LOCAL_MEM_FENCE );
        }
      
      // i) copy intermediate results from local memory to global memory, and ii) clean "tmp_res_lcl"
      //#pragma unroll
      for( int i_3 = 0 ; i_3 < LM_SIZE_3 / NUM_WI_LCL_3 ; ++i_3 )
        //#pragma unroll
        for( int i_2 = 0 ; i_2 < LM_SIZE_2 / NUM_WI_LCL_2 ; ++i_2 )
        {
          const int OFFSET_3 = id_lm_block_3 * LM_BLOCK_SIZE_3;
          const int OFFSET_2 = id_lm_block_2 * LM_BLOCK_SIZE_2;

          const int glb_index_3 = WI_ID_GLB_3 + i_3 * NUM_WI_GLB_3;
          const int glb_index_2 = WI_ID_GLB_2 + i_2 * NUM_WI_GLB_2;

          const int lcl_index_3 = WI_ID_LCL_3 + i_3 * NUM_WI_LCL_3;
          const int lcl_index_2 = WI_ID_LCL_2 + i_2 * NUM_WI_LCL_2;
        
          if( WI_ID_LCL_1 == 0 )
            glb_output[ (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
                        (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
                        WG_ID_1                                             // dim 3
                      ] += tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ 0 ];
  
//  if( WI_ID_LCL_1 == 0 && WI_ID_LCL_2 == 0 )
//  {
//  printf("y = %i, x = %i, k = %i, WI_ID = %i\n\n", (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1), (OFFSET_2 + glb_index_2) * NUM_WG_1, WG_ID_1, WI_ID_LCL_1 );
//printf("index = %i, OFFSET_3 = %i, glb_index_3 = %i, GM_SIZE_2 = %i, NUM_WG_1 = %i, OFFSET_2 = %i, id_lm_block_2 = %i, LM_BLOCK_SIZE_2 = %i,  glb_index_2 = %i, NUM_WG_1 = %i, WG_ID_1 = %i, res = %f\n",
//(OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
//                        (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
//                        WG_ID_1,
//OFFSET_3, glb_index_3, GM_SIZE_2, NUM_WG_1, OFFSET_2, id_lm_block_2, LM_BLOCK_SIZE_2, glb_index_2, NUM_WG_1, WG_ID_1,
//glb_output[ (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
//                        (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
//                        WG_ID_1                                             // dim 3
//                      ]
//);
//}
          barrier( CLK_GLOBAL_MEM_FENCE );

//printf("res = %f\n",             glb_output[ (OFFSET_3 + glb_index_3) * (GM_SIZE_2 * NUM_WG_1) + // dim 1
//                      (OFFSET_2 + glb_index_2) *              NUM_WG_1  + // dim 2
//                      WG_ID_1                                             // dim 3
//                    ] );
          
          // clean "tmp_res_lcl"
          tmp_res_lcl[ lcl_index_3 ][ lcl_index_2 ][ WI_ID_LCL_1 ] = 0;
          barrier( CLK_LOCAL_MEM_FENCE );
        } // end for-loop "i_2"

    } // end for-loop "i_lm_2"
}


__kernel void gemm_2( __global float* output_1,
                      __global float* output_2
                    )
{
}

)";

#endif
