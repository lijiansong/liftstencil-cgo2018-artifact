// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#pragma once

#include <fstream>
#include <iostream>
#include <ostream>
#include <string>
#include <tuple>
#include <vector>

#include "job.hxx"

namespace atfc {
auto parse_json(const ::std::string &p_path) -> job;
}
