// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <parser/meta.hxx>

namespace atfc {
const ::std::unordered_set<::std::string> meta_kw_guard::m_KeyWords = {
    "source",       "run_script", "search_technique", "abort_condition",
    "cost_file",    "tp",         "compile_script",   "var",
    "ignore_begin", "ignore_end"};

const ::std::unordered_set<::std::string> meta_ocl_kw_guard::m_KeyWords = {
    "input", "gs", "ls", "device_info", "kernel_name"};

const ::std::unordered_set<::std::string> meta_cuda_kw_guard::m_KeyWords = {
    "input", "block_dim", "grid_dim", "device_id", "kernel_name"};
} // namespace atfc
