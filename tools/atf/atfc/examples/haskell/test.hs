-- ATFC directives
#atf::run_script "./run.sh"
#atf::search_technique "atf::exhaustive"
#atf::tp name "N" type "size_t" range "atf::interval<size_t>(1, Max)"
#atf::tp name "M" type "size_t" range "atf::interval<size_t>(1, Max)"
#atf::cost_file "costfile.txt"

#atf::var<int> Max = argv[1]

import System.IO  
import Control.Monad

value1 :: Int
value1 = N

value2 :: Int
value2 = M


main = do
        file <- openFile "costfile.txt" WriteMode
        hPrint file (value1 + value2)
        hClose file





