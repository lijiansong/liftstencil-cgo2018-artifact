#atf::var<int> N = 4096

#atf::search_technique "atf::exhaustive"
#atf::abort_condition "atf::cond::duration<std::chrono::seconds>(2)"

#atf::tp 	name "WPT" \
			type "int" \
			range "atf::interval<int>(1, N)" \
			constraint "atf::divides(N)"
			
#atf::tp 	name "LS" \
			type "int" \
			range "atf::interval<int>(1, N)" \
			constraint "atf::divides(N / WPT)"

#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

// kernel inputs
#atf::ocl::input "atf::scalar<int>(N)"		// N
#atf::ocl::input "atf::scalar<float>()"		// a
#atf::ocl::input "atf::buffer<float>(N)"	// x
#atf::ocl::input "atf::buffer<float>(N)"	// y

#atf::ocl::kernel_name "saxpy"
#atf::ocl::ls "LS"
#atf::ocl::gs "N/WPT"


__kernel void saxpy( const int N,
                     const float a,
                     const __global float* x,
                      __global float* y)
{       
    for(int w = 0; w < WPT; ++w)
    {
            const int id = w * get_global_size(0) 
                            + get_global_id(0);
            
            y[id] += a * x[id];
    }
}
